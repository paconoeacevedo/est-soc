<?php

header('Content-Type: application/json');
backup_tables('fnar.mx', 'fnar', 'Paco001002.', 'fnar_hrwise_est_soc');

//backup_tables('fnar.com','root','','est_soc_temp');
//backup_tables('hrwise.com.mx','hrwistoz','mx-fn@paco','hrwistoz_est_temp');
/* backup the db OR just a table */
function backup_tables($host, $user, $pass, $name, $tables = '*') {

    $return = "SET NAMES 'utf8';\n\n";
    $link = mysql_connect($host, $user, $pass);
    mysql_select_db($name, $link);

    //get all of the tables
    if ($tables == '*') {
        $tables = array();
        $result = mysql_query('SHOW TABLES');
        while ($row = mysql_fetch_row($result)) {
            $tables[] = $row[0];
        }
    } else {
        $tables = is_array($tables) ? $tables : explode(',', $tables);
    }

    //cycle through
    foreach ($tables as $table) {
        $result = mysql_query('SELECT * FROM ' . $table);
        $num_fields = mysql_num_fields($result);

        $return .= 'DROP TABLE IF EXISTS ' . $table . ';';
        $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE ' . $table));
        $return .= "\n\n" . $row2[1] . ";\n\n";

        for ($i = 0; $i < $num_fields; $i++) {
            while ($row = mysql_fetch_row($result)) {
                $return .= 'INSERT INTO ' . $table . ' VALUES(';
                for ($j = 0; $j < $num_fields; $j++) {
                    $row[$j] = addslashes($row[$j]);
                    $row[$j] = ereg_replace("\n", "\\n", $row[$j]);
                    if (isset($row[$j])) {
                        $return .= "'" . $row[$j] . "'";
                    } else {
                        $return .= "''";
                    }
                    if ($j < ($num_fields - 1)) {
                        $return .= ',';
                    }
                }
                $return .= ");\n";
            }
        }
        $return .= "\n\n\n";
    }

    //save file
    $file = 'backs/est-soc-server-' . /* .time(). */'-' . /* (md5(implode(',',$tables))) */date('Y.m.d') . '.sql';
    $handle = fopen($file, 'w+');
    fwrite($handle, $return);
    fclose($handle);

    $mysql_host = "hrwise.local";
    $mysql_database = "estudios-local";
    $mysql_user = "root";
    $mysql_password = "";
    # MySQL with PDO_MYSQL  
    $db = new PDO("mysql:host=$mysql_host;dbname=$mysql_database", $mysql_user, $mysql_password, 
             array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
    
    $query = file_get_contents($file);
    //$db->query( 'SET @@global.max_allowed_packet = ' . 500 * 1024 * 1024 );
    
    $stmt = $db->prepare($query);
    //$stmt->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //$stmt->setAttribute(PDO::ATTR_TIMEOUT, ini_get('max_execution_time'));
    try {
        if ($stmt->execute()) {
            echo json_encode(array('status' => '200'));
        } else {
            echo json_encode(array('status' => '400'));
        }
        $stmt->errorInfo();
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }

    
}
