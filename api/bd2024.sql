ALTER TABLE `fnarmx_hrwise_est_soc_2024`.`padre_familia` 
ADD COLUMN `ocupacion_list` varchar(255) NULL AFTER `depende_ingreso`,
ADD COLUMN `foto` varchar(255) NULL AFTER `ocupacion_list`,
ADD COLUMN `profesion_list` varchar(255) NULL AFTER `foto`,
ADD COLUMN `giro_list` varchar(255) NULL AFTER `profesion_list`,
ADD COLUMN `sueldo_bruto` decimal(65, 2) NULL AFTER `giro_list`,
ADD COLUMN `aporta_ingreso_familiar` varchar(255) NULL AFTER `sueldo_bruto`;


ALTER TABLE `fnarmx_hrwise_est_soc_2024`.`hijo_familia` 
ADD COLUMN `seccion_list` varchar(255) NULL AFTER `seccion_nivel_grado_cursar`,
ADD COLUMN `aporta_ingreso_familiar` varchar(255) NULL AFTER `seccion_list`,
ADD COLUMN `ocupacion` varchar(255) NULL AFTER `aporta_ingreso_familiar`;


ALTER TABLE `fnarmx_hrwise_est_soc_2024`.`dependiente_familia` 
ADD COLUMN `aporta_ingreso_familiar` varchar(255) NULL AFTER `parentesco_otro`,
ADD COLUMN `ocupacion` varchar(255) NULL AFTER `aporta_ingreso_familiar`;