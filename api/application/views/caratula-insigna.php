<script src="<?= base_url() ?>assets/print2.js"></script>

<!--<button class="btn btn-xs btn-success" onclick="imprime();">Imprimir</button>
<button class="btn btn-xs btn-default" onclick="window.history.back();">Regresar</button>-->

<style>
    .content {
        font-family: Arial, Helvetica, sans-serif;
        width: 1000px;
        /*border-style: ridge;*/
        height: auto;
        margin: auto;
    }

    .text-center {
        text-align: center;
    }

    .centrar {
        width: 70%;
        margin: auto;
    }

    .centrar2 {
        width: 90%;
        margin: auto;
    }

    .centrar100 {
        width: 100%;
        margin: auto;
    }

    .border {
        border: solid;
        border-color: black;
        border-width: 1px;
        margin: 5px 10px 0px 0px;
    }

    .subrayado {
        text-decoration-line: underline;
    }

    table.example-table,
    .example-table td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    table.example-table2,
    .example-table2 td {
        border: 0px solid black;
        border-collapse: collapse;
    }

    .elemento_tabla {
        padding: 5px;
        margin-left: 10px;
    }

    td p {
        margin-left: 10px;
    }

    .avatar {

        background-position: center;
        background-size: cover;
        height: 64px;
        width: 64px;
        margin: 0px 40px 5px 65px;
        border: 1px solid black;
    }

    .custom-file-upload {
        border: 1px solid #ccc;
        display: inline-block;
        padding: 6px 12px;
        cursor: pointer;
    }

    input[type="file"] {
        display: none;
    }
</style>
<div class="content" id="contenido">
    <div style="text-align: center;">
        <img style="width: 250px; height: 130px;" class="i" src="<?= base_url() ?>assets/logo_insigna.png">
    </div>

    <h1 class="text-center">SOLICITUD DE BECA (CICLO <?= $estudio->ciclo_escolar->ciclo_escolar ?>)</h1>

    <table class="centrar100 example-table" style="font-size:20px">
        <tr>
            <td>
                <p><strong>FAMILIA:</strong> <strong style=" font-weight: bold; font-weight: 900; padding: 2px;"><?= $estudio->familia->familia ?></strong></p>
            </td>
            <td>
                <p><strong>PAGO:</strong> <strong style="font-weight: bold;font-weight: 900; padding: 2px;"><?= $estudio->pago ?></strong></p>
            </td>
            <td>
                <p><strong>FOLIO:</strong> <strong style="font-weight: bold; font-weight: 900; padding: 2px;"><?= $estudio->instituciones[0]->num_recibo ?></strong></p>
            </td>
        </tr>
    </table>

    <h1 style="background-color: black; color: white" class="text-center">DATOS DEL (LOS) ALUMNO(S) SOLICITANTE(S) </h1>

    <table class="centrar100 example-table">
        <tr>
            <td class="elemento_tabla" style="width: 40%;">
                <p><strong>Nombre completo del solicitante</strong></p>
            </td>
            <td class="elemento_tabla">
                <p><strong>SEDE: KVP, KVH, CET, CEE</strong></p>
            </td>
            <td class="elemento_tabla">
                <p><strong>SECCIÓN: LITTLES, KINDER, ES, JH, HS</strong></p>
            </td>
            <td class="elemento_tabla">
                <p><strong>GRADO</strong></p>
            </td>
            <td class="elemento_tabla">
                <p><strong>MONTO DE APOYO ECONOMICO MENSUAL QUE SOLICITA</strong></p>
            </td>
        </tr>


        <?php
        foreach ($estudio->hijos as $hijo) {
            if ($hijo->solicita_beca === '1') {
        ?>
                <tr>
                    <td class="elemento_tabla">
                        <p><?= $hijo->nombre ?> <?= $hijo->apellido_paterno ?> <?= $hijo->apellido_materno ?></p>
                    </td>
                    <td>
                        <p><?= $hijo->institucion ?></p>
                    </td>
                    <td>
                        <p><?= $hijo->seccion_list ?></p>
                    </td>
                    <td class="elemento_tabla">
                        <p><?= $hijo->nivel_grado_cursar ?></p>
                    </td>
                    <td>
                        <p>$ <?= $hijo->apoyo_solicitado ?></p>
                    </td>
                </tr>

        <?php
            }
        }
        ?>

    </table>

    <p>Calle y número: <u><?= $estudio->familia->calle ?>, <?= $estudio->familia->num_ext ?></u></p>
    <p>Colonia: <u><?= $estudio->familia->colonia ?></u></p>
<br>
<br>
<br>
    <table class="centrar100 example-table2">
        <tr>
            <td style="width: 50%;">
                <img style="width: 100px; height: 150px; display:block;margin:auto;" class="avatar" src="<?= base_url() ?>storage/fotos/<?= $estudio->foto_papa ?>" />
            </td>
            <td style="width: 50%;">
                <img style="width: 100px; height: 150px; text-align: center; display:block;margin:auto;" class="avatar" src="<?= base_url() ?>storage/fotos/<?= $estudio->foto_mama ?>" />
            </td>
        </tr>
    </table>


    <h1 style="background-color: black; color: white" class="text-center">DATOS DEL PADRE O TUTOR DE LA MADRE Y FAMILIARES:</h1>

    <p>Estado civil de los padres: <u><?= $estudio->familia->estado_civil_padres ?></u></p>

    <table class="centrar100 example-table">
        <tr>
            <td class="text-center">
                <p><strong>Datos</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Padre o tutor</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Madre</strong></p>
            </td>
        </tr>
        <tr>
            <td class="elemento_tabla">Nombre</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->nombre ?> <?= $padre->apellido_paterno ?> <?= $padre->apellido_materno ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Fecha de nacimiento</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->fecha_nacimiento ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Teléfono oficina</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->telefono_oficina ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Teléfono celular</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->celular ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Correo electrónico</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->correo ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Profesion</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->profesion_list ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Ocupación</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->ocupacion_list ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Empresa en que trabaja</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->empresa ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Giro de la empresa</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->giro_list ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Puesto que desempeña</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->puesto ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Antiguedad</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->antiguedad ?> </td>
            <?php } ?>
        </tr>
    </table>
    <br>
    <table class="centrar100 example-table">
        <tr>
            <td class="text-center">
                <p><strong>Nombre hermano(s)</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Edad</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Nivel y grado</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Institución</strong></p>
            </td>
            <td class="text-center">
                <p><strong>% beca actual</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Procedencia del apoyo</strong></p>
            </td>
        </tr>
        <?php
        foreach ($estudio->hijos as $hijo) {
            if ($hijo->solicita_beca === '0') {
        ?>
                <tr>
                    <td class="elemento_tabla text-center">
                        <p><?= $hijo->nombre ?> <?= $hijo->apellido_paterno ?> <?= $hijo->apellido_materno ?></p>
                    </td>
                    <td class="elemento_tabla text-center">
                        <p><?= $hijo->edad ?></p>
                    </td>
                    <td class="elemento_tabla text-center">
                        <p><?= $hijo->nivel_grado_cursar ?></p>
                    </td>
                    <td class="elemento_tabla text-center">
                        <p><?= $hijo->institucion ?></p>
                    </td>
                    <td class="elemento_tabla text-center">
                        <p>-</p>
                    </td>
                    <td class="elemento_tabla text-center">
                        <p><?= $hijo->fuente_apoyo ?></p>
                    </td>
                </tr>

        <?php
            }
        }
        ?>
    </table>
    <br>

    <table class="centrar100 example-table">
        <?php
        foreach ($estudio->hijos as $hijo) {
            if ($hijo->solicita_beca === '0') {
        ?>
        <?php
            }
        }
        ?>
        <tr>
            <td class="elemento_tabla">
                <p>Número de personas que dependen de los ingresos familiares</p>
            </td>
            <td class="elemento_tabla text-center">
                <p><?php
                    $sumaDependen = 0;
                    foreach ($estudio->padres as $padre) {
                        if ($padre->depende_ingreso == '1') {
                            $sumaDependen++;
                        }
                    }

                    $sumaDependen += count($estudio->hijos);
                    $sumaDependen += count($estudio->dependientes);

                    echo $sumaDependen;
                    ?></p>
            </td>
        </tr>
    </table>
    <br>
    <p>Escriba en la siguiente tabla los miembros de la familia que APORTAN a los ingresos familiares y el tipo de actividad que desempeñan:</p>

    <table class="centrar100 example-table">
        <tr>
            <td class="text-center">
                <p><strong>Nombre de miembros que aportan al ingreso familiar.</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Parentesco</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Ocupación</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Aporta al ingreso familiar</strong></p>
            </td>

        </tr>

        <?php
        foreach ($estudio->padres as $padre) {
            if ($padre->aporta_ingreso_familiar == 'SI') {
        ?>
                <tr>
                    <td class="elemento_tabla text-center">
                        <p><?= $padre->nombre ?> <?= $padre->apellido_paterno ?> <?= $padre->apellido_materno ?></p>
                    </td>
                    <td class="elemento_tabla text-center">
                        <p>Padre</p>
                    </td>
                    <td class="elemento_tabla text-center">
                        <p><?= $padre->ocupacion_list ?></p>
                    </td>
                    <td class="elemento_tabla text-center">
                        <p>Si</p>
                    </td>
                </tr>

        <?php
            }
        }
        ?>

        <?php
        foreach ($estudio->hijos as $hijo) {
            if ($hijo->aporta_ingreso_familiar == 'SI') {
        ?>
                <tr>
                    <td class="elemento_tabla text-center">
                        <p><?= $hijo->nombre ?> <?= $hijo->apellido_paterno ?> <?= $hijo->apellido_materno ?></p>
                    </td>
                    <td class="elemento_tabla text-center">
                        <p>Hij@</p>
                    </td>
                    <td class="elemento_tabla text-center">
                        <p><?= $hijo->ocupacion ?></p>
                    </td>
                    <td class="elemento_tabla text-center">
                        <p>Si</p>
                    </td>
                </tr>

        <?php
            }
        }
        ?>

        <?php
        foreach ($estudio->dependientes as $dependiente) {
            if ($dependiente->aporta_ingreso_familiar == 'SI') {
        ?>
                <tr>
                    <td class="elemento_tabla text-center">
                        <p><?= $dependiente->nombre ?> <?= $dependiente->apellido_paterno ?> <?= $dependiente->apellido_materno ?></p>
                    </td>
                    <td class="elemento_tabla text-center">
                        <p><?= $dependiente->parentesco ?></p>
                    </td>
                    <td class="elemento_tabla text-center">
                        <p><?= $dependiente->ocupacion ?></p>
                    </td>
                    <td class="elemento_tabla text-center">
                        <p>Si</p>
                    </td>
                </tr>

        <?php
            }
        }
        ?>

    </table>


    <h1 style="background-color: black; color: white" class="text-center">DATOS ECONÓMICOS Y SITUACIÓN FINANCIERA FAMILIAR:</h1>


    <p>Señale la circunstancia que impacta su economía familiar: <strong><?= $estudio->motivos[0]->tipo_motivo ?> </strong></p>
    <p>Comentarios: <strong><?= $estudio->motivos[0]->motivo ?> </strong></p>
    <p>Defina el tiempo que considere necesario para solucionar o mejorar su situación económica: <strong><?= $estudio->motivos[0]->tiempo_solucionar ?></strong></p>


    <h3>DATOS DE LA CASA HABITACIÓN:</h3>

    <table class="centrar100 example-table">
        <tr>
            <td class="text-center">
                <p><strong>Tiempo de habitar o adquirir</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Estatus</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Tipo de propiedad</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Superficie terreno m2</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Superficie construcción m2</strong></p>
            </td>
            <td class="text-center">
                <p><strong>No. de cuartos</strong></p>
            </td>
            <td class="text-center">
                <p><strong>No. de.baños</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Valor comercial</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Cantidad que adeuda</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Pago mensual</strong></p>
            </td>
        </tr>


        <?php
        foreach ($estudio->propiedades as $propiedad) {
            //if ($propiedad->casa_habitacion == '1') {
        ?>
            <tr>
                <td class="text-center">
                    <p><?= $propiedad->tiempo_habitar ?></p>
                </td>
                <td class="text-center">
                    <p><?= $propiedad->status ?></p>
                </td>
                <td class="text-center">
                    <p><?= $propiedad->tipo_propiedad ?></p>
                </td>
                <td class="text-center">
                    <p><?= $propiedad->superficie_terreno ?></p>
                </td>
                <td class="text-center">
                    <p><?= $propiedad->superficie_construccion ?></p>
                </td>
                <td class="text-center">
                    <p><?= $propiedad->numero_cuartos ?></p>
                </td>
                <td class="text-center">
                    <p><?= $propiedad->numero_banos ?></p>
                </td>
                <td class="text-center">
                    <p><?= number_format($propiedad->valor_comercial_actual, 2) ?></p>
                </td>
                <td class="text-center">
                    <p><?= number_format($propiedad->cantidad_adeuda, 2) ?></p>
                </td>
                <td class="text-center">
                    <p><?= number_format($propiedad->monto_renta, 2) ?></p>
                </td>
            </tr>
        <?php
            //}
        }
        ?>
    </table>

    <p>Vehículos que posee y/o utiliza la familia, incluya los proporcionados por la empresa:</p>
    <table class="centrar100 example-table">
        <tr>
            <td class="text-center">
                <p><strong>Propietario</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Marca</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Modelo</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Valor comercial</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Cantidad que adeuda</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Pago mensual</strong></p>
            </td>
        </tr>
        <?php
        foreach ($estudio->vehiculos as $v) {
        ?>
            <tr>
                <td class="elemento_tabla text-center">
                    <p><?= $v->propietario  ?></p>
                </td>
                <td class="elemento_tabla text-center">
                    <p><?= $v->marca  ?></p>
                </td>
                <td class="elemento_tabla text-center">
                    <p><?= $v->modelo  ?></p>
                </td>
                <td class="elemento_tabla text-center">
                    <p>$ <?= number_format($v->valor_comercial_actual, 2)  ?></p>
                </td>
                <td class="elemento_tabla text-center">
                    <p>$ <?= number_format($v->cantidad_adeuda, 2)  ?></p>
                </td>
                <td class="elemento_tabla text-center">
                    <p>$ <?= number_format($v->pago_mensual, 2) ?></p>
                </td>
            </tr>

        <?php
        }
        ?>
    </table>
    <p>Indique si el padre o madre poseen pasivos financieros como tarjetas de crédito, créditos bancarios, personales y comerciales, etc. Anexe estados de cuenta de los 3 últimos meses:</p>
    <table class="centrar100 example-table">
        <tr>
            <td class="text-center" style="width: 25%;">
                <p><strong>Institución a la que se le adeuda
                        (banco, institución financiera, trabajo, establecimiento)
                    </strong></p>
            </td>
            <td class="text-center" style="width: 45%;">
                <p><strong>Concepto por el que se adeuda</strong></p>
            </td>
            <td class="text-center" style="width: 10%;">
                <p><strong>Monto solicitado</strong></p>
            </td>
            <td class="text-center" style="width: 10%;">
                <p><strong>Cantidad que adeuda actualmente</strong></p>
            </td>
        </tr>
        <?php
        foreach ($estudio->pasivos as $p) {
        ?>
            <tr>
                <td class="elemento_tabla text-center">
                    <p><?= $p->institucion  ?></p>
                </td>
                <td class="elemento_tabla text-center">
                    <p><?= $p->concepto  ?></p>
                </td>
                <td class="elemento_tabla text-center">
                    <p>$ <?= number_format($p->monto_solicitado, 2)  ?></p>
                </td>
                <td class="elemento_tabla text-center">
                    <p>$ <?= number_format($p->cantidad_adeuda, 2)  ?></p>
                </td>
            </tr>

        <?php
        }
        ?>
    </table>

    <br>
    <h3>INGRESOS Y EGRESOS FAMILIARES:</h3>
    <h4>Ingresos mensuales:</h4>
    <table class="centrar100 example-table">
        <tr>
            <td class="">
                <p>Sueldo bruto mensual del padre:</p>
            </td>
            <td class="text-center">
                <p>$
                    <?php
                    foreach ($estudio->padres as $padre) {
                        if ($padre->tipo_persona == 'PAPA') {
                            echo number_format($padre->sueldo_bruto, 2);
                        }
                    }
                    ?>
                </p>
            </td>
        </tr>
        <tr>
            <td class="">
                <p>Sueldo neto MENSUAL del padre:</p>
            </td>
            <td class="text-center">$ <?= number_format($estudio->ingresos[0]->sueldo_papa, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Sueldo bruto mensual de la madre:</p>
            </td>
            <td class="text-center">
                <p>$
                    <?php
                    foreach ($estudio->padres as $padre) {
                        if ($padre->tipo_persona == 'MAMA') {
                            echo number_format($padre->sueldo_bruto, 2);
                        }
                    }
                    ?>
                </p>
            </td>
        </tr>
        <tr>
            <td class="">
                <p>Sueldo neto MENSUAL de la madre:</p>
            </td>
            <td class="text-center">$ <?= number_format($estudio->ingresos[0]->sueldo_mama, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Ingresos MENSUALES de otros familiares:</p>
            </td>
            <td class="text-center">$ <?= number_format($estudio->ingresos[0]->ingreso_otros_miembros, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Ingresos por rentas:</p>
            </td>
            <td class="text-center">$ <?= number_format($estudio->ingresos[0]->ingreso_renta, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Otros ingresos MENSUALES (bonos, aguinaldos, honorarios, comisiones, pensiones, apoyos adicionales):</p>
            </td>
            <td class="text-center">$ <?= number_format($estudio->ingresos[0]->otros_ingresos, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p><strong>Total ingreso neto mensual:</strong></p>
            </td>
            <td class="text-center">$ <?= number_format($estudio->ingresos[0]->total_ingresos, 2) ?></td>
        </tr>
    </table>
    <h4>Egresos mensuales:</h4>
    <table class="centrar100 example-table">
        <tr>
            <td class="">
                <p>Alimentación:</p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->alimentacion_despensa, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Renta:</p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->renta, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Crédito Hipotecario:</p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->credito_hipotecario, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Crédito o arrendamiento de automóvil:</p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->credito_auto, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Créditos comerciales y tarjetas de crédito:</p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->pago_tdc_mensual, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Gasolina:</p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->gasolina, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Servicios: (agua, luz, gas, teléfono, celulares, internet, cable, otros):</p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->total_servicios, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Servicio domestico:</p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->servicio_domestico, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Vestido y calzado (prorrateo mensual):</p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->vestido_calzado, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Colegiaturas:</p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->colegiaturas, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Colegiaturas de otros miembros de la familia:</p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->otras_colegiaturas, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Clases particulares:</p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->clases_particulares, 2)?></td>
        </tr>
        <tr>
            <td class="">
                <p>Diversión y entretenimiento:</p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->diversion_entretenimiento, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Vacaciones (prorrateo mensual):</p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->vacaciones, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Gastos médicos y medicinas:</p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->medico_medicinas, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Gasto mensual de seguros (suma de seguros médicos, autos, vida, casa, etc):</p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->seguros, 2)?></td>
        </tr>
        <tr>
            <td class="">
                <p>Club deportivo:</p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->clubes_deportivos, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Otros (especifique):</p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->otros2, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p><strong>Total gasto mensual:</strong></p>
            </td>
            <td class="text-center"><?= '$ ' . number_format($estudio->egresos[0]->total_egresos, 2) ?></td>
        </tr>
        <tr>
            <td class="">
                <p>&nbsp;</p>
            </td>
            <td class="text-center"></td>
        </tr>
        <tr>
            <td class="">
                <p><strong>Remanente / Déficit neto mensual:</strong></p>
            </td>
            <td class="text-center">
                <p>$ 
                    <?php
                        echo number_format($estudio->ingresos[0]->total_ingresos-$estudio->egresos[0]->total_egresos, 2);
                    ?>
                </p>
            </td>
        </tr>
    </table>
    <p>En caso de existir diferencias (déficit o remanente) explicar cómo se sobrellevan los gastos o que se hace con remanente: <strong><?= $estudio->egresos[0]->explicacion_diferencia?></strong> </p>


</div>