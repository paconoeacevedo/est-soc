<style>
    .content {
        font-family: Arial, Helvetica, sans-serif;
        width: 1000px;
        /*border-style: ridge;*/
        height: auto;
        margin: auto;
    }

    .text-center {
        text-align: center;
    }

    .centrar {
        width: 70%;
        margin: auto;
    }

    .centrar2 {
        width: 90%;
        margin: auto;
    }

    .centrar100 {
        width: 100%;
        margin: auto;
    }

    .border {
        border: solid;
        border-color: black;
        border-width: 1px;
        margin: 5px 10px 0px 0px;
    }

    .subrayado {
        text-decoration-line: underline;
    }

    table.example-table,
    .example-table td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    .elemento_tabla {
        padding: 5px;
    }
</style>
<div class="content">

    <table style="margin: auto; width: 90%">
        <tr>
            <td class="text-center"><img style="width: 50px; height: 50px;" class="i" src="<?= base_url() ?>assets/logoc-triana.png" align="center"></td>
            <td class="text-center"><img style="width: 50px; height: 50px;" class="i" src="<?= base_url() ?>assets/logoc-villa.png" align="center"></td>
            <td class="text-center"><img style="width: 50px; height: 50px;" class="i" src="<?= base_url() ?>assets/logoc-encino.png" align="center"></td>
        </tr>
    </table>

    <h1 class="text-center">SOLICITUD DE BECA (<?= $estudio->ciclo_escolar->ciclo_escolar ?>)</h1>

    <table class="centrar" style="font-size:12px">
        <tr>
            <td>
                <p><strong>FAMILIA:</strong> <strong style="text-decoration-line: underline; font-weight: bold; margin: 45px; font-weight: 900; padding: 2px;"><u><?= $estudio->familia->familia ?></u></strong></p>
            </td>
            <td>
                <p><strong>PAGO:</strong> <strong style="font-weight: bold; margin: 45px; font-weight: 900; padding: 2px;"><u><?= $estudio->pago ?></u></strong></p>
            </td>
            <td>
                <p><strong>FOLIO:</strong> <strong style="font-weight: bold; margin: 45px; font-weight: 900; padding: 2px;"><u><?= $estudio->instituciones[0]->num_recibo ?></u></strong></p>
            </td>
        </tr>
    </table>

    <h1 style="background-color: black; color: white" class="text-center">DATOS DE LOS ALUMNOS SOLICITANTES</h1>

    <table class="centrar100 example-table">
        <tr>
            <td colspan="5" class="text-center">
                <p><strong>Nombres de los alumnos solicitantes</strong></p>
            </td>
            <td colspan="3" class="text-center">
                <p><strong>Beca</strong></p>
            </td>
        </tr>

        <tr>
            <td class="elemento_tabla">
                <p>Nombre completo del solicitante</p>
            </td>
            <td class="elemento_tabla">
                <p>Sede</p>
            </td>
            <td class="elemento_tabla">
                <p>Sección</p>
            </td>
            <td class="elemento_tabla">
                <p>Grado que cursará</p>
            </td>
            <td class="elemento_tabla">
                <p>Antiguedad en la institución</p>
            </td>
            <td class="elemento_tabla">
                <p>Beca por primera vez</p>
            </td>
            <td class="elemento_tabla">
                <p>Beca por renovación</p>
            </td>
            <td class="elemento_tabla">
                <p>Monto en pesos de apoyo económico mensual que necesita</p>
            </td>
        </tr>


        <?php
        foreach ($estudio->hijos as $hijo) {
            if ($hijo->solicita_beca === '1') {
        ?>
                <tr>
                    <td class="elemento_tabla">
                        <p><?= $hijo->nombre ?> <?= $hijo->apellido_paterno ?> <?= $hijo->apellido_materno ?></p>
                    </td>
                    <td>
                        <p><?= $hijo->institucion ?></p>
                    </td>
                    <td>
                        <p>-</p>
                    </td>
                    <td class="elemento_tabla">
                        <p><?= $hijo->nivel_grado_cursar ?></p>
                    </td>
                    <td>
                        <p>-</p>
                    </td>
                    <td>
                        <p>-</p>
                    </td>
                    <td>
                        <p>-</p>
                    </td>
                    <td>
                        <p>-</p>
                    </td>
                </tr>

        <?php
            }
        }
        ?>

    </table>

    <p>DIRECCIÓN (Calle, número y colonia): <u><?= $estudio->familia->calle ?>, <?= $estudio->familia->num_ext ?>, <?= $estudio->familia->colonia ?></u></p>
    <p>Teléfono particular: <u><?= $estudio->familia->telefono_casa ?></u></p>
    <p>Celular: <u><?= $estudio->familia->celular ?></u></p>

    <h1 style="background-color: black; color: white" class="text-center">DATOS DEL PADRE O TUTOR Y DE LA MADRE:</h1>
    <p>Estado civil de los padres: <u><?= $estudio->familia->estado_civil_padres ?></u></p>

    <table class="centrar100 example-table">
        <tr>
            <td class="text-center">
                <p><strong>Datos</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Padre o tutor</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Madre</strong></p>
            </td>
        </tr>
        <tr>
            <td class="elemento_tabla">Nombre</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->nombre ?> <?= $padre->apellido_paterno ?> <?= $padre->apellido_materno ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Fecha de nacimiento</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->fecha_nacimiento ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Teléfono oficina</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->telefono_oficina ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Teléfono celular</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->celular ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Correo electrónico</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->correo ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Profesion</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->profesion ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Ocupación</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->ocupacion ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Empresa en que trabaja</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->empresa ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Giro de la empresa</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->giro ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Puesto que desempeña</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->puesto ?> </td>
            <?php } ?>
        </tr>
        <tr>
            <td class="elemento_tabla">Antiguedad</td>
            <?php
            foreach ($estudio->padres as $padre) {
            ?>
                <td class="elemento_tabla"><?= $padre->antiguedad ?> </td>
            <?php } ?>
        </tr>
    </table>

    <h1 style="background-color: black; color: white" class="text-center">DATOS FAMILIARES:</h1>
    <table class="centrar100 example-table">
        <tr>
            <td class="text-center">
                <p><strong>Nombre hermano(s)</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Nivel y grado</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Institución</strong></p>
            </td>
            <td class="text-center">
                <p><strong>% beca actual</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Procedencia del apoyo</strong></p>
            </td>
        </tr>
        <?php
        foreach ($estudio->hijos as $hijo) {
            if ($hijo->solicita_beca === '0') {
        ?>
                <tr>
                    <td class="elemento_tabla text-center">
                        <p><?= $hijo->nombre ?> <?= $hijo->apellido_paterno ?> <?= $hijo->apellido_materno ?></p>
                    </td>
                    <td class="elemento_tabla text-center">
                        <p><?= $hijo->nivel_grado_cursar ?></p>
                    </td>
                    <td class="elemento_tabla text-center">
                        <p><?= $hijo->institucion ?></p>
                    </td>
                    <td class="elemento_tabla text-center">
                        <p>-</p>
                    </td>
                    <td class="elemento_tabla text-center">
                        <p><?= $hijo->fuente_apoyo ?></p>
                    </td>
                </tr>

        <?php
            }
        }
        ?>
    </table>
    <br>

    <table class="centrar100 example-table">
        <?php
        foreach ($estudio->hijos as $hijo) {
            if ($hijo->solicita_beca === '0') {
        ?>
        <?php
            }
        }
        ?>
        <tr>
            <td class="elemento_tabla">
                <p>Número de personas que viven en la casa habitación</p>
            </td>
            <td class="elemento_tabla text-center">
                <p><?php
                    echo count($estudio->hijos) + count($estudio->dependientes) + 2;
                    ?></p>
            </td>
        </tr>
        <tr>
            <td class="elemento_tabla ">
                <p>Número de hermanos del solicitante</p>
            </td>
            <td class="elemento_tabla text-center">
                <p><?php
                    echo count($estudio->hijos);
                    ?></p>
            </td>
        </tr>
        <tr>
            <td class="elemento_tabla">
                <p>Número de personas que dependen de los ingresos familiares</p>
            </td>
            <td class="elemento_tabla text-center">
                <p><?php
                    echo count($estudio->hijos) + count($estudio->dependientes) + 2;
                    ?></p>
            </td>
        </tr>
    </table>
    <br>
    <br>

    <p>Señale el motivo por el cual está solicitando la beca:</p>
    <table class="centrar100 example-table">
        <tr>
            <td class="elemento_tabla">
                <p><?php
                    foreach ($estudio->motivos as $motivo) {
                        echo $motivo->motivo;
                    }
                    ?></p>
            </td>
        </tr>
    </table>

    <h1 style="background-color: black; color: white" class="text-center">DATOS ECONÓMICOS:</h1>
    <table class="centrar100 example-table">
        <?php
        foreach ($estudio->propiedades as $propiedad) {
            if ($propiedad->casa_habitacion == '1') {
        ?>
                <tr>
                    <td class="">
                        <p><strong>Datos de la casa habitación:</strong></p>
                    </td>
                    <td class="text-center"></td>
                </tr>
                <tr>
                    <td class="">
                        <p>Fecha de adquisición o arrendamiento:</p>
                    </td>
                    <td class="text-center"><?= $propiedad->status ?></td>
                </tr>
                <tr>
                    <td class="">
                        <p>Superficie del terreno en m2:</p>
                    </td>
                    <td class="text-center"><?= $propiedad->superficie_terreno ?></td>
                </tr>
                <tr>
                    <td class="">
                        <p>Superficie construida en m2:</p>
                    </td>
                    <td class="text-center"><?= $propiedad->superficie_construccion ?></td>
                </tr>
                <tr>
                    <td class="">
                        <p>Número de cuartos:</p>
                    </td>
                    <td class="text-center">-</td>
                </tr>
                <tr>
                    <td class="">
                        <p>Renta:</p>
                    </td>
                    <td class="text-center"><?= '$ ' . $propiedad->monto_renta ?></td>
                </tr>
                <tr>
                    <td class="">
                        <p>Pago mensual hipoteca:</p>
                    </td>
                    <td class="text-center"><?= '$ ' . $propiedad->monto_renta ?></td>
                </tr>
                <tr>
                    <td class="">
                        <p>Cantidad que adeuda:</p>
                    </td>
                    <td class="text-center"><?= '$ ' . $propiedad->cantidad_adeuda ?></td>
                </tr>
                <tr>
                    <td class="">
                        <p>Valor comercial actual:</p>
                    </td>
                    <td class="text-center"><?= '$ ' . $propiedad->valor_comercial_actual ?></td>
                </tr>

        <?php
            }
        }
        ?>
    </table>
    <br>
    <table class="centrar100 example-table">
        <tr>
            <td class="">
                <p><strong>Ingresos mensuales:</strong></p>
            </td>
            <td class="text-center"></td>
        </tr>
        <!--<tr>
            <td class="">
                <p>Sueldo bruto mensual del padre:</p>
            </td>
            <td class="text-center"><?php echo $estudio->ingresos[0]->sueldo_papa ?></td>
        </tr>-->
        <tr>
            <td class="">
                <p>Sueldo neto mensual del padre:</p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->ingresos[0]->sueldo_papa ?></td>
        </tr>
        <!--<tr>
            <td class="">
                <p>Sueldo bruto mensual de la madre:</p>
            </td>
            <td class="text-center">100 m2</td>
        </tr>-->
        <tr>
            <td class="">
                <p>Sueldo neto mensual de la madre:</p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->ingresos[0]->sueldo_mama ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Otros ingresos:</p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->ingresos[0]->otros_ingresos ?></td>
        </tr>
        <tr>
            <td class="">
                &nbsp;
            </td>
            <td class="text-center">&nbsp;</td>
        </tr>
        <tr>
            <td class="">
                <p><strong>Egresos mensuales:</strong></p>
            </td>
            <td class="text-center"></td>
        </tr>
        <tr>
            <td class="">
                <p>Alimentación:</p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->egresos[0]->alimentacion_despensa ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Renta:</p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->egresos[0]->renta ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Crédito Hipotecario:</p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->egresos[0]->credito_hipotecario ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Crédito automóvil:</p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->egresos[0]->credito_auto ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Gasolina:</p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->egresos[0]->gasolina ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Servicios: agua, luz, gas, teléfono, cable, mantenimiento:</p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->egresos[0]->total_servicios ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Vestido y calzado:</p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->egresos[0]->vestido_calzado ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Colegiaturas:</p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->egresos[0]->colegiaturas ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Diversión y entretenimiento:</p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->egresos[0]->diversion_entretenimiento ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Servicio doméstico:</p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->egresos[0]->servicio_domestico ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Médicos y medicinas:</p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->egresos[0]->medico_medicinas ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Gasto mensual de seguros:</p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->egresos[0]->seguros ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Club deportivo:</p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->egresos[0]->clubes_deportivos ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Clases particulares:</p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->egresos[0]->clases_particulares ?></td>
        </tr>
        <tr>
            <td class="">
                <p>Pago de Tarjeta de Crédito. Restar al saldo los gastos ya reportados:</p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->egresos[0]->pago_tdc_mensual ?></td>
        </tr>
        <tr>
            <td class="">&nbsp;
            </td>
            <td class="text-center">&nbsp;</td>
        </tr>
        <tr>
            <td class="">
                <p><strong>Total gasto mensual:</strong></p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->egresos[0]->total_egresos ?></td>
        </tr>
        <tr>
            <td class="">
                <p><strong>Remanente neto mensual:</strong></p>
            </td>
            <td class="text-center">-</td>
        </tr>
        <tr>
            <td class="">
                <p><strong>Saldo actual en Tarjeta de Crédito::</strong></p>
            </td>
            <td class="text-center"><?= '$ ' . $estudio->egresos[0]->saldo_tdc ?></td>
        </tr>
    </table>

    <p>Si tiene otras propiedades:</p>
    <table class="centrar100 example-table">
        <?php
        foreach ($estudio->propiedades as $propiedad) {
            if ($propiedad->casa_habitacion == '0') {
        ?>
                <tr>
                    <td class="">
                        <p><strong>Datos de la casa habitación:</strong></p>
                    </td>
                    <td class="text-center"></td>
                </tr>
                <tr>
                    <td class="">
                        <p>Fecha de adquisición o arrendamiento:</p>
                    </td>
                    <td class="text-center"><?= $propiedad->status ?></td>
                </tr>
                <tr>
                    <td class="">
                        <p>Superficie del terreno en m2:</p>
                    </td>
                    <td class="text-center"><?= $propiedad->superficie_terreno ?></td>
                </tr>
                <tr>
                    <td class="">
                        <p>Superficie construida en m2:</p>
                    </td>
                    <td class="text-center"><?= $propiedad->superficie_construccion ?></td>
                </tr>
                <tr>
                    <td class="">
                        <p>Número de cuartos:</p>
                    </td>
                    <td class="text-center">-</td>
                </tr>
                <tr>
                    <td class="">
                        <p>Renta:</p>
                    </td>
                    <td class="text-center"><?= $propiedad->monto_renta ?></td>
                </tr>
                <tr>
                    <td class="">
                        <p>Pago mensual hipoteca:</p>
                    </td>
                    <td class="text-center"><?= $propiedad->monto_renta ?></td>
                </tr>
                <tr>
                    <td class="">
                        <p>Cantidad que adeuda:</p>
                    </td>
                    <td class="text-center"><?= $propiedad->cantidad_adeuda ?></td>
                </tr>
                <tr>
                    <td class="">
                        <p>Valor comercial actual:</p>
                    </td>
                    <td class="text-center"><?= $propiedad->valor_comercial_actual ?></td>
                </tr>

        <?php
            }
        }
        ?>
    </table>

    <p>Vehículos que posee:</p>
    <table class="centrar100 example-table">
        <tr>
            <td class="text-center">
                <p><strong>Propietario</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Marca</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Modelo</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Valor comercial</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Cantidad que adeuda</strong></p>
            </td>
            <td class="text-center">
                <p><strong>Pago mensual</strong></p>
            </td>
        </tr>
        <?php
        foreach ($estudio->vehiculos as $v) {
        ?>
            <tr>
                <td class="elemento_tabla text-center">
                    <p><?= $v->propietario  ?></p>
                </td>
                <td class="elemento_tabla text-center">
                    <p><?= $v->marca  ?></p>
                </td>
                <td class="elemento_tabla text-center">
                    <p><?= $v->modelo  ?></p>
                </td>
                <td class="elemento_tabla text-center">
                    <p><?= $v->valor_comercial_actual  ?></p>
                </td>
                <td class="elemento_tabla text-center">
                    <p><?= $v->cantidad_adeuda  ?></p>
                </td>
                <td class="elemento_tabla text-center">
                    <p><?= $v->pago_mensual ?></p>
                </td>
            </tr>

        <?php
        }
        ?>
    </table>
    <br>



</div>