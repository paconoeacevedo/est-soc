<style>
    .content {
        font-family: Arial, Helvetica, sans-serif;
        width: 1000px;
        /*border-style: ridge;*/
        height: auto;
        margin: auto;
    }

    .text-center {
        text-align: center;
    }

    .centrar {
        width: 70%;
        margin: auto;
    }

    .centrar2 {
        width: 90%;
        margin: auto;
    }

    .centrar100 {
        width: 100%;
        margin: auto;
    }

    .border {
        border: solid;
        border-color: black;
        border-width: 1px;
        margin: 5px 10px 0px 0px;
    }

    .subrayado {
        text-decoration-line: underline;
    }

    table.example-table,
    .example-table td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    .elemento_tabla {
        padding: 5px;
    }
</style>
<div class="content">

    <table style="margin: auto; width: 90%">
        <tr>
            <td class="text-center"><img style="width: 50px; height: 50px;" class="i" src="<?= base_url() ?>assets/logoc-triana.png" align="center"></td>
            <td class="text-center"><img style="width: 50px; height: 50px;" class="i" src="<?= base_url() ?>assets/logoc-villa.png" align="center"></td>
            <td class="text-center"><img style="width: 50px; height: 50px;" class="i" src="<?= base_url() ?>assets/logoc-encino.png" align="center"></td>
        </tr>
    </table>

    <h1 class="text-center">SOLICITUDES DE BECA (<?= $ciclo_escolar->ciclo_escolar ?>) </h1>

    <table class="centrar100 example-table">

        <tr>
            <td class="elemento_tabla">
                <p>Folio: </p>
            </td>
            <td class="elemento_tabla">
                <p>Familia: </p>
            </td>
            <td class="elemento_tabla">
                <p>Dirección</p>
            </td>

            <td class="elemento_tabla">
                <p>Fecha solicitud</p>
            </td>
            <td class="elemento_tabla">
                <p>Estatus</p>
            </td>
        </tr>
        <?php
        foreach ($estudios as $estudio) {
        ?>
            <tr>
                <td class="elemento_tabla">
                    <p><?= $estudio->instituciones[0]->num_recibo ?></p>
                </td>
                <td class="elemento_tabla">
                    <p><?= $estudio->familia ?></p>
                </td>
                <td class="elemento_tabla">
                    <p><?= $estudio->calle ?>, <?= $estudio->num_ext ?>, <?= $estudio->colonia ?></p>
                </td>
                <td class="elemento_tabla">
                    <p><?= $estudio->fecha_estudio ?></p>
                </td>
                <td class="elemento_tabla">
                    <p><?= $estudio->nombre_estatus ?></p>
                </td>
            </tr>

        <?php
        }
        ?>

    </table>

</div>