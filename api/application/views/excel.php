<div style="
    font-family: Arial, Helvetica, sans-serif;
        width: 1000px;
        height: auto;
        margin: auto;">

    <!--<table style="margin: auto; width: 100%">
        <tr align="center">
            <td align="center"><img align="center" height="42" width="42" src="<?= base_url() ?>assets/logoc-triana.png"></td>
            <td></td>
            <td align="center"><img align="center" height="42" width="42" src="<?= base_url() ?>assets/logoc-villa.png"></td>
            <td></td>
            <td align="center"><img align="center" height="42" width="42" src="<?= base_url() ?>assets/logoc-encino.png"></td>
        </tr>
    </table>-->

    <h1 style="text-align: center;">SOLICITUDES DE BECA (<?= $ciclo_escolar->ciclo_escolar ?>) </h1>

    <table style="width: 100%;
        margin: auto; border: 1px solid black;
        border-collapse: collapse;">

        <tr style="border: 1px solid black;
        border-collapse: collapse;">
            <td style="border: 1px solid black;
        border-collapse: collapse; padding: 5px">
                <p>No.</p>
            </td>
            <td style="border: 1px solid black;
        border-collapse: collapse; padding: 5px">
                <p>Folio: </p>
            </td>
            <td style="border: 1px solid black;
        border-collapse: collapse; padding: 5px">
                <p>Institución: </p>
            </td>
            <td style="border: 1px solid black;
        border-collapse: collapse; padding: 5px">
                <p>Familia: </p>
            </td>
            <td style="border: 1px solid black;
        border-collapse: collapse; padding: 5px">
                <p>Compartida/Instituciones</p>
            </td>

            <td style="border: 1px solid black;
        border-collapse: collapse; padding: 5px">
                <p>Fecha solicitud</p>
            </td>
            <td style="border: 1px solid black;
        border-collapse: collapse; padding: 5px">
                <p>Estatus</p>
            </td>
            <td style="border: 1px solid black;
        border-collapse: collapse; padding: 5px">
                <p>Asignado</p>
            </td>
        </tr>
        <?php
        $no = 1;
        foreach ($estudios as $estudio) {

        ?>
            <tr style="border: 1px solid black;
        border-collapse: collapse;">
                <td style="border: 1px solid black;
        border-collapse: collapse; padding: 5px">
                    <p><?php echo $no; $no++; ?></p>
                </td>
                <td style="border: 1px solid black;
        border-collapse: collapse; padding: 5px">
                    <p><?= $estudio->instituciones[0]->num_recibo ?></p>
                </td>
                <td style="border: 1px solid black;
        border-collapse: collapse; padding: 5px">
                    <p><?= $estudio->instituciones[0]->clave_institucion ?></p>
                </td>
                <td style="border: 1px solid black;
        border-collapse: collapse; padding: 5px">
                    <p><?= $estudio->familia ?></p>
                </td>
                <td style="border: 1px solid black;
        border-collapse: collapse; padding: 5px">
                    <?php

                    $solicitudes = $estudio->solicitudes;
                    if (!$solicitudes->es_compartida) {
                        echo 'No compartida';
                    } else {
                        echo 'Compartida<br>'.$solicitudes->text_instituciones;
                    }

                    ?>
                </td>
                <td style="border: 1px solid black;
        border-collapse: collapse; padding: 5px;">
                    <p style="text-align: center"><?= $estudio->fecha_estudio ?></p>
                </td>
                <td style="border: 1px solid black;
        border-collapse: collapse; padding: 5px">
                    <p><?= $estudio->nombre_estatus ?></p>
                </td>
                <td style="border: 1px solid black;
        border-collapse: collapse; padding: 5px">
                    <p><?php
                    if(isset($estudio->usuario_asignado[0])){
                        echo $estudio->usuario_asignado[0]->nombre;
                    }
                      
                     ?></p>
                </td>
            </tr>

        <?php
        }
        ?>

    </table>

</div>