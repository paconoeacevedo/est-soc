<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reporte extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
    }

    public function caratulaView($id)
    {
        $estudio = $this->estudio_model->getEstudioDetalle($id, '0');
        $data['estudio'] = $estudio;
        if($estudio->familia->id_grupo == '9'){
            $this->load->view('caratula-insigna', $data);
        }   
        if($estudio->familia->id_grupo == '10'){
            $this->load->view('caratula-iter', $data);
        }   
    }

    public function caratula($id)
    {
        header('Content-Type: application/json');
        $this->load->library('pdf');
        $estudio = $this->estudio_model->getEstudioDetalle($id, '0');
        $data['estudio'] = $estudio;
        $html = mb_convert_encoding($this->load->view('caratula-insigna', $data, true), 'HTML-ENTITIES', 'UTF-8');

        $nombre_archivo = $estudio->folio_estudio . '.pdf';
        $result = file_put_contents("storage/" . $nombre_archivo, $this->pdf->createPDF($html, 'mypdf', false));
        $response['file'] = $result;

        $datau['id_estudio'] = $estudio->id_estudio;
        $datau['archivo_caratula'] = $nombre_archivo;
        $response['update'] = $this->estudio_model->updateEstudio($datau);

        echo json_encode($response);
    }

    public function solicitudesEstudios($tipoUsuario, $rolUsuario, $idUsuario, $idInstitucion, $id_ciclo_escolar, $filterFamilia)
    {
        $params['idInstitucion'] = $idInstitucion;
        $params['tipoUsuario'] = $tipoUsuario;
        $params['rolUsuario'] = $rolUsuario;
        $params['idUsuario'] = $idUsuario;
        $params['id_ciclo_escolar'] = $id_ciclo_escolar;
        $params['filterFamilia'] = $filterFamilia;
        $params['ciclos'] = [$id_ciclo_escolar];

        $grupo_id = null;
        if (isset($params['idInstitucion'])) {
            $this->db->where('id_institucion', $params['idInstitucion']);
            $i = $this->db->get('institucion')->row();
            if(isset($i)){
                $grupo_id = $i->id_grupo;
            }   
        }

        $dget = $this->input->get('ids');
        $aarrD = [];
        $ids = explode("-", $dget);
        foreach ($ids as $id) {
            if ($id != '') {
                $aarrD[] = $id;
            }
        }

        $ifsIds = $this->input->get('idInstituciones');
        $aarrifs = [];
        $ids = explode("-", $ifsIds);
        foreach ($ids as $id) {
            if ($id != '') {
                $aarrifs[] = $id;
            }
        }

        $pagoIds = $this->input->get('pago');
        $aarrpago = [];
        $ids = explode("-", $pagoIds);
        foreach ($ids as $id) {
            if ($id != '') {
                $aarrpago[] = $id;
            }
        }

        $params['fInstitucionesHijos'] =  $aarrD;
        $params['pago'] = $aarrpago;
        $params['idInstituciones'] = $aarrifs;

        

        $estudios = $this->estudio_model->getEstudios($params);


        $cc = $this->estudio_model->getCicloEscolarById($params['id_ciclo_escolar']);
        $nombre_archivo = 'reporte-solicitudes-' . $cc->ciclo_escolar . '.xls';
        header('Content-Disposition: attachment; filename=' . $nombre_archivo);
        header('Content-type: application/force-download');
        header("Content-Type: application/vnd.ms-excel");
        //header('Content-Transfer-Encoding: binary');
        header('Pragma: public');
        print "\xEF\xBB\xBF";

        $data['ciclo_escolar'] = $cc;
        $data['estudios'] = $estudios;
        $html = mb_convert_encoding($this->load->view('excel', $data, true), 'HTML-ENTITIES', 'UTF-8');
        echo $html;
    }

    public function pdf()
    {
        $this->load->library('pdf');
        $html = $this->load->view('index', [], true);
        return;
        file_put_contents("storage/test.pdf", $this->pdf->createPDF($html, 'mypdf', false));
        //file_put_contents("/tmp/test.pdf", $this->pdf->createPDF('/hola', $html, 'mypdf', false));
    }

    public function solicitudesEstudiosPDF()
    {
        $data = json_decode(file_get_contents('php://input'), true);

        $estudios = $this->estudio_model->getEstudios($data);
        $this->load->library('pdf');
        //$estudio = $this->estudio_model->getEstudioDetalle($id, '0');
        $data['ciclo_escolar'] = $this->estudio_model->getCicloEscolarById($data['id_ciclo_escolar']);
        $data['estudios'] = $estudios;

        $html = mb_convert_encoding($this->load->view('reporte-solicitudes', $data, true), 'HTML-ENTITIES', 'UTF-8');

        $nombre_archivo = 'reporte-solicitudes.pdf';
        $this->pdf->createPDF($html, $nombre_archivo, true);
    }

    public function download($archivo)
    {
        header("Content-type: application/pdf");
        $this->load->helper('download');
        $pth = file_get_contents("storage/" . $archivo);
        $nme = $archivo;
        force_download($nme, $pth);
    }
}
