<?php

defined('BASEPATH') OR exit('No direct script access allowed');
header('Content-Type: application/json');

class Grado extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function create() {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->grado_model->create($data));
    }

    public function get($id) {
        echo json_encode($this->grado_model->get($id));
    }
    
    public function getByNombrePost() {
        $data = json_decode(file_get_contents('php://input'), true);
        $nombre = $data['nombre_grado'];
        $id_grupo = $data['id_grupo'];
        echo json_encode($this->grado_model->getByNombre($nombre, $id_grupo));
    }

    public function getListaGradosByClaveIns() {
        $data = json_decode(file_get_contents('php://input'), true);
        $clave_institucion = $data['clave_institucion'];
        $id_grupo = $data['id_grupo'];
        echo json_encode($this->grado_model->getListaGradosByClaveIns($clave_institucion, $id_grupo));
    }
    
    public function delete($id) {
        echo json_encode($this->grado_model->delete($id));
    }

    public function getAll() {
        echo json_encode($this->grado_model->getAll());
    }
    
    public function update() {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->grado_model->update($data));
    }

}
