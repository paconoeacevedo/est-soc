<?php

defined('BASEPATH') OR exit('No direct script access allowed');
header('Content-Type: application/json');

class Colonia extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function create() {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->colonia_model->create($data));
    }

    public function get($id) {
        echo json_encode($this->colonia_model->get($id));
    }
    
    public function delete($id) {
        echo json_encode($this->colonia_model->delete($id));
    }

    public function getAll() {
        echo json_encode($this->colonia_model->getAll());
    }
    
    public function getZonas() {
        echo json_encode($this->colonia_model->getZonas());
    }

    public function update() {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->colonia_model->update($data));
    }

    public function getColoniasPorZona($zona) {
        echo json_encode($this->colonia_model->getColoniasPorZona($zona));
    }

}
