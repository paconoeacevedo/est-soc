<?php
defined('BASEPATH') or exit('No direct script access allowed');
header('Content-Type: application/json');
class Estudio extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
    }

    public function uploadFoto($id_estudio, $papa)
    {
        $json = array();

        $config['upload_path']          = 'storage/fotos/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['encrypt_name']                 = TRUE;

        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        if (!$this->upload->do_upload('file')) {
            $json = array('error' => true, 'message' => $this->upload->display_errors());
        } else {
            $upload_details = $this->upload->data();

            $this->db->where('id_estudio', $id_estudio);
            if($papa == 1){
                $this->db->set('foto_papa', $upload_details['file_name']);
            }
            if($papa == 2){
                $this->db->set('foto_mama', $upload_details['file_name']);
            }
        
            $this->db->update('estudio');

            $json = array('success' => true, 'message' => 'File transfer completed', 'newfilename' => $upload_details['file_name']);
        }

        echo json_encode($json);

    }

    public function testEmail()
    {
        $mail = new EmailV2();
        $data = array();

        $user['username'] = 'usernamee';
        $user['email'] = 'email';
        $user['temp_password'] = 'temp_password';
        $data['inssue'] = 'Test mail';
        $message = 'Estimado(a) <strong> : </strong>';
        $message .= "<br><br><br>Los administradores de lo han dado de alta para que pueda acceder al sistema de Est-Soc.";
        $message .= "<br><br>Datos para acceder al sistema:";
        $message .= "<br><a href='" . URL_FRONT . "'>" . URL_FRONT . "</a>";
        $message .= "<br>Usuario: <strong> " . $user['username'] . " ó " . $user['email'] . " </strong>";
        $message .= "<br>Contraseña: <strong>" . $user['temp_password'] . "</strong>";
        $message .= "<p><strong>Le recomendamos que una vez que entre al sistema cambie su contraseña y la recuerde.</strong></p>";

        $data['body'] = $message;
        $data['to'] = array();
        $data['cc'] = array();
        $data['cco'] = array();


        $emails = array();
        $emails[] = array(
            'email' => 'fnacevedo92@gmail.com',
            'name' => 'FNAR'
        );

        $data['to'] = array_merge($emails);
        return $mail->send($data);
    }

    public function getGrupo($id)
    {
        $this->db->where('id_institucion', $id);
        return $this->db->get('institucion')->row();
    }

    public function getEstudioFromToken($token)
    {
        $response = new stdClass();
        $estudio = $this->estudio_model->getEstudioFromToken($token);
        if ($estudio) {
            //validar token
            $fecha_token = $estudio->vigencia_token;
            $hoy = date('Y-m-d');
            $response->status = 200;
            if ($hoy > $fecha_token) {
                $response->token_vencido = true;
            } else {
                $response->token_vencido = false;
            }
            $response->status = 200;
            $estudio->familia = $this->estudio_model->getFamilia($estudio->id_familia);
            $response->data = $estudio;
        } else {
            $response->status = 204;
            $response->msg = 'no data found';
        }
        echo json_encode($response);
    }

    public function getFamilias()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $total_registros = $this->estudio_model->getFamiliasCount($data);
        $numberByPage = 7;
        if ($total_registros < $numberByPage) {
            $paginas = 1;
        } else {
            $paginas = $total_registros / $numberByPage;
        }
        $page = 0;
        $pageAux = $data['page'];
        if ($pageAux != 0) {
            $page = $pageAux * $numberByPage;
        }
        $data['limit'] = $numberByPage;
        $data['page'] = $page;
        $result = new stdClass();
        $result->total_rows = $total_registros;
        $result->number_of_pages = round($paginas);
        $result->current_page = $pageAux;
        $result->rows_page = $data['limit'];
        $result->data = $this->estudio_model->getFamilias($data);
        echo json_encode($result);
        //echo json_encode($this->estudio_model->getFamilias($data));
    }

    public function reindexarFolios()
    {
        $this->estudio_model->reindexarFolios();
    }

    public function generarCaratulas()
    {
        $this->estudio_model->generarCaratulas();
    }

    public function obtenerFolio()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->obtenerFolio($data));
    }

    public function actualizarComentarios()
    {
        $comentarios = json_decode(file_get_contents('php://input'), true);
        foreach ($comentarios as $c) {
            $this->db->where('id_comentario_familia', $c['id_comentario_familia']);
            $this->db->set('posicion', $c['posicion']);
            $this->db->update('comentario_familia');
        }
        echo json_encode(true);
    }


    public function saveFamilia()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $familia = $this->estudio_model->saveFamilia($data);
        $id_familia = $familia['id_familia'];

        /*$nombre=  explode(" ", $data['familia']);
        $result1 = substr($nombre[0], 0, 2);
        $result2 = substr($nombre[1], 0, 2);
        
        $clave=$result1."-".$result2."-".$id_familia;
        
        $data_update['id_familia']=$id_familia;
        $data_update['clave_familia']=$clave;
        */
        //$this->estudio_model->updateFamilia($data_update);

        echo json_encode(array('id_familia' => $id_familia));
    }

    public function saveEstudioInstitucion()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $this->estudio_model->saveEstudioInstitucion($data);
        echo json_encode(array('status' => '200'));
    }

    public function updateFamilia()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->updateFamilia($data));
    }

    public function getFamilia($id)
    {
        echo json_encode($this->estudio_model->getFamilia($id));
    }

    function RandomStringGenerator($n)
    {
        // Variable which store final string 
        $generated_string = "";

        // Create a string with the help of  
        // small letters, capital letters and 
        // digits. 
        $domain = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

        // Find the length of created string 
        $len = strlen($domain);

        // Loop to create random string 
        for ($i = 0; $i < $n; $i++) {
            // Generate a random index to pick 
            // characters 
            $index = rand(0, $len - 1);

            // Concatenating the character  
            // in resultant string 
            $generated_string = $generated_string . $domain[$index];
        }

        // Return the random generated string 
        return $generated_string;
    }

    public function saveEstudio()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $id_estudio = 0;
        $id_institucion_familia = $data['id_institucion_familia'];
        $ins = $this->estudio_model->getInstitucion($data['id_institucion_familia']);
        //$clave_institucion = $data['clave_institucion'];
        $clave_institucion = $ins->clave_institucion;
        unset($data['clave_institucion']);

        $token = strtoupper($this->RandomStringGenerator(25));
        $data['token_acceso'] = $token;
        $fecha_actual = date('Y-m-d');
        $expiracion_token = date('Y-m-d', strtotime($fecha_actual . '+ 21 days'));
        $data['vigencia_token'] = $expiracion_token;
        $data['nums_accesos'] = 0;

        $params['id_ciclo_escolar'] = $data['id_ciclo_escolar'];
        $params['pago'] = $data['pago'];
        $params['id_institucion'] = $data['id_institucion_familia'];
        $folio_pago = $this->estudio_model->obtenerFolio($params);
        $data['folio_estudio_pago'] = $folio_pago;

        $estudio = $this->estudio_model->saveEstudio($data);
        $id_estudio = $estudio['id_estudio'];
        $data_update['id_estudio'] = $id_estudio;
        $clave = $clave_institucion . "-" . $id_estudio;
        //$clave=date('Y').'-'.$clave;

        $cat = $this->estudio_model->getCicloEscolarObj();
        $strCat = explode("-", $cat->ciclo_escolar);
        $clave = $strCat[0] . '-' . $clave;
        $data_update['folio_estudio'] = $clave;

        $this->estudio_model->updateEstudio($data_update);

        //enviar correo
        $familia = $this->estudio_model->getFamilia($data['id_familia']);

        $mail = new EmailV2();
        $data = array();

        $url = URL_FRONT . '/auto-captura/' . $token . '/init';
        $user['username'] = 'usernamee';
        $user['email'] = 'email';
        $user['temp_password'] = 'temp_password';
        $data['inssue'] = 'Envio de acceso de solicitud de estudio socioeconómico.';

        $message = '';
        $message .= '<p>Estimada Familia: <strong>' . $familia->familia . '</strong></p>';
        $insigna = ["1", "2", "3", "4", "5", "6", "7", "8"]; //triana encino
        if (in_array($id_institucion_familia, $insigna)) {
            $data['id_grupo'] = "9";
            $message .= $this->getMensajeTriana();
        } else {
            $iter = ["11", "12", "13", "14", "15", "16"]; //iter
            if (in_array($id_institucion_familia, $iter)) {
                $data['id_grupo'] = "10";
                $message .= $this->getMensajeIter();
            }
        }

        $message .= "<h3>Liga de acceso a la solicitud:</h3>";
        $message .= "<h2><a href='" . $url . "'>" . $url . "</a></h2>";

        $data['body'] = $message;
        $data['to'] = array();
        $data['cc'] = array();
        $data['cco'] = array();

        $emails = array();
        $emails[] = array(
            'email' => $familia->correo_acceso,
            'name' => 'FAM: ' . $familia->familia
        );

        $data['to'] = array_merge($emails);
        $mail->send($data);
        echo json_encode(array('id_estudio' => $id_estudio, 'folio_estudio_pago' => $folio_pago));
    }

    function getMensajeTriana()
    {
        $message = '';
        $message .= '<p>
            Buscando sistematizar el proceso de solicitud de beca y atendiendo a la consciencia de disminuir el uso del papel, ya no será necesario imprimir la solicitud de beca, bastará con llenar la información en la liga de captura que se adjunta.
        </p>';

        $message .= '<p>
           <strong>Esta liga es intransferible, queda bajo su responsabilidad el no compartirla, 
           porque es transferencia de información y el reenviarla causará baja del sistema de becas.</strong>
        </p>';

        $message .= '<p>
            El enlace a la liga tiene una <strong>vigencia de 7 días</strong> a partir de la fecha de recepción en su correo,
            posterior a esta fecha, ya no se permitirá el acceso para su llenado.
        </p>';

        $message .= '<p>Para tener acceso al llenado de la solicitud, le pedimos ratificar la aceptación al Reglamento General de Becas de Grupo Educativo Insignia y Avisos de Privacidad; una vez leído y aceptado tendrá acceso a la captura donde aparecerá la primera pantalla con los datos generales de la familia, favor de revisar y en caso necesario, capturar o hacer las modificaciones correspondientes. A continuación, se deberán llenar todas las pestañas del lado izquierdo; para ingresar, bastará hacer click con el botón izquierdo del mouse. Es indispensable dar guardar en cada una de las pestañas.</p>';

        $message .= '<p>
            Por último, para enviar la solicitud, se debe dar click en la última pestaña donde dice “Finalizar y enviar solicitud”.
        </p>';

        $message .= '<p>
            Una vez terminada la captura de la información y enviada la solicitud, será necesario que nos haga llegar al correo electrónico: 
        </p>';

        $message .= '<h3 style="text-align:center; background-color: yellow;">documentosbeca.insignia@gmail.com,</h3>';

        $message .= '
        la siguiente documentación (escaneada o en fotografía) <strong>en un solo archivo PDF</strong>, 
        (<span style="font-style: italic">como título del asunto deberá venir nombre de la familia</span>, ejemplo GARCIA GONZALEZ), en el mismo orden que a continuación se presenta
        ';
        
        $message .= '
        <ol>
            <li>Comprobante de ingresos (recibos de nómina semanales, quincenales, mensuales y/o carta de ingresos mensuales expedida por contador con número de cédula profesional)</li>
               
            <table style="margin:auto; width:80%; border: 1px solid black; border-collapse: collapse;">
                <tr style="text-align:center;  border: 1px solid black; border-collapse: collapse;">
                    <th style="width:15%;  border: 1px solid black; border-collapse: collapse;">Tipo de Actividad Económica</th>
                    <th style="width:50%;  border: 1px solid black; border-collapse: collapse;">COMPROBANTE DE INGRESOS<br>
                    de acuerdo a su actividad económica</th>
                </tr>
                <tr>
                    <td style="text-align:center;  border: 1px solid black; border-collapse: collapse;"><strong>Empleado</strong></td>
                    <td style="text-align:center;  border: 1px solid black; border-collapse: collapse;">Copia de los recibos semanales, quincenales o mensuales de nómina.</td>
                </tr>
                <tr>
                    <td style="text-align:center;  border: 1px solid black; border-collapse: collapse;"><strong>Honorarios</strong></td>
                    <td style="text-align:center;  border: 1px solid black; border-collapse: collapse;">Copia de la última declaración anual, declaraciones de pagos provisiones de los dos últimos meses, certificación de ingresos obtenidos por actividades empresariales, de servicio o consulta independiente, expedido por contador público; incluir copia de la cédula profesional del mismo. </td>
                </tr>
                <tr>
                    <td style="text-align:center;  border: 1px solid black; border-collapse: collapse;"><strong>Empresario</strong></td>
                    <td style="text-align:center;  border: 1px solid black; border-collapse: collapse;">Copia de la última declaración anual, declaraciones de pagos provisiones de los dos últimos meses, certificación de ingresos obtenidos por actividades empresariales, de servicio o consulta independiente, expedido por contador público; incluir copia de la cédula profesional del mismo. </td>
                </tr>
                <tr>
                    <td style="text-align:center;  border: 1px solid black; border-collapse: collapse;"><strong>Pensionado</strong></td>
                    <td style="text-align:center;  border: 1px solid black; border-collapse: collapse;">Copia simple de los recibos de pensión de los últimos dos meses.</td>
                </tr>
                <tr>
                    <td style="text-align:center;  border: 1px solid black; border-collapse: collapse;"><strong>Desempleado</strong></td>
                    <td style="text-align:center;  border: 1px solid black; border-collapse: collapse;">Constancia de la última empresa, negocio o institución donde prestó sus servicios indicando el tiempo de trabajo con fechas, motivo de separación, último sueldo percibido, monto de liquidación.</td>
                </tr>
                <tr>
                    <td style="text-align:center;  border: 1px solid black; border-collapse: collapse;"><strong>Inversionista</strong></td>
                    <td style="text-align:center;  border: 1px solid black; border-collapse: collapse;">Estados de cuenta de bancos, recibo de arrendamiento de los bienes muebles e inmuebles. </td>
                </tr>
            </table>
            
            <li>Comprobantes de <strong>pagos de servicios</strong> (luz, agua, teléfono, celular, servicio de TV de paga, cuotas de mtto. de coto, etc)</li>
            <li>Comprobante de pago de <strong>rentas o créditos hipotecarios</strong> (en caso de que aplique)</li>
            <li>Comprobante de pago de <strong>créditos o arrendamiento de automóvil</strong> (en caso de que aplique)</li>
            <li>Comprobante de <strong>pago de seguros</strong> (autos, gastos médicos, vida, fideicomisos, en caso de que aplique)</li>
            <li>Comprobantes de <strong>pagos</strong> extra que haya reportado en el estudio (colegiaturas de otras instituciones, clases particulares, préstamos, club deportivo, gastos médicos extraordinarios, etc.)</li>
            <li>Copia del <strong>estado de cuenta de créditos comerciales y/o la tarjeta de crédito</strong> de los últimos dos meses.</li>
            <li>Si desea ampliar la información, podrá anexar una carta de máximo una cuartilla.</li>
        </ol>
        ';

        $message .= '<p>A diferencia de otros ciclos, en el proceso de trámite de beca del ciclo 24 - 25, no se llevará acabo visita – entrevista, si no que  una vez recibida la información tanto de la solicitud como de la documentación, se hará llegar a la institución para que el Consejo de becas tome una decisión.</p>';


        $message .= '<p>
        <strong>NOTA: Es importante resaltar que, de no COMPLETAR la información solicitada, se considerará el proceso de solicitud de beca INCONCLUSO.</strong>
        </p>';

        $message .= '<p>
        Si tiene alguna duda, favor de comunicarse al Despacho al teléfono 449 174-7414.
        </p>';

        $message .= '<p>
        La resolución del Comité de Becas se dará a través de la institución a la que solicite la beca.
        </p>';

        $message .= '<p>
        A partir de esta fecha, si ustedes no han sido notificados, favor de consultar con su directora de sección para conocer su resultado.</p>';
        
        return $message;
    }

    function getMensajeIter()
    {
        $message = '';
        $message .= '<p>
            Buscando sistematizar el proceso de solicitud de becas y atendiendo a la consciencia de disminuir el uso del papel, no será necesario
            imprimir la solicitud de beca, bastará con llenar la información en la liga de captura que se adjunta.
        </p>';

        $message .= '<p>
            Esta liga es intransferible, queda bajo su responsabilidad el no compartirla, porque es transferencia de información y 
            reenviarla causará baja del sistema de becas.
        </p>';

        $message .= '<p>
            El enlace a la liga tiene una vigencia de 15 días a partir de la fecha de recepción en su correo electrónico, posterior a esta fecha, 
            ya no se permitirá el acceso para su llenado.
        </p>';

        $message .= '<p>
            Para tener acceso al llenado de la solicitud, le pedimos ratificar la aceptación de Aviso de Privacidad y el 
            Reglamento de Becas de GRUPO ITER, una vez leído y aceptado tendrá acceso a la captura.
        </p>';

        $message .= '<p>
            En dicha liga aparecerá la primera pantalla con os datos generales de la familia, favor de revisar y en caso de ser necesario, 
            capturar o hacer las modificaciones correspondientes. A continuación se deberán llenar todas las pestañas de lado izquierdo; 
            para ingresar bastará con hacer click con el botón izquierdo del mouse. Es indispensable guardar la información que vaya capturando 
            en cada una de las pestañas.
        </p>';

        $message .= '<p>
            Por último para enviar la solicitud se debe dar click en la última parte donde dice “Finalizar y enviar solicitud”
        </p>';

        $message .= '<p>
            Una vez terminada la captura de la información y enviada la solicitud, será necesario que nos haga llegar al correo electrónico 
            (como título del asunto deberá venir nombre de la familia, ejemplo GARCIA GONZALEZ): 
            <strong>estudios.giter.hrw@gmail.com</strong>, la siguiente documentación (scaneada o fotografía en un solo archivo PDF,
            con nombre de familia, ejemplo GARCIA GONZALEZ) en el mismo orden que a continuación se presenta:
        </p>';

        $message .= '
        <ol>
            <li>Comprobante de ingresos (recibos  de nómina semanales, quincenales, mensuales y/o carta de ingresos expedida por contador 
            con número de cédula profesional por ingresos mensuales)</li>
            <li>Carta de renuncia o liquidación (en caso de que aplique)</li>
            <li>Comprobantes de pagos de servicios (luz, agua, teléfono, celular, servicio de TV de paga, gas, mtto. cotos, etc)</li>
            <li>Comprobante de pago de créditos hipotecarios (en caso de que aplique)</li>
            <li>Comprobante de pago de renta (en caso de que aplique)</li>
            <li>Comprobante de  pago de créditos de automóvil (en caso de que aplique)</li>
            <li>Comprobante de pago de seguros (autos, gastos médicos, vida, fideicomisos, en caso de que aplique)</li>
            <li>Comprobante de pago de colegiaturas que no sean de Grupo ITER</li>
            <li>Comprobantes de pagos extra que haya reportado en el estudio</li>
            <li>Si desea ampliar la información, podrá anexar una carta de máximo una cuartilla</li>
        </ol>
        ';

        $message .= '<p>
            Una vez recibida la información tanto de la solicitud como de la documentación, el despacho se pondrá en contacto con usted(es)
            a los teléfonos registrados en la plataforma (importante registrar celulares), para agendar una entrevista virtual en la cual 
            se ampliará la información y se corroborará la misma
        </p>';

        $message .= '<p>
            Si tiene cualquier duda, favor de comunicarse al despacho HR WISE al teléfono 449 1747414
        </p>';

        $message .= '<p>
            La resolución del Comité de Becas se dará a través de la institución a la que
            solicite la beca.
        </p>';

        $message .= '<p>
            A partir de esta fecha, si ustedes no han sido notificados, favor de consultar con su
            directora de sección para conocer su resultado.
        </p>';

        return $message;
    }

    public function reenviarLigaCaptura()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $estudio = $this->estudio_model->getEstudioDetalle($data['id_estudio'], '0');
        $id_institucion_familia = $estudio->familia->id_institucion;
        $mail = new EmailV2();
        $data = array();

        $url = URL_FRONT . '/auto-captura/' . $estudio->token_acceso . '/init';
        $user['username'] = 'usernamee';
        $user['email'] = 'email';
        $user['temp_password'] = 'temp_password';
        $data['inssue'] = 'Envio de acceso de solicitud de estudio socioeconómico.';
        $message = '';
        $message .= '<p>Estimada Familia: <strong>' . $estudio->familia->familia . '</strong></p>';
        $insigna = ["1", "2", "3", "4", "5", "6", "7", "8"]; //triana encino
        if (in_array($id_institucion_familia, $insigna)) {
            $data['id_grupo'] = "9";
            $message .= $this->getMensajeTriana();
        } else {
            $iter = ["11", "12", "13", "14", "15", "16"]; //iter
            if (in_array($id_institucion_familia, $iter)) {
                $data['id_grupo'] = "10";
                $message .= $this->getMensajeIter();
            }
        }
        $message .= "<h3>Liga de acceso a la solicitud:</h3>";
        $message .= "<h2><a href='" . $url . "'>" . $url . "</a></h2>";
        $data['body'] = $message;
        $data['to'] = array();
        $data['cc'] = array();
        $data['cco'] = array();

        $emails = array();
        $emails[] = array(
            'email' => $estudio->familia->correo_acceso,
            'name' => 'FAM: ' . $estudio->familia->familia
        );

        $data['to'] = array_merge($emails);
        $mail->send($data);

        echo json_encode($estudio);
    }

    public function updateEstudio()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->updateEstudio($data));
    }

    public function sumarDias()
    {
        $this->db->where('id_ciclo_escolar', '6');
        $this->db->order_by('id_estudio', 'asc');
        //$this->db->limit(1);

        $estudios = $this->db->get('estudio')->result();
        foreach ($estudios as $estudio) {
            echo $estudio->id_estudio . '<br>';
            echo $estudio->vigencia_token . '<br>';
            $fechaadd = date("Y-m-d", strtotime($estudio->vigencia_token . "+ 15 days"));
            echo $fechaadd . '<br>';
            echo $estudio->fecha_estudio;
            $this->db->where('id_estudio', $estudio->id_estudio);
            $this->db->set('vigencia_token', $fechaadd);
            $r = $this->db->update('estudio');
            echo $r . '<br>';
        }
    }

    public function sumarDiasByEstudio($id_estudio)
    {
        $this->db->where('id_estudio', $id_estudio);
        $estudio = $this->db->get('estudio')->row();
        //foreach ($estudios as $estudio) {
        //echo $estudio->id_estudio . '<br>';
        //echo $estudio->vigencia_token . '<br>';
        $fechaadd = date("Y-m-d", strtotime($estudio->vigencia_token . "+ 21 days"));
        //echo $fechaadd . '<br>';
        //echo $estudio->fecha_estudio;
        $this->db->where('id_estudio', $estudio->id_estudio);
        $this->db->set('vigencia_token', $fechaadd);
        $r = $this->db->update('estudio');
        echo json_encode($r);
    }

    //public function getEstudios($tipoUsuario, $rolUsuario, $idUsuario, $idInstitucion, $familia) {
    public function getEstudios()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        //echo json_encode($this->estudio_model->getEstudios($tipoUsuario, $rolUsuario, $idUsuario, $idInstitucion, $familia));
        //echo json_encode($this->estudio_model->getEstudios($data));
        $total_registros = $this->estudio_model->getEstudiosCount($data);

        $numberByPage = 7;
        if ($total_registros < $numberByPage) {
            $paginas = 1;
        } else {
            $paginas = $total_registros / $numberByPage;
            if (is_float($paginas)) {
                $paginas = round($paginas, 1);
                $tmp = explode('.', $paginas);
                //echo $tmp[1];
                if ($tmp[1] < 5) {
                    $paginas = $paginas + 1;
                }
            }
        }

        $page = 0;
        $pageAux = $data['page'];
        if ($pageAux != 0) {
            $page = $pageAux * $numberByPage;
        }
        $data['limit'] = $numberByPage;
        $data['page'] = $page;
        $result = new stdClass();
        $result->total_rows = $total_registros;
        $result->number_of_pages = round($paginas);
        $result->current_page = $pageAux;
        $result->rows_page = $data['limit'];
        $result->data = $this->estudio_model->getEstudios($data);
        echo json_encode($result);
    }

    public function getEstudioDetalle($idEstudio, $idInstitucion)
    {
        $estudio = $this->estudio_model->getEstudioDetalle($idEstudio, $idInstitucion);
        if ($estudio) {
            //validar token
            $fecha_token = $estudio->vigencia_token;
            $hoy = date('Y-m-d');
            if ($hoy > $fecha_token) {
                $estudio->token_vencido = true;
            } else {
                $estudio->token_vencido = false;
            }
            /*$response->status = 200;
            $estudio->familia = $this->estudio_model->getFamilia($estudio->id_familia);
            $response->data = $estudio;*/
        }
        echo json_encode($estudio);
    }
    /*hijos*/
    public function saveHijo()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->saveHijo($data));
    }

    public function deleteHijo()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->deleteHijo($data));
    }

    public function updateHijo()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->updateHijo($data));
    }

    /*dependientes*/
    public function saveDependiente()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->saveDependiente($data));
    }

    public function deleteDependiente()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->deleteDependiente($data));
    }

    public function updateDependiente()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->updateDependiente($data));
    }

    /*motivos*/
    public function saveMotivo()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->saveMotivo($data));
    }

    public function deleteMotivo()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->deleteMotivo($data));
    }

    public function updateMotivo()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->updateMotivo($data));
    }

    /*vehiculos*/
    public function saveVehiculo()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->saveVehiculo($data));
    }

    public function deleteVehiculo()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->deleteVehiculo($data));
    }

    public function updateVehiculo()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->updateVehiculo($data));
    }

    /*propiedades*/
    public function savePropiedad()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->savePropiedad($data));
    }

    public function deletePropiedad()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->deletePropiedad($data));
    }

    public function updatePropiedad()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->updatePropiedad($data));
    }

    public function cancelEstudioInstitucion($idEstudioInstitucion)
    {
        $this->db->where('borrado', 0);
        $this->db->where('id_estudio_institucion', $idEstudioInstitucion);
        $q = $this->db->get('estudios_instituciones')->row();

        $idEstudio = $q->id_estudio;

        /*$this->db->where('borrado', 0);
        $this->db->where('id_estudio', $idEstudio);
        $q2 = $this->db->get('estudio')->row();*/

        $this->db->where('borrado', 0);
        $this->db->where('id_estudio', $idEstudio);
        $this->db->where('estatus', 1);
        //$this->db->set('fecha_modificacion', date("Y-m-d H:i:s"));
        $this->db->where('id_institucion !=', $q->id_institucion);
        $q = $this->db->get('estudios_instituciones');

        if ($q->num_rows() == 0) {
            $this->db->where('id_estudio', $idEstudio);
            $this->db->set('borrado', 1);
            $this->db->set('fecha_modificacion', date("Y-m-d H:i:s"));
            $this->db->update('estudio');

            $this->db->where('id_estudio_institucion', $idEstudioInstitucion);
            $this->db->set('estatus', 2);
            $this->db->set('fecha_modificacion', date("Y-m-d H:i:s"));
            $this->db->update('estudios_instituciones');
        } else {
            $this->db->where('id_estudio_institucion', $idEstudioInstitucion);
            $this->db->set('estatus', 2);
            $this->db->set('fecha_modificacion', date("Y-m-d H:i:s"));
            $this->db->update('estudios_instituciones');
        }
    }

    public function saveIngresos()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->saveIngresos($data));
    }

    public function updateIngresos()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->updateIngresos($data));
    }

    public function savePasivo()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->savePasivo($data));
    }

    public function updatePasivo()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->updatePasivo($data));
    }

    public function deletePasivo()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->deletePasivo($data));
    }

    public function savePapa()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->savePapa($data));
    }

    public function updatePapa()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->updatePapa($data));
    }

    public function saveEgresos()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->saveEgresos($data));
    }

    public function updateEgresos()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->updateEgresos($data));
    }

    public function saveDocumentos()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->saveDocumentos($data));
    }

    public function updateDocumentos()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->updateDocumentos($data));
    }

    public function saveEvaluacion()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->saveEvaluacion($data));
    }

    public function updateEvaluacion()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->updateEvaluacion($data));
    }

    public function getCicloEscolar()
    {
        echo json_encode($this->estudio_model->getCicloEscolar());
    }

    public function getEstatusCat()
    {
        echo json_encode($this->estudio_model->getEstatusCat());
    }

    public function getCicloEscolarCat()
    {
        echo json_encode($this->estudio_model->getCicloEscolarCat());
    }

    /*comentarios*/
    public function saveComentario()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->saveComentario($data));
    }

    public function deleteComentario()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->deleteComentario($data));
    }

    public function updateComentario()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        echo json_encode($this->estudio_model->updateComentario($data));
    }
}
