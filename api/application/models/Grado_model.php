<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Grado_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function create($data)
    {
        $this->db->insert('grado', $data);
        return $this->get($this->db->insert_id());
    }

    public function get($id)
    {
        $this->db->where('borrado', 0);
        $this->db->where('id_grado', $id);
        return $this->db->get('grado')->row();
    }

    public function getByNombre($grado, $id_grupo)
    {
        if ($id_grupo == "9") {
            $this->db->where('borrado', 0);
            $this->db->where('nombre_grado', $grado);
            return $this->db->get('grado')->row();
        }
        if ($id_grupo == "10") {
            $this->db->where('borrado', 0);
            $this->db->where('id_grado', $grado);
            return $this->db->get('grado')->row();
        }
    }

    public function update($data)
    {
        $this->db->where('id_grado', $data['id_grado']);
        unset($data['id_grado']);
        return $this->db->update('grado', $data);
    }

    public function delete($id)
    {
        $this->db->where('id_grado', $id);
        $this->db->set('borrado', 1);
        return $this->db->update('grado');
    }

    public function getAll()
    {
        $this->db->where('borrado', 0);
        return $this->db->get('grado')->result();
    }

    public function getListaGradosByClaveIns($clave, $id_grupo)
    {
        if ($id_grupo == "9") {
            $this->db->where('clave_institucion', $clave);
            $i = $this->db->get('institucion')->row();

            $es_k = false;

            if ($i->letra_identificacion == 'K') {
                $es_k = true;
            }

            if ($es_k) {
                $this->db->where('kinder', 1);
                $this->db->join('institucion i', 'i.id_institucion = grado.id_institucion');
                $this->db->where('i.id_grupo', $id_grupo);
                return $this->db->get('grado')->result();
            } else {
                $this->db->where('id_institucion', $i->id_institucion);
                $this->db->where('kinder', 0);
                return $this->db->get('grado')->result();
            }
        }

        if ($id_grupo == "10") {
            $this->db->where('id_institucion', $clave);
            $i = $this->db->get('institucion')->row();
            $es_k = false;

            if ($i->letra_identificacion == 'K') {
                $es_k = true;
            }

            if ($es_k) {
                //$this->db->where('kinder', 1);
                $this->db->where('id_institucion', $i->id_institucion);
				$this->db->order_by(1);
                return $this->db->get('grado')->result();
            } else {
                $this->db->where('id_institucion', $i->id_institucion);
				$this->db->order_by(1);
                return $this->db->get('grado')->result();
            }
        }
    }
}
