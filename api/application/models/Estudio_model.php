<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Estudio_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function obtenerFolio($data)
    {
        $strCat = explode("-", $this->getCicloEscolarById($data['id_ciclo_escolar'])->ciclo_escolar);

        $es_insigna = false;
        $grupo_id = null;
        $letra = null;
        if (isset($data['id_institucion'])) {
            $this->db->where('id_institucion', $data['id_institucion']);
            $i = $this->db->get('institucion')->row();
            if (isset($i)) {
                $grupo_id = $i->id_grupo;
                $letra = $i->letra_identificacion;
            }
        }
        if (isset($grupo_id) && $grupo_id != null) {
            if ($grupo_id == "9") {
                $es_insigna = true;
            }
        }

        if ($es_insigna === false) {
            $this->db->where('id_institucion', $data['pago']);
            $i = $this->db->get('institucion')->row();
            if (isset($i)) {
                $grupo_id = $i->id_grupo;
                $letra = $i->letra_identificacion;
            }
            $consecutivo = $this->getConsecutivoEstudio($data['id_ciclo_escolar'], $data['pago'], $data['id_institucion']);
            $folio = $strCat[0] . '-' . substr($letra, 0, 1)    . '-' . $consecutivo;
            return $folio;
        } else {
            $consecutivo = $this->getConsecutivoEstudio($data['id_ciclo_escolar'], $data['pago'], $data['id_institucion']);
            $folio = $strCat[0] . '-' . substr($data['pago'], 0, 1)    . '-' . $consecutivo;
            return $folio;
        }
    }

    function getConsecutivoEstudio($id_ciclo_escolar, $if, $id_institucion)
    {
        $grupo_id = null;
        $es_insigna = false;
        if (isset($id_institucion)) {
            $this->db->where('id_institucion', $id_institucion);
            $i = $this->db->get('institucion')->row();
            if (isset($i)) {
                $grupo_id = $i->id_grupo;
            }
        }
        if (isset($grupo_id) && $grupo_id != null) {
            if ($grupo_id == "9") {
                $es_insigna = true;
            }
        }
        if (!$es_insigna) {
            if ($if == "11" || $if == "12") {
                $ciclo = $this->getCicloEscolarById($id_ciclo_escolar);
                $c = null;
                $this->db->where('id_ciclo_escolar', $ciclo->id_ciclo_escolar);
                $this->db->join('estudios_instituciones ei', 'estudio.id_estudio = ei.id_estudio');
                $this->db->where_in('ei.pago', ["11", "12"]);
                $c = $this->db->get('estudio')->num_rows() + 1;
            } else {
                $ciclo = $this->getCicloEscolarById($id_ciclo_escolar);
                $c = null;
                $this->db->where('id_ciclo_escolar', $ciclo->id_ciclo_escolar);
                $this->db->join('estudios_instituciones ei', 'estudio.id_estudio = ei.id_estudio');
                $this->db->where('ei.pago', $if);
                $c = $this->db->get('estudio')->num_rows() + 1;
            }



            $strL = strlen($c);
            $r = '';
            switch ($strL) {
                case 1:
                    $r = sprintf("00%d", $c);
                    break;
                case 2:
                    $r = sprintf("0%d", $c);
                    break;
                case 3:
                    $r = sprintf("%d", $c);
                    break;
            }
            return $r;
        }


        $ciclo = $this->getCicloEscolarById($id_ciclo_escolar);
        $c = null;
        switch ($if) {
            case "ENCINO":
                $this->db->where('id_ciclo_escolar', $ciclo->id_ciclo_escolar);
                $this->db->join('estudios_instituciones ei', 'estudio.id_estudio = ei.id_estudio');
                //$this->db->where('ei.id_institucion', '1');
                $this->db->where('ei.pago', 'ENCINO');
                $c = $this->db->get('estudio')->num_rows() + 1;
                break;
            case "TRIANA":
                $this->db->where('id_ciclo_escolar', $ciclo->id_ciclo_escolar);
                $this->db->join('estudios_instituciones ei', 'estudio.id_estudio = ei.id_estudio');
                //$this->db->where('ei.id_institucion', '2');
                $this->db->where('ei.pago', 'TRIANA');
                $c = $this->db->get('estudio')->num_rows() + 1;
                break;
            case "KV":
                $this->db->where('id_ciclo_escolar', $ciclo->id_ciclo_escolar);
                $this->db->join('estudios_instituciones ei', 'estudio.id_estudio = ei.id_estudio');
                //$this->db->where_in('ei.id_institucion', ['3', '5', '6', '7', '8']);
                $this->db->where('ei.pago', 'KV');
                $c = $this->db->get('estudio')->num_rows() + 1;
                break;
        }

        $strL = strlen($c);
        $r = '';
        switch ($strL) {
            case 1:
                $r = sprintf("00%d", $c);
                break;
            case 2:
                $r = sprintf("0%d", $c);
                break;
            case 3:
                $r = sprintf("%d", $c);
                break;
        }
        return $r;
    }



    function reindexarFolios()
    {
        $this->db->order_by('id_institucion', 'asc');
        $this->db->where_in('id_institucion', [1, 2, 3]);
        $ins = $this->db->get('institucion')->result();

        $ciclo = $this->getCicloEscolarById('6');
        $strCat = explode("-", $ciclo->ciclo_escolar);
        foreach ($ins as $i) {
            //buscar folios por cada ins
            if ($i->id_institucion === '3') {
                $this->db->where('id_ciclo_escolar', $ciclo->id_ciclo_escolar);
                $this->db->join('estudios_instituciones ei', 'estudio.id_estudio = ei.id_estudio');
                //$this->db->where_in('ei.id_institucion', [3, 5, 6, 7, 8]);
                $this->db->where('ei.pago', 'KV');
                $this->db->from('estudio');
                //$this->db->where('estudio.id_institucion_solicito', 'ei.id_institucion');
                $this->db->select('estudio.id_estudio, ei.id_estudio_institucion, ei.pago');
                $this->db->order_by('estudio.id_estudio', 'asc');
                $result = $this->db->get()->result();

                $index = 1;
                foreach ($result as $i) {
                    //actualizar de nuevo el folio
                    $strL = strlen($index);
                    $r = '';
                    switch ($strL) {
                        case 1:
                            $r = sprintf("00%d", $index);
                            break;
                        case 2:
                            $r = sprintf("0%d", $index);
                            break;
                        case 3:
                            $r = sprintf("%d", $index);
                            break;
                    }
                    $consecutivo = $r;

                    $folio = $strCat[0] . '-' . substr($i->pago, 0, 1)    . '-' . $consecutivo;
                    echo $folio . '<br>';
                    $this->db->where('id_estudio', $i->id_estudio);
                    $data['folio_estudio_pago'] = $folio;
                    $this->db->update('estudio', $data);

                    $this->db->where('id_estudio', $i->id_estudio);
                    $data1['num_recibo'] = $folio;
                    $this->db->update('estudios_instituciones', $data1);
                    $index++;
                }

                break;
            }
            $this->db->where('id_ciclo_escolar', $ciclo->id_ciclo_escolar);
            $this->db->join('estudios_instituciones ei', 'estudio.id_estudio = ei.id_estudio');
            //$this->db->where('ei.id_institucion', $i->id_institucion);
            if ($i->id_institucion == '1') {
                $this->db->where('ei.pago', 'ENCINO');
            }
            if ($i->id_institucion == '2') {
                $this->db->where('ei.pago', 'TRIANA');
            }

            $this->db->from('estudio');
            $this->db->select('estudio.id_estudio, ei.id_estudio_institucion, ei.pago');
            $this->db->order_by('estudio.id_estudio', 'asc');
            $result = $this->db->get()->result();

            $index = 1;
            foreach ($result as $i) {
                //actualizar de nuevo el folio
                $strL = strlen($index);
                $r = '';
                switch ($strL) {
                    case 1:
                        $r = sprintf("00%d", $index);
                        break;
                    case 2:
                        $r = sprintf("0%d", $index);
                        break;
                    case 3:
                        $r = sprintf("%d", $index);
                        break;
                }
                $consecutivo = $r;

                $folio = $strCat[0] . '-' . substr($i->pago, 0, 1)    . '-' . $consecutivo;
                echo $i->id_estudio . '->' . $folio . '<br>';


                $this->db->where('id_estudio', $i->id_estudio);
                $data['folio_estudio_pago'] = $folio;
                $this->db->update('estudio', $data);

                $this->db->where('id_estudio', $i->id_estudio);
                $data1['num_recibo'] = $folio;
                $this->db->update('estudios_instituciones', $data1);

                $index++;
            }
            echo '<br>';
        }
    }

    function generarCaratulas()
    {
        //buscar estudios
        $this->db->where('id_ciclo_escolar', '6');
        $this->db->where_in('id_estatus_estudio', [2, 12, 11]);
        $this->db->order_by('id_estudio', 'asc');
        $estudios = $this->db->get('estudio')->result();
        $response = [];
        foreach ($estudios as $estudio) {
            $response[] = array('id' => $estudio->id_estudio, 'caratula' => $this->caratula($estudio->id_estudio));
            //break; //test to one
        }
        echo json_encode($response);
    }

    public function caratula($id)
    {
        //header('Content-Type: application/json');
        $this->load->library('pdf');
        $estudio = $this->getEstudioDetalle($id, '0');
        $data['estudio'] = $estudio;
        $html = mb_convert_encoding($this->load->view('caratula', $data, true), 'HTML-ENTITIES', 'UTF-8');
        $nombre_archivo = $estudio->folio_estudio . '.pdf';
        $result = file_put_contents("storage/" . $nombre_archivo, $this->pdf->createPDF($html, 'mypdf', false));
        $response['file'] = $result;

        $datau['id_estudio'] = $estudio->id_estudio;
        $datau['archivo_caratula'] = $nombre_archivo;
        $response['update'] = $this->updateEstudio($datau);

        return $response;
        //echo json_encode($response);
    }

    public function getFamiliasCount($data)
    {
        $grupo_id = null;
        if (isset($data['id_institucion'])) {
            $this->db->where('id_institucion', $data['id_institucion']);
            $i = $this->db->get('institucion')->row();
            if (isset($i)) {
                $grupo_id = $i->id_grupo;
            }
        }
        if (isset($grupo_id) && $grupo_id != null) {
            $this->db->where('familia.id_grupo', $grupo_id);
        }

        $filterFamilia = $data['filtro_familia'];
        if ($filterFamilia != 'all') {
            $this->db->like('familia', $filterFamilia);
        }

        $this->db->join('institucion', 'institucion.id_institucion = familia.id_institucion');
        //$this->db->join('estudios_instituciones', 'institucion.id_institucion = estudios_instituciones.id_institucion');
        $this->db->order_by('familia.familia', 'ASC');
        $this->db->where('familia.borrado', 0);
        //$this->db->where('institucion.borrado', 0);
        //$this->db->from('familia');
        return $this->db->count_all_results('familia');
    }

    public function getFamilias($data)
    {
        $grupo_id = null;
        if (isset($data['id_institucion'])) {
            $this->db->where('id_institucion', $data['id_institucion']);
            $i = $this->db->get('institucion')->row();
            if (isset($i)) {
                $grupo_id = $i->id_grupo;
            }
        }
        if (isset($grupo_id) && $grupo_id != null) {
            $this->db->where('familia.id_grupo', $grupo_id);
        }
        $idInstitucion = $data['id_institucion'];
        $idInstituciones = $data['idInstituciones'];

        $filterFamilia = $data['filtro_familia'];
        $this->db->limit($data['limit'], $data['page']);
        if ($filterFamilia != 'all') {
            $this->db->like('familia', $filterFamilia);
        }

        $this->db->select('familia.*, institucion.id_institucion, institucion.clave_institucion, institucion.nombre_institucion');
        $this->db->join('institucion', 'institucion.id_institucion = familia.id_institucion');
        //$this->db->join('estudios_instituciones', 'institucion.id_institucion = estudios_instituciones.id_institucion');
        //$this->db->order_by('familia.familia', 'ASC');
        $this->db->order_by('familia.id_familia', 'ASC');
        $this->db->where('familia.borrado', 0);
        //$this->db->where('institucion.borrado', 0);
        $familias = $this->db->get('familia')->result();
        $array_familias = array();
        $i = 0;

        foreach ($familias as $familia) {
            $familia->estudio = $this->getEstudiosFamiliaInstitucion($familia->id_familia, 0, 0, $idInstituciones);
            array_push($array_familias, $familia);
            $i++;
        }
        return $array_familias;
    }

    //Familia
    public function saveEstudioInstitucion($data)
    {
        return $this->db->insert('estudios_instituciones', $data);
    }

    public function saveFamilia($data)
    {
        $data['fecha_registro'] = date("Y-m-d");
        $data['fecha_modificacion'] = date("Y-m-d H:i:s");
        $this->db->insert('familia', $data);
        $insert_id = $this->db->insert_id();
        return array('id_familia' => $insert_id);
    }

    public function updateFamilia($data)
    {
        $this->db->where('id_familia', $data['id_familia']);
        unset($data['id_familia']);
        if ($data['borrado'] === '0') {
            unset($data['borrado']);
        }
        $data['fecha_modificacion'] = date("Y-m-d H:i:s");
        return array('status', $this->db->update('familia', $data));
    }

    public function getFamilia($id)
    {
        $this->db->where('id_familia', $id);
        return $this->db->get('familia')->row();
    }

    public function getEstudioFromToken($token)
    {
        $this->db->where('token_acceso', $token);
        $data['fecha_modificacion'] = date("Y-m-d H:i:s");
        $data['leido'] = '1';
        $this->db->update('estudio', $data);

        $this->db->where('token_acceso', $token);
        return $this->db->get('estudio')->row();
    }

    //Estudio
    public function saveEstudio($data)
    {
        $data['fecha_estudio'] = date("Y-m-d");
        unset($data['pago']);
        unset($data['num_recibo']);
        $this->db->insert('estudio', $data);
        $insert_id = $this->db->insert_id();
        return array('id_estudio' => $insert_id);
    }

    public function updateEstudio($data)
    {
        $this->db->where('id_estudio', $data['id_estudio']);
        unset($data['id_estudio']);
        $data['fecha_modificacion'] = date("Y-m-d H:i:s");
        return array('status' => $this->db->update('estudio', $data));
    }

    public function getInstitucion($id)
    {
        $this->db->where('id_institucion', $id);
        return $this->db->get('institucion')->row();
    }

    public function getInstitucionByClave($clave)
    {
        $this->db->where('clave_institucion', $clave);
        return $this->db->get('institucion')->row();
    }

    public function getCompartido($id_estudio, $id_familia)
    {
        $response = new stdClass();
        $result = [];
        $iden = [];

        $hijos = $this->getHijosFamilia($id_familia, $id_estudio);
        $famila = $this->getFamilia($id_familia);
        $id_ins_familia = $famila->id_institucion;
        $instuticion = $this->getInstitucion($id_ins_familia);

        $compartida = false;

        foreach ($hijos as $hijo) {
            if ($hijo->solicita_beca == '1') {
                $cveI = strtoupper($hijo->institucion);
                if (!in_array($cveI, $result)) {
                    $result[] = $cveI;
                    if($instuticion->id_grupo == "9"){
                        $iden[] = $this->getInstitucionByClave($hijo->institucion)->letra_identificacion;
                    }
                    if($instuticion->id_grupo == "10"){
                        $iden[] = $this->getInstitucion($hijo->institucion)->letra_identificacion;
                    }
                }
            }
        }
        $clasificacion = '';
        $aux = '';
        $index = 1;
        $ii = 0;
        foreach ($result as $i) {
            if (count($result) == 1) {
                $aux .= $i;
                $clasificacion .= $iden[$ii];
                break;
            }
            $aux .= $i;
            $clasificacion .= $iden[$ii];
            if ($index < count($result)) {
                $aux .= '/';
                //$clasificacion .= '/';
            }
            $index++;
            $ii++;
            if ($instuticion->clave_institucion != $i) {
                $compartida = true;
            }
        }


        $response->instituciones = $result;
        $response->text_instituciones = $aux;
        $response->es_compartida = $compartida;
        $response->clasificacion = $clasificacion;
        $response->hijos = $hijos;

        return $response;
    }

    public function getEstudiosCount($params)
    {
        $grupo_id = null;
        if (isset($params['idInstitucion'])) {
            $this->db->where('id_institucion', $params['idInstitucion']);
            $i = $this->db->get('institucion')->row();
            if (isset($i)) {
                $grupo_id = $i->id_grupo;
            }
        }
        $tipoUsuario = $params['tipoUsuario'];
        $rolUsuario = $params['rolUsuario'];
        $idUsuario = $params['idUsuario'];
        $idInstitucion = $params['idInstitucion'];
        if (isset($params['filtroFamilia'])) {
            $filterFamilia = $params['filtroFamilia'];
        }
        $ciclos = array();
        $ciclos = $params['ciclos'];

        $this->db->select(
            'es.id_estudio, es.vigencia_token, es.leido, es.folio_estudio, es.archivo_caratula, es.finalizo_captura, cee.nombre as nombre_estatus, es.folio_estudio_pago,'
                . ''
                . 'fam.familia,'
                . 'fam.calle, fam.num_ext, fam.num_int, fam.colonia, fam.localidad, fam.municipio, fam.estado,'
                . 'es.fecha_estudio, fecha_entrevista, hora_entrevista, fecha_reagendar_entrevista, es.id_estatus_estudio, es_in.id_institucion, es.id_familia, es.id_usuario_asignado,'
                . 'es_in.id_estudio_institucion, es_in.estatus, c.id_colonia, c.nombre_colonia, c.zona, es_in.filtro_uno, es_in.filtro_dos, es_in.filtro_tres, '
                . 'ccc.id_ciclo_escolar, ccc.ciclo_escolar, in.id_grupo, es_in.pago as institucion_pago'
        );

        $this->db->from('estudio es');
        $this->db->join('estudios_instituciones es_in', 'es.id_estudio = es_in.id_estudio');
        $this->db->join('institucion in', 'es_in.id_institucion = in.id_institucion', 'left');
        $this->db->join('familia fam', 'es.id_familia = fam.id_familia');
        $this->db->join('cat_ciclo_escolar ccc', 'ccc.id_ciclo_escolar = es.id_ciclo_escolar');
        $this->db->join('colonia c', 'fam.id_colonia = c.id_colonia', 'left');
        $this->db->join('cat_estatus_estudios cee', 'cee.id_estatus = es.id_estatus_estudio');

        if (isset($grupo_id) && $grupo_id != null) {
            $this->db->where('fam.id_grupo', $grupo_id);
        }

        if ($tipoUsuario == '1' && $rolUsuario == '2') {
            $this->db->where('es.id_usuario_asignado', $idUsuario);
        }

        if (isset($params['pago']) && count($params['pago'])  == 0) {
            if ($tipoUsuario == '2' && $idInstitucion != '0' && count($params['idInstituciones']) == 0) {
                $this->db->where('es_in.id_institucion', $idInstitucion);
            }

            if (

                isset($params['idInstituciones']) && count($params['idInstituciones']) > 0
            ) {
                $this->db->where_in('es_in.id_institucion', $params['idInstituciones']);
            }
        }

        if (isset($params['pago']) && count($params['pago'])  > 0) {
            if ($tipoUsuario == '2' && $idInstitucion != '0' && count($params['idInstituciones']) == 0) {
                $this->db->where('es_in.id_institucion', $idInstitucion);
            }
            if (

                isset($params['idInstituciones']) && count($params['idInstituciones']) > 0
            ) {
                $this->db->where_in('es_in.id_institucion', $params['idInstituciones']);
            }
            $this->db->where_in('es_in.pago', $params['pago']);
        }

        if ($filterFamilia != 'all') {
            $this->db->like('fam.familia', $filterFamilia);
        }

        if ($tipoUsuario == '1') {
            $this->db->group_by('es.id_estudio');
        }

        if (count($ciclos) > 0) {
            $this->db->where_in('es.id_ciclo_escolar', $ciclos);
        }

        if (isset($params['id_estatus_estudio'])) {
            if ($params['id_estatus_estudio'] != '') {
                $this->db->where('es.id_estatus_estudio', $params['id_estatus_estudio']);
            }
        }

        if ($tipoUsuario == '1' && isset($params['id_usuario_asignado'])) {
            if ($params['id_usuario_asignado'] != '') {
                $this->db->where('es.id_usuario_asignado', $params['id_usuario_asignado']);
            }
        }

        if (isset($params['id_estatus_estudio'])) {
            if ($params['id_estatus_estudio'] != '') {
                $this->db->where('es.id_estatus_estudio', $params['id_estatus_estudio']);
            }
        }

        if (isset($params['zona'])) {
            if ($params['zona'] != '') {
                $this->db->where('c.zona', $params['zona']);
            }
        }
        if ($tipoUsuario == '1' && isset($params['id_grupo'])) {
            $this->db->where('fam.id_grupo', $params['id_grupo']);
        }

        $this->db->where('es.borrado', 0);
        $this->db->where('es_in.borrado', 0);
        $this->db->where('es_in.estatus', 1);
        $this->db->order_by('es.id_estudio');
        $this->db->order_by('es.folio_estudio_pago');
        return $this->db->count_all_results();
    }

    public function getEstudios($params)
    {
        $grupo_id = null;
        if (isset($params['idInstitucion'])) {
            $this->db->where('id_institucion', $params['idInstitucion']);
            $i = $this->db->get('institucion')->row();
            if (isset($i)) {
                $grupo_id = $i->id_grupo;
            }
        }

        if (isset($params['limit']) && isset($params['page'])) {
            $this->db->limit($params['limit'], $params['page']);
        }
        $tipoUsuario = $params['tipoUsuario'];
        $rolUsuario = $params['rolUsuario'];
        $idUsuario = $params['idUsuario'];
        $idInstitucion = $params['idInstitucion'];
        $filterFamilia = "";
        if (isset($params['filtroFamilia'])) {
            $filterFamilia = $params['filtroFamilia'];
        }
        $ciclos = array();
        if (isset($params['ciclos'])) {
            $ciclos = $params['ciclos'];
        }

        $this->db->select(
            'es.id_estudio, es.vigencia_token, es.leido, es.folio_estudio, es.archivo_caratula, es.finalizo_captura, cee.nombre as nombre_estatus, es.folio_estudio_pago,'
                . ''
                . 'fam.familia,'
                . 'fam.calle, fam.num_ext, fam.num_int, fam.colonia, fam.localidad, fam.municipio, fam.estado,'
                . 'es.fecha_estudio, fecha_entrevista, hora_entrevista, fecha_reagendar_entrevista, es.id_estatus_estudio, es_in.id_institucion, es.id_familia, es.id_usuario_asignado,'
                . 'es_in.id_estudio_institucion, es_in.estatus, c.id_colonia, c.nombre_colonia, c.zona, es_in.filtro_uno, es_in.filtro_dos, es_in.filtro_tres, '
                . 'ccc.id_ciclo_escolar, ccc.ciclo_escolar, in.id_grupo, fam.id_institucion as institucion_familia'
        );

        $this->db->from('estudio es');
        $this->db->join('estudios_instituciones es_in', 'es.id_estudio = es_in.id_estudio');
        $this->db->join('institucion in', 'es_in.id_institucion = in.id_institucion', 'left');
        $this->db->join('familia fam', 'es.id_familia = fam.id_familia');
        $this->db->join('cat_ciclo_escolar ccc', 'ccc.id_ciclo_escolar = es.id_ciclo_escolar');
        $this->db->join('colonia c', 'fam.id_colonia = c.id_colonia', 'left');
        $this->db->join('cat_estatus_estudios cee', 'cee.id_estatus = es.id_estatus_estudio');

        if (isset($grupo_id) && $grupo_id != null) {
            $this->db->where('fam.id_grupo', $grupo_id);
        }

        if ($tipoUsuario == '1' && $rolUsuario == '2') {
            $this->db->where('es.id_usuario_asignado', $idUsuario);
        }

        if (isset($params['pago']) && count($params['pago'])  == 0) {
            if ($tipoUsuario == '2' && $idInstitucion != '0' && count($params['idInstituciones']) == 0) {
                $this->db->where('es_in.id_institucion', $idInstitucion);
            }

            if (

                isset($params['idInstituciones']) && count($params['idInstituciones']) > 0
            ) {
                $this->db->where_in('es_in.id_institucion', $params['idInstituciones']);
            }
        }

        if (isset($params['pago']) && count($params['pago'])  > 0) {
            if ($tipoUsuario == '2' && $idInstitucion != '0' && count($params['idInstituciones']) == 0) {
                $this->db->where('es_in.id_institucion', $idInstitucion);
            }
            if (

                isset($params['idInstituciones']) && count($params['idInstituciones']) > 0
            ) {
                $this->db->where_in('es_in.id_institucion', $params['idInstituciones']);
            }
            $this->db->where_in('es_in.pago', $params['pago']);
        }

        if ($filterFamilia != 'all') {
            $this->db->like('fam.familia', $filterFamilia);
        }

        if ($tipoUsuario == '1') {
            $this->db->group_by('es.id_estudio');
        }

        if (count($ciclos) > 0) {
            $this->db->where_in('es.id_ciclo_escolar', $ciclos);
        }

        if (isset($params['id_estatus_estudio'])) {
            if ($params['id_estatus_estudio'] != '') {
                $this->db->where('es.id_estatus_estudio', $params['id_estatus_estudio']);
            }
        }

        if ($tipoUsuario == '1' && isset($params['id_usuario_asignado'])) {
            if ($params['id_usuario_asignado'] != '') {
                $this->db->where('es.id_usuario_asignado', $params['id_usuario_asignado']);
            }
        }

        if ($tipoUsuario == '1' && isset($params['id_grupo'])) {
            $this->db->where('fam.id_grupo', $params['id_grupo']);
        }

        if (isset($params['id_estatus_estudio'])) {
            if ($params['id_estatus_estudio'] != '') {
                $this->db->where('es.id_estatus_estudio', $params['id_estatus_estudio']);
            }
        }

        if (isset($params['zona'])) {
            if ($params['zona'] != '') {
                $this->db->where('c.zona', $params['zona']);
            }
        }

        $this->db->where('es.borrado', 0);
        $this->db->where('es_in.borrado', 0);
        $this->db->where('es_in.estatus', 1);
        $this->db->order_by('es.id_estudio');
        $this->db->order_by('es.folio_estudio_pago');

        $estudios = $this->db->get()->result();

        //echo $this->db->last_query();
        $array = array();

        $estudiosAux = $estudios;
        $response = [];

        /*if (isset($params['fInstitucionesHijos']) && count($params['fInstitucionesHijos']) > 0) {
            $response = $this->getFiltrarCompartido($estudiosAux, $params['fInstitucionesHijos']);
            $estudios = $response;
        }*/

        foreach ($estudios as $estudio) {
            $estudio->instituciones = $this->getEstudiosFamiliaInstitucion($estudio->id_familia, $estudio->id_institucion, 0, $tipoUsuario);
            $estudio->usuario_asignado = $this->getUsuarioAsignado($estudio->id_usuario_asignado);
            $estudio->solicitudes = $this->getCompartido($estudio->id_estudio, $estudio->id_familia);
            $estudio->grupo = $this->getGrupo($estudio->id_grupo);
            $estudio->institucion_familia = $this->getInstitucion($estudio->institucion_familia);
            if ($estudio) {
                $fecha_token = $estudio->vigencia_token;
                $hoy = date('Y-m-d');
                if ($hoy > $fecha_token) {
                    $estudio->token_vencido = true;
                } else {
                    $estudio->token_vencido = false;
                }
            }

            array_push($array, $estudio);
        }
        return $array;
    }

    public function getGrupo($id)
    {
        $this->db->where('id_institucion', $id);
        return $this->db->get('institucion')->row();
    }

    public function getFiltrarCompartido($estudios, $params)
    {
        $result = [];
        foreach ($estudios as $estudio) {
            $this->db->where('es.id_estudio', $estudio->id_estudio);
            $this->db->select('es.id_estudio');
            $this->db->from('estudio es');
            $this->db->join('hijo_familia hf', 'es.id_estudio = hf.id_estudio');
            if (is_array($params) && count($params) > 0) {
                $this->db->where_in('hf.institucion', $params);
                $this->db->join('familia f', 'f.id_familia = es.id_familia');
                $this->db->join('institucion i', 'f.id_institucion = i.id_institucion');
                $this->db->where_in('i.clave_institucion', $params);
            }

            $data = $this->db->get()->result();
            if (count($data) > 0) {
                $ids = [];
                foreach ($data as $es) {
                    $ids[] = $es->id_estudio;
                }
                if (in_array($estudio->id_estudio, $ids)) {
                    $result[] = $estudio;
                }
            } else {
                $this->db->where('es.id_estudio', $estudio->id_estudio);
                $this->db->select('es.id_estudio');
                $this->db->from('estudio es');
                $this->db->join('familia f', 'f.id_familia = es.id_familia');
                $this->db->join('institucion i', 'f.id_institucion = i.id_institucion');
                $this->db->where_in('i.clave_institucion', $params);

                $data = $this->db->get()->result();
                if (count($data) > 0) {
                    $ids = [];
                    foreach ($data as $es) {
                        $ids[] = $es->id_estudio;
                    }
                    if (in_array($estudio->id_estudio, $ids)) {
                        $result[] = $estudio;
                    }
                }
            }
        }

        return $result;
    }

    public function getUsuarioAsignado($idUsuario)
    {
        $this->db->where('id_usuario', $idUsuario);
        return $this->db->get('usuario')->result();;
    }

    public function getEstudiosFamiliaInstitucion($idFamilia, $idInstitucion, $idEstudio, $tipoUsuario)
    {
        $add = "";
        if ($idEstudio != 0) {
            $add = ", in.clave_institucion";
        }
        if ($idInstitucion != 0) {
            $add = ",in.clave_institucion";
        }
        /* if ($idEstudio != 0) {
            $add = ", in.clave_institucion";
        }
        if (is_array($idInstitucion) != 0) {
            $add = ",in.clave_institucion";
        }*/
        $this->db->select('es_in.id_estudio_institucion, es_in.id_estudio, '
            . ' es.id_estatus_estudio, es_in.id_institucion, ciclo.ciclo_escolar, es_in.num_recibo'
            . $add);
        $this->db->from('estudios_instituciones es_in');
        $this->db->join('estudio es', 'es.id_estudio = es_in.id_estudio');
        $this->db->join('cat_ciclo_escolar ciclo', 'es.id_ciclo_escolar = ciclo.id_ciclo_escolar');
        $this->db->where('ciclo.status', 1);

        if ($idInstitucion != 0 || $idEstudio != 0) {
            $this->db->join('institucion in', 'in.id_institucion = es_in.id_institucion');
        }

        if (is_array($idInstitucion)) {
            $this->db->join('institucion in', 'in.id_institucion = es_in.id_institucion');
            $this->db->where_in('es_in.id_institducion', $idInstitucion);
        }

        if ($idFamilia != 0) {
            $this->db->where('es.id_familia', $idFamilia);
        }

        if ($idEstudio != 0) {
            $this->db->where('es_in.id_estudio', $idEstudio);
        }

        $this->db->where('es_in.estatus', 1);
        $this->db->where('ciclo.status', 1);
        $this->db->where('es.borrado', 0);
        $this->db->group_by('es_in.id_institucion');
        $ins = $this->db->get()->result();
        $result = array();
        //return $ins;
        foreach ($ins as $in) {
            $in->count = $this->getCountIns($in->id_estudio, $in->id_institucion);
            array_push($result, $in);
        }
        //return $this->db->last_query();
        return $result;
    }

    public function getCountIns($idEstudio, $idInstitucion)
    {
        $this->db->select('count(*) as total');
        $this->db->from('estudios_instituciones es_in');
        $this->db->join('estudio es', 'es.id_estudio = es_in.id_estudio');
        $this->db->join('institucion in', 'in.id_institucion = es_in.id_institucion');
        $this->db->where('es.id_estudio', $idEstudio);
        $this->db->where('es_in.estatus', 1);
        $this->db->where('in.id_institucion', $idInstitucion);
        return $this->db->get()->row();
    }

    public function getEstudioActivoInstitucion($idInstitucion)
    {
        $this->db->select('es_in.id_estudio, es.pago, es.num_recibo');
        $this->db->from('estudios_instituciones es_in');
        $this->db->join('estudio es', 'es.id_estudio = es_in.id_estudio');
        $this->db->where('es_in.id_institucion', $idInstitucion);
        $this->db->where('es.id_estatus_estudio!=', 6);
        return $this->db->get()->row();
    }

    public function getEstudioDetalle($idEstudio, $idInstitucion)
    {
        $this->db->select('es.*, ei.*, in.*');
        $this->db->from('estudio es');
        $this->db->join('familia fam', 'es.id_familia = fam.id_familia');
        $this->db->join('institucion in', 'fam.id_institucion = in.id_institucion');
        $this->db->join('estudios_instituciones ei', 'ei.id_estudio = es.id_estudio');
        $this->db->join('cat_ciclo_escolar c', 'c.id_ciclo_escolar = es.id_ciclo_escolar');
        $this->db->where('es.id_estudio', $idEstudio);
        if ($idInstitucion !== '0') {
            $this->db->where('ei.id_institucion', $idInstitucion);
            $this->db->where('ei.estatus', 1);
        }
        $estudio = $this->db->get()->row();
        $estudio->instituciones = $this->getEstudiosFamiliaInstitucion($estudio->id_familia, 0, $estudio->id_estudio, 1);
        $estudio->hijos = $this->getHijosFamilia($estudio->id_familia, $estudio->id_estudio);
        $estudio->dependientes = $this->getDependientesFamilia($estudio->id_familia, $estudio->id_estudio);
        $estudio->motivos = $this->getMotivosFamilia($estudio->id_familia, $estudio->id_estudio);
        $estudio->vehiculos = $this->getVehiculosFamilia($estudio->id_familia, $estudio->id_estudio);
        $estudio->propiedades = $this->getPropiedadesFamilia($estudio->id_familia, $estudio->id_estudio);
        $estudio->ingresos = $this->getIngresosFamilia($estudio->id_familia, $estudio->id_estudio);
        $estudio->egresos = $this->getEgresosFamilia($estudio->id_familia, $estudio->id_estudio);
        $estudio->egresosCicloAnterior = $this->getEgresosFamiliaCicloAnterior($estudio->id_familia, $estudio->id_estudio);
        $estudio->pasivos = $this->getPasivosFamilia($estudio->id_familia, $estudio->id_estudio);
        $estudio->documentos = $this->getDocumentosFamilia($estudio->id_familia, $estudio->id_estudio);
        $estudio->evaluacion = $this->getEvaluacionFamilia($estudio->id_familia, $estudio->id_estudio);
        $estudio->comentarios = $this->getComentarioesFamilia($estudio->id_familia, $estudio->id_estudio);
        $estudio->padres = $this->getPadreFamilia($estudio->id_familia, $estudio->id_estudio);
        $estudio->usuario_asignado = $this->getUsuarioAsignado($estudio->id_usuario_asignado);
        $estudio->ciclo_escolar = $this->getCicloEscolarById($estudio->id_ciclo_escolar);
        $estudio->familia = $this->getFamilia($estudio->id_familia);
        $estudio->solicitudes = $this->getCompartido($estudio->id_estudio, $estudio->id_familia);
        return $estudio;
    }

    /* hijos */
    public function getHijosFamilia($idFamilia, $idEstudio)
    {
        $this->db->where('borrado', 0);
        $this->db->where('id_familia', $idFamilia);
        $this->db->where('id_estudio', $idEstudio);
        $this->db->order_by('fecha_registro');
        return $this->db->get('hijo_familia')->result();
    }

    public function saveHijo($data)
    {
        $this->db->insert('hijo_familia', $data);
        return $this->getHijosFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function deleteHijo($data)
    {
        $this->db->where('id_hijo_familia', $data['id_hijo_familia']);
        $this->db->set('borrado', 1);
        $this->db->update('hijo_familia');
        return $this->getHijosFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function updateHijo($data)
    {
        $this->db->where('id_hijo_familia', $data['id_hijo_familia']);
        unset($data['id_hijo_familia']);
        unset($data['borrado']);
        $data['fecha_modificacion'] = date("Y-m-d H:i:s");
        $this->db->update('hijo_familia', $data);
        return $this->getHijosFamilia($data['id_familia'], $data['id_estudio']);
    }

    /* dependientes */

    public function getDependientesFamilia($idFamilia, $idEstudio)
    {
        $this->db->where('borrado', 0);
        $this->db->where('id_familia', $idFamilia);
        $this->db->where('id_estudio', $idEstudio);
        return $this->db->get('dependiente_familia')->result();
    }

    public function saveDependiente($data)
    {
        $this->db->insert('dependiente_familia', $data);
        return $this->getDependientesFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function deleteDependiente($data)
    {
        $this->db->where('id_dependiente_economico', $data['id_dependiente_economico']);
        $this->db->set('borrado', 1);
        $this->db->update('dependiente_familia');
        return $this->getDependientesFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function updateDependiente($data)
    {
        $this->db->where('id_dependiente_economico', $data['id_dependiente_economico']);
        unset($data['borrado']);
        $data['fecha_modificacion'] = date("Y-m-d H:i:s");
        $this->db->update('dependiente_familia', $data);
        return $this->getDependientesFamilia($data['id_familia'], $data['id_estudio']);
    }

    /* motivos */

    public function getMotivosFamilia($idFamilia, $idEstudio)
    {
        $this->db->where('borrado', 0);
        $this->db->where('id_familia', $idFamilia);
        $this->db->where('id_estudio', $idEstudio);
        return $this->db->get('motivo_familia')->result();
    }

    public function saveMotivo($data)
    {
        $this->db->insert('motivo_familia', $data);
        return $this->getMotivosFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function deleteMotivo($data)
    {
        $this->db->where('id_motivo', $data['id_motivo']);
        $this->db->set('borrado', 1);
        $this->db->update('motivo_familia');
        return $this->getMotivosFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function updateMotivo($data)
    {
        $this->db->where('id_motivo', $data['id_motivo']);
        unset($data['id_motivo']);
        unset($data['borrado']);
        $data['fecha_modificacion'] = date("Y-m-d H:i:s");
        $this->db->update('motivo_familia', $data);
        return $this->getMotivosFamilia($data['id_familia'], $data['id_estudio']);
    }

    /* vehiculos */

    public function getVehiculosFamilia($idFamilia, $idEstudio)
    {
        $this->db->where('borrado', 0);
        $this->db->where('id_familia', $idFamilia);
        $this->db->where('id_estudio', $idEstudio);
        return $this->db->get('vehiculo_familia')->result();
    }

    public function saveVehiculo($data)
    {
        $this->db->insert('vehiculo_familia', $data);
        return $this->getVehiculosFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function deleteVehiculo($data)
    {
        $this->db->where('id_vehiculo_familia', $data['id_vehiculo_familia']);
        $this->db->set('borrado', 1);
        $this->db->update('vehiculo_familia');
        return $this->getVehiculosFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function updateVehiculo($data)
    {
        $this->db->where('id_vehiculo_familia', $data['id_vehiculo_familia']);
        unset($data['id_vehiculo_familia']);
        unset($data['borrado']);
        $data['fecha_modificacion'] = date("Y-m-d H:i:s");
        $this->db->update('vehiculo_familia', $data);
        return $this->getVehiculosFamilia($data['id_familia'], $data['id_estudio']);
    }

    /* propiedades */

    public function getPropiedadesFamilia($idFamilia, $idEstudio)
    {
        $this->db->where('borrado', 0);
        $this->db->where('id_familia', $idFamilia);
        $this->db->where('id_estudio', $idEstudio);
        return $this->db->get('propiedad_familia')->result();
    }

    public function savePropiedad($data)
    {
        $this->db->insert('propiedad_familia', $data);
        return $this->getPropiedadesFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function deletePropiedad($data)
    {
        $this->db->where('id_propiedad_familia', $data['id_propiedad_familia']);
        $this->db->set('borrado', 1);
        $this->db->update('propiedad_familia');
        return $this->getPropiedadesFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function updatePropiedad($data)
    {
        $this->db->where('id_propiedad_familia', $data['id_propiedad_familia']);
        unset($data['id_propiedad_familia']);
        unset($data['borrado']);
        $data['fecha_modificacion'] = date("Y-m-d H:i:s");
        $this->db->update('propiedad_familia', $data);
        return $this->getPropiedadesFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function updateEstudioInstitucion($data)
    {
        $this->db->where('id_estudio_institucion', $data['id_estudio_institucion']);
        unset($data['id_estudio_institucion']);
        unset($data['borrado']);
        $data['fecha_modificacion'] = date("Y-m-d H:i:s");
        return $this->db->update('estudios_instituciones', $data);
    }

    public function getIngresosFamilia($idFamilia, $idEstudio)
    {
        $this->db->where('borrado', 0);
        $this->db->where('id_familia', $idFamilia);
        $this->db->where('id_estudio', $idEstudio);
        $this->db->order_by('id_ingreso_familia', 'desc');
        return $this->db->get('ingresos_familia')->result();
    }

    public function saveIngresos($data)
    {
        $this->db->insert('ingresos_familia', $data);
        return $this->getIngresosFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function updateIngresos($data)
    {
        $this->db->where('id_ingreso_familia', $data['id_ingreso_familia']);
        unset($data['id_ingreso_familia']);
        unset($data['borrado']);
        $data['fecha_modificacion'] = date("Y-m-d H:i:s");
        $this->db->update('ingresos_familia', $data);
        return $this->getIngresosFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function getPadreFamilia($idFamilia, $idEstudio)
    {
        $this->db->where('borrado', 0);
        $this->db->where('id_familia', $idFamilia);
        $this->db->where('id_estudio', $idEstudio);
        $this->db->order_by('tipo_persona', 'DESC');
        return $this->db->get('padre_familia')->result();
    }

    public function savePapa($data)
    {
        $this->db->insert('padre_familia', $data);
        return $this->getPadreFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function updatePapa($data)
    {
        $this->db->where('id_padre_familia', $data['id_padre_familia']);
        unset($data['id_padre_familia']);
        unset($data['borrado']);
        $data['fecha_modificacion'] = date("Y-m-d H:i:s");
        $this->db->update('padre_familia', $data);
        return $this->getPadreFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function getEgresosFamilia($idFamilia, $idEstudio)
    {
        $this->db->where('borrado', 0);
        $this->db->where('id_familia', $idFamilia);
        $this->db->where('id_estudio', $idEstudio);
        $this->db->order_by('id_egreso_familia', 'desc');
        return $this->db->get('egresos_familia')->result();
    }

    public function getEgresosFamiliaCicloAnterior($idFamilia, $idEstudio)
    {
        $cicloActual = $this->getCicloEscolarObj();
        $idCicloAnterior = $cicloActual->id_ciclo_escolar - 1;
        $this->db->where('id_familia', $idFamilia);
        $this->db->where('id_ciclo_escolar', $idCicloAnterior);
		$this->db->order_by('id_estudio', 'desc');
        $estudioAnterior = $this->db->get('estudio')->row();
        if($estudioAnterior){
            return $this->getEgresosFamilia($estudioAnterior->id_familia, $estudioAnterior->id_estudio);
        }else{
            return 0;
        }
    }

    public function saveEgresos($data)
    {
        $this->db->insert('egresos_familia', $data);
        return $this->getEgresosFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function updateEgresos($data)
    {
        $this->db->where('id_egreso_familia', $data['id_egreso_familia']);
        unset($data['id_egreso_familia']);
        unset($data['borrado']);
        $data['fecha_modificacion'] = date("Y-m-d H:i:s");
        $this->db->update('egresos_familia', $data);
        return $this->getEgresosFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function getPasivosFamilia($idFamilia, $idEstudio)
    {
        $this->db->where('borrado', 0);
        $this->db->where('id_familia', $idFamilia);
        $this->db->where('id_estudio', $idEstudio);
        $this->db->order_by('id_pasivo', 'desc');
        return $this->db->get('pasivo_familia')->result();
    }

    public function savePasivo($data)
    {
        $this->db->insert('pasivo_familia', $data);
        return $this->getPasivosFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function updatePasivo($data)
    {
        $this->db->where('id_pasivo', $data['id_pasivo']);
        unset($data['id_pasivo']);
        $this->db->update('pasivo_familia', $data);
        return $this->getPasivosFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function deletePasivo($data)
    {
        $this->db->where('id_pasivo', $data['id_pasivo']);
        $this->db->delete('pasivo_familia');
        return $this->getComentarioesFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function getDocumentosFamilia($idFamilia, $idEstudio)
    {
        $this->db->where('borrado', 0);
        $this->db->where('id_familia', $idFamilia);
        $this->db->where('id_estudio', $idEstudio);
        $this->db->order_by('id_documento_familia', 'desc');
        return $this->db->get('documentos_familia')->result();
    }

    public function saveDocumentos($data)
    {
        $this->db->insert('documentos_familia', $data);
        return $this->getDocumentosFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function updateDocumentos($data)
    {
        $this->db->where('id_documento_familia', $data['id_documento_familia']);
        unset($data['id_documento_familia']);
        unset($data['borrado']);
        $data['fecha_modificacion'] = date("Y-m-d H:i:s");
        $this->db->update('documentos_familia', $data);
        return $this->getDocumentosFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function getEvaluacionFamilia($idFamilia, $idEstudio)
    {
        $this->db->where('borrado', 0);
        $this->db->where('id_familia', $idFamilia);
        $this->db->where('id_estudio', $idEstudio);
        $this->db->order_by('id_evaluacion_familia', 'desc');
        return $this->db->get('evaluacion_familia')->result();
    }

    public function saveEvaluacion($data)
    {
        $this->db->insert('evaluacion_familia', $data);
        return $this->getEvaluacionFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function updateEvaluacion($data)
    {
        $this->db->where('id_evaluacion_familia', $data['id_evaluacion_familia']);
        unset($data['id_evaluacion_familia']);
        unset($data['borrado']);
        $data['fecha_modificacion'] = date("Y-m-d H:i:s");
        $this->db->update('evaluacion_familia', $data);
        return $this->getEvaluacionFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function getCicloEscolar()
    {
        $this->db->where('status', 1);
        return $this->db->get('cat_ciclo_escolar')->result();
    }

    public function getCicloEscolarById($id)
    {
        //$this->db->where('status', 1);
        $this->db->where('id_ciclo_escolar', $id);
        return $this->db->get('cat_ciclo_escolar')->row();
    }

    public function getCicloEscolarObj()
    {
        $this->db->where('status', 1);
        $this->db->limit(1);
        return $this->db->get('cat_ciclo_escolar')->row();
    }

    public function getEstatusCat()
    {
        return $this->db->get('cat_estatus_estudios')->result();
    }

    public function getCicloEscolarCat()
    {
        return $this->db->get('cat_ciclo_escolar')->result();
    }

    /* comentarios */

    public function getComentarioesFamilia($idFamilia, $idEstudio)
    {
        $this->db->where('id_familia', $idFamilia);
        $this->db->where('id_estudio', $idEstudio);
        $this->db->order_by('posicion', 'asc');
        return $this->db->get('comentario_familia')->result();
    }

    public function saveComentario($data)
    {
        $this->db->insert('comentario_familia', $data);
        return $this->getComentarioesFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function deleteComentario($data)
    {
        $this->db->where('id_comentario_familia', $data['id_comentario_familia']);
        $this->db->delete('comentario_familia');
        return $this->getComentarioesFamilia($data['id_familia'], $data['id_estudio']);
    }

    public function updateComentario($data)
    {
        $this->db->where('id_comentario_familia', $data['id_comentario_familia']);
        unset($data['id_comentario_familia']);
        $this->db->update('comentario_familia', $data);
        return $this->getComentarioesFamilia($data['id_familia'], $data['id_estudio']);
    }
}
