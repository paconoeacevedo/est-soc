<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Colonia_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function create($data){
        $this->db->insert('colonia', $data);
        return $this->get($this->db->insert_id());
    }

    public function get($id) {
        $this->db->where('borrado', 0);
        $this->db->where('id_colonia', $id);
        return $this->db->get('colonia')->row();
    }
    
    public function update($data) {
        $this->db->where('id_colonia', $data['id_colonia']);
        unset($data['id_colonia']);
        return $this->db->update('colonia', $data);
    }

    public function delete($id) {
        $this->db->where('id_colonia', $id);
        $this->db->set('borrado', 1);
        return $this->db->update('colonia');
    }
    
    public function getAll() {
        $this->db->select('*');
        $this->db->where('borrado', 0);
        $this->db->order_by('zona');
        return $this->db->get('colonia')->result();
    }

    public function getZonas() {
        $this->db->select('zona');
        $this->db->where('borrado', 0);
        $this->db->group_by('zona');
        return $this->db->get('colonia')->result();
    }

    public function getColoniasPorZona($zona) {
        $this->db->select('*');
        $this->db->where('borrado', 0);
        $this->db->where('zona', $zona);
        return $this->db->get('colonia')->result();
    }

}
