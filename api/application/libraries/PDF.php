<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/dompdf/autoload.inc.php');
use Dompdf\Dompdf;

class PDF 
{
    public function __construct()
    {
        log_message('Debug', 'dompdf class is loaded.');
    }

    function createPDF($html, $filename = '', $download = false, $paper = 'A3', $orientation = 'portrait')
    {
        $dompdf = new DOMPDF();
        $dompdf->set_option('enable_remote', TRUE);
        $dompdf->load_html($html);
        $dompdf->set_paper($paper, $orientation);
        $dompdf->render();
        if ($download) {
            $dompdf->stream($filename, array("Attachment" => 1));
        } else {
            return $dompdf->output();
        }
    }
}
