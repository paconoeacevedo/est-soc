<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class EmailV2
{
    public function __construct()
    {
        log_message('Debug', 'PHPMailer class is loaded.');
    }

    public function load()
    {
        // Include PHPMailer library files
        require_once APPPATH . 'libraries/PHPMailer6/src/Exception.php';
        require_once APPPATH . 'libraries/PHPMailer6/src/PHPMailer.php';
        require_once APPPATH . 'libraries/PHPMailer6/src/SMTP.php';;
        $mail = new PHPMailer(true);
        return $mail;
    }

    public function send($data)
    {
        $mail = $this->load();
        // SMTP configuration
        $mail->isSMTP();
        $mail->Host     = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'estudioswise@gmail.com';
        $mail->Password = 'xhqemtakpmjqqqar';
        $mail->SMTPSecure = 'tls';
        $mail->Port     = 587;
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
		
        $mail->setFrom('estudioswise@gmail.com', 'Estudios Socioeconómicos HRWise');

        $mto = $this->obj($data['to']);
        $mcc = $this->obj($data['cc']);
        $mcco = $this->obj($data['cco']);

        $text_dev = "";
        if (ENVIRONMENT == "") {
            $mail->addAddress("francisco.rivera@infotec.mx", "Admin Estudios");
            $text_dev .= "<div style='font-size:8px'>";
            $text_dev .= "<hr>";
            $text_dev .= "<h3>SIN VALIDEZ OFICIAL, SISTEMA EN DESARROLLO</h3>";
            $text_dev .= "<h3>Destinatarios</h3>";
            foreach ($mto as $to) {
                $text_dev .= "<li>" . $to->email . "</li>";
            }
            $text_dev .= "<h3>Con copia</h3>";
            foreach ($mcc as $to) {
                $text_dev .= "<li>" . $to->email . "</li>";
            }
            $text_dev .= "<h3>Con copia oculta</h3>";
            foreach ($mcco as $to) {
                $text_dev .= "<li>" . $to->email . "</li>";
            }
            $text_dev .= "</div>";
            $mail->addBCC("paconoeacevedo@gmail.com", "Admin Est-Soc");
        } else {
            foreach ($mto as $to) {
                $mail->addAddress($to->email, $to->name);
            }
            foreach ($mcc as $to) {
                $mail->addCC($to->email, $to->name);
            }
            foreach ($mcco as $to) {
                $mail->addBCC($to->email, $to->name);
            }
            $mail->addBCC("paconoeacevedo@gmail.com", "Admin Est-Soc");
        }

        // Add cc or bcc 

        // Email subject
        $mail->Subject = $data['inssue'];

        // Set email format to HTML
        $mail->isHTML(true);

        // Email body content
        $text = $data['body'];
        $text .= $text_dev;
        $body = '';
        if(isset($data['id_grupo'])){
            if($data['id_grupo'] == "9"){
                $body = str_replace('content', $text, file_get_contents('email.php'));
            }else{
                if($data['id_grupo'] == "10"){
                    $body = str_replace('content', $text, file_get_contents('email_iter.php'));
                }
            }
        }else{
            $body = str_replace('content', $text, file_get_contents('email_generico.php'));
        }
        
        $mail->Body = $body;
        $mail->CharSet = "UTF-8";
        // Send email
        if (!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
            return false;
        } else {
            return true;
        }
    }

    function obj($array)
    {
        $object = new stdClass();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = $this->obj($value);
            }
            $object->$key = $value;
        }
        return $object;
    }
}
