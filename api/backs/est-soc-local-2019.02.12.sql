#Script para exportar datos de N estudios al servidor

delete from padre_familia where id_estudio=812 and id_familia=264;

insert into padre_familia(id_familia, id_estudio, nombre, apellido_paterno, apellido_materno, edad, fecha_nacimiento, correo, rfc, celular, profesion, ocupacion,empresa, puesto, giro, dueno, antiguedad, sueldo_neto, tipo_persona) values ('264', '812', 'MANUEL','ACEVES', 'RUBIO', '', '', 'manuel.acevesr@live.com.mx', '', '4499192101', '', '','', '', '', 'NO', '',   '6000.00',  'PAPA'); 
insert into padre_familia(id_familia, id_estudio, nombre, apellido_paterno, apellido_materno, edad, fecha_nacimiento, correo, rfc, celular, profesion, ocupacion,empresa, puesto, giro, dueno, antiguedad, sueldo_neto, tipo_persona) values ('264', '812', 'ROSALINDA','CHÁVEZ', 'LÓPEZ', '', '', '', '', '4497698266', '', '','', '', '', 'NO', '',   '3000.00',  'MAMA'); 

delete from hijo_familia where id_estudio=812 and id_familia=264;

insert into hijo_familia(id_familia, id_estudio, folio_estudio, nombre, apellido_paterno, apellido_materno, fecha_nacimiento, edad, correo_electronico, nivel_grado_cursar, institucion, colegiatura_pasado, colegiatura_actual, beca_actual, fuente_apoyo, apoyo_solicitado, status, comentarios, otras_colegiaturas) values ('264', '812', '2019-CEE-812', 'FRANCISCO NOÉ','RIVERA', 'ACEVEDO', '', '', '', '3° Kinder', '', '5350.00', '0.00','', '0.00', '0.00', '', 'Hola', ''); 

delete from dependiente_familia where id_estudio=812 and id_familia=264;


delete from motivo_familia where id_estudio=812 and id_familia=264;

insert into motivo_familia(id_familia, id_estudio, folio_estudio, tipo_motivo, motivo) values ('264', '812', '2019-CEE-812', 'AE','Apoyo eco'); 

delete from vehiculo_familia where id_estudio=812 and id_familia=264;


delete from propiedad_familia where id_estudio=812 and id_familia=264;


delete from ingresos_familia where id_estudio=812 and id_familia=264;

insert into ingresos_familia(id_familia, id_estudio, folio_estudio, num_personas_aportan, consecutivo_ingreso, sueldo_papa, sueldo_mama, ingreso_otros_miembros, ingreso_renta, ingreso_honorarios, ingreso_inversiones, ingreso_pensiones, ingreso_ventas, otros_ingresos, total_otros_ingresos, total_ingresos, ingreso_percapita, clasificacion, otros_comentario) values ('264', '812', '2019-CEE-812', '', '', '6000.00', '3000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '9000.00', '3000.00', 'C', ''); 

delete from egresos_familia where id_estudio=812 and id_familia=264;

insert into egresos_familia(id_familia, id_estudio, folio_estudio, alimentacion_despensa, renta, credito_hipotecario, colegiaturas, otras_colegiaturas, clases_particulares, agua, luz, telefono, servicio_domestico, gas, otros, gasolina, credito_auto, pago_tdc_mensual, saldo_tdc, creditos_comerciales, vestido_calzado, medico_medicinas, diversion_entretenimiento, clubes_deportivos, seguros, vacaciones, otros2, total_servicios, comentarios, total_egresos, diferencia_egre_ingre) values ('264', '812', '2019-CEE-812', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '0.00', '9000.00'); 

delete from documentos_familia where id_estudio=812 and id_familia=264;

insert into documentos_familia(id_familia, id_estudio, folio_estudio, carta_no_adeudo, firma_reglamento, nomina_carta, poliza, estado_cuenta, recibos_renta, facturas_hospital, comprobante_finiquito, demandas_judiciales, servicios, pagos_credito_hipo, pagos_credito_auto, otros, comentarios) values ('264', '812', '2019-CEE-812', '0', '0', '0', '0', '1', '1', '1', '1', '1', '0', '0', '0', '0', 'Faltaron docs'); 

delete from evaluacion_familia where id_estudio=812 and id_familia=264;

insert into evaluacion_familia(id_familia, id_estudio, folio_estudio, apreciacion, discrepancia) values ('264', '812', '2019-CEE-812', 'CONFIABLE', 'NINGUNA'); 

delete from comentario_familia where id_estudio=812 and id_familia=264;

insert into comentario_familia(id_familia, id_estudio, folio_estudio, comentario, tipo, posicion) values ('264', '812', '2019-CEE-812', 'Comenta la mama que.........', 'GENERAL', ''); 
insert into comentario_familia(id_familia, id_estudio, folio_estudio, comentario, tipo, posicion) values ('264', '812', '2019-CEE-812', 'OK OK OK', 'ACEPTO_ENTREVISTA', '0'); 
insert into comentario_familia(id_familia, id_estudio, folio_estudio, comentario, tipo, posicion) values ('264', '812', '2019-CEE-812', 'me voy de vacaciones', 'CANCELO_ENTREVISTA', '0'); 
insert into comentario_familia(id_familia, id_estudio, folio_estudio, comentario, tipo, posicion) values ('264', '812', '2019-CEE-812', 'Reagendo', 'ACEPTO_ENTREVISTA', '0'); 
update estudio set id_estatus_estudio=5 where id_estudio=812;