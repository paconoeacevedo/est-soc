ALTER TABLE familia
ADD COLUMN correo_acceso VARCHAR(255) NULL AFTER  id_familia;

ALTER TABLE estudio
ADD COLUMN token_acceso VARCHAR(255) NULL AFTER id_estudio;

ALTER TABLE estudio
ADD COLUMN vigencia_token DATETIME NULL AFTER token_acceso;

ALTER TABLE estudio
ADD COLUMN nums_accesos INT(5) NULL AFTER vigencia_token;

ALTER TABLE estudio
ADD COLUMN token_utilizado TINYINT(4) DEFAULT '0' NULL AFTER nums_accesos;


ALTER TABLE estudio
ADD COLUMN token_finalizado TINYINT(4) DEFAULT '0' NULL AFTER token_utilizado;


alter table padre_familia add COLUMN telefono_oficina VARCHAR(255) null AFTER celular;

INSERT INTO `cat_estatus_estudios` (`id_estatus`, `nombre`) VALUES ('10', 'En autocaptura');
INSERT INTO `cat_estatus_estudios` (`id_estatus`, `nombre`) VALUES ('11', 'Autocaptura finalizada');

alter table estudio add COLUMN guardo_datos TINYINT(4) DEFAULT '0' NULL;

ALTER TABLE `estudio`
ADD COLUMN `guardo_captura`  tinyint(4) NULL DEFAULT '0' AFTER `guardo_datos`,
ADD COLUMN `finalizo_captura`  tinyint(4) NULL DEFAULT '0' AFTER `guardo_captura`,
ADD COLUMN `guardo_ingresos`  tinyint(4) NULL DEFAULT '0' AFTER `finalizo_captura`,
ADD COLUMN `guardo_egresos`  tinyint(4) NULL DEFAULT '0' AFTER `guardo_ingresos`;

UPDATE `cat_estatus_estudios` SET `nombre`='Guardo auto captura' WHERE (`id_estatus`='11');
INSERT INTO `cat_estatus_estudios` (`id_estatus`, `nombre`) VALUES ('12', 'Finalizo y envio autocaptura')

ALTER TABLE `estudio`
ADD COLUMN `acepto_terminos`  tinyint(4) NULL DEFAULT '0' AFTER `guardo_egresos`;

ALTER TABLE `hijo_familia`
ADD COLUMN `solicita_beca`  tinyint(4) NULL DEFAULT '0';

ALTER TABLE `hijo_familia`
ADD COLUMN `es_estudiante`  tinyint(4) NULL DEFAULT '0';

ALTER TABLE `hijo_familia`
ADD COLUMN `institucion_diferente`  tinyint(4) NULL DEFAULT '0';

ALTER TABLE `dependiente_familia`
ADD COLUMN `parentesco_otro` VARCHAR(255) null;

ALTER TABLE `estudio`
ADD COLUMN `archivo_caratula`  varchar(255) NULL;

UPDATE `cat_estatus_estudios` SET `nombre`='Enviado a familia' WHERE (`id_estatus`='1');

ALTER TABLE `cat_estatus_estudios`
ADD COLUMN `class`  varchar(255) NULL AFTER `nombre`;

UPDATE `cat_estatus_estudios` SET `class`='warning' WHERE (`id_estatus`='1');
UPDATE `cat_estatus_estudios` SET `class`='default' WHERE (`id_estatus`='2');
UPDATE `cat_estatus_estudios` SET `class`='primary' WHERE (`id_estatus`='3');
UPDATE `cat_estatus_estudios` SET `class`='success' WHERE (`id_estatus`='4');
UPDATE `cat_estatus_estudios` SET `class`='info' WHERE (`id_estatus`='5');
UPDATE `cat_estatus_estudios` SET `class`='warning' WHERE (`id_estatus`='6');
UPDATE `cat_estatus_estudios` SET `class`='default' WHERE (`id_estatus`='7');
UPDATE `cat_estatus_estudios` SET `class`='danger' WHERE (`id_estatus`='8');
UPDATE `cat_estatus_estudios` SET `class`='danger' WHERE (`id_estatus`='9');
UPDATE `cat_estatus_estudios` SET `class`='info' WHERE (`id_estatus`='10');
UPDATE `cat_estatus_estudios` SET `class`='warning' WHERE (`id_estatus`='11');
UPDATE `cat_estatus_estudios` SET `class`='success' WHERE (`id_estatus`='12');

ALTER TABLE `estudio`
ADD COLUMN `folio_estudio_pago`  varchar(255) NULL;

ALTER TABLE `hijo_familia`
ADD COLUMN `institucion_id`  int(11) NULL;

ALTER TABLE `institucion`
ADD COLUMN `letra_identificacion`  varchar(255) NULL;

ALTER TABLE `ingresos_familia`
ADD COLUMN `aplica`  tinyint(4) NULL DEFAULT '0';

ALTER TABLE `egresos_familia`
ADD COLUMN `aplica`  tinyint(4) NULL DEFAULT '0';

ALTER TABLE `propiedad_familia`
ADD COLUMN `casa_habitacion`  tinyint(4) NULL DEFAULT 0;


-- ----------------------------
-- Table structure for grado
-- ----------------------------
DROP TABLE IF EXISTS `grado`;
CREATE TABLE `grado` (
  `id_grado` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_grado` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `colegiatura` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_registro` decimal(60,2) DEFAULT NULL,
  `fecha_modificacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `borrado` tinyint(4) DEFAULT '0',
  `kinder` tinyint(4) DEFAULT NULL,
  `id_institucion` int(11) DEFAULT '0',
  PRIMARY KEY (`id_grado`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of grado
-- ----------------------------
INSERT INTO `grado` VALUES ('1', 'Prematernal', '3350', '20190212151432.00', '2020-01-30 14:11:47', '0', '1', '0');
INSERT INTO `grado` VALUES ('2', 'Maternal', '4550', '20190212151432.00', '2020-01-30 14:12:01', '0', '1', '0');
INSERT INTO `grado` VALUES ('3', 'Kinder 1', '5325', '20190212151432.00', '2020-01-30 14:12:26', '0', '1', '0');
INSERT INTO `grado` VALUES ('4', 'Kinder 2', '5325', '20190212151432.00', '2020-01-30 14:12:43', '0', '1', '0');
INSERT INTO `grado` VALUES ('5', 'Kinder 3', '5800', '20190212151432.00', '2020-01-30 14:12:45', '0', '1', '0');
INSERT INTO `grado` VALUES ('6', '1° Elementary Encino', '7690', '20190212151432.00', '2020-01-30 14:41:26', '0', '0', '1');
INSERT INTO `grado` VALUES ('7', '2° Elementary Encino', '7690', '20190212151432.00', '2020-01-30 14:41:24', '0', '0', '1');
INSERT INTO `grado` VALUES ('8', '3° Elementary Encino', '7690', '20190212151432.00', '2020-01-30 14:41:23', '0', '0', '1');
INSERT INTO `grado` VALUES ('9', '4° Elementary Encino', '7690', '20190212151432.00', '2020-01-30 14:41:22', '0', '0', '1');
INSERT INTO `grado` VALUES ('10', '5° Junior High Encino', '7690', '20190212151432.00', '2020-01-30 14:41:21', '0', '0', '1');
INSERT INTO `grado` VALUES ('11', '6° Junior High Encino', '7690', '20190212151432.00', '2020-01-30 14:41:20', '0', '0', '1');
INSERT INTO `grado` VALUES ('12', '7° Junior High Encino', '8800', '20190212151432.00', '2020-01-30 14:41:18', '0', '0', '1');
INSERT INTO `grado` VALUES ('13', '8° Junior High Encino', '8800', '20190212151432.00', '2020-01-30 14:41:17', '0', '0', '1');
INSERT INTO `grado` VALUES ('14', '9° High School Encino', '8800', '20190212151432.00', '2020-01-30 14:05:30', '0', '0', '1');
INSERT INTO `grado` VALUES ('15', '10° High School Encino', '10400', '20190212151432.00', '2020-01-30 14:08:48', '0', '0', '1');
INSERT INTO `grado` VALUES ('16', '11° High School Encino', '10400', '20190212151432.00', '2020-01-30 14:08:51', '0', '0', '1');
INSERT INTO `grado` VALUES ('17', '12° High School Encino', '10400', '20190212151433.00', '2020-01-30 14:08:52', '0', '0', '1');
INSERT INTO `grado` VALUES ('18', '1° Elementary Triana', '7690', '20190212151433.00', '2020-01-30 14:41:36', '0', '0', '2');
INSERT INTO `grado` VALUES ('19', '2° Elementary Triana', '7690', '20190212151433.00', '2020-01-30 14:41:35', '0', '0', '2');
INSERT INTO `grado` VALUES ('20', '3° Elementary Triana', '7690', '20190212151433.00', '2020-01-30 14:41:34', '0', '0', '2');
INSERT INTO `grado` VALUES ('21', '4° Elementary Triana', '7690', '20200130135910.00', '2020-01-30 14:41:33', '0', '0', '2');
INSERT INTO `grado` VALUES ('22', '5° Junior High Triana', '7690', '20200130135910.00', '2020-01-30 14:41:32', '0', '0', '2');
INSERT INTO `grado` VALUES ('23', '6° Junior High Triana', '7690', '20200130135910.00', '2020-01-30 14:41:31', '0', '0', '2');
INSERT INTO `grado` VALUES ('24', '7° Junior High Triana', '8800', '20200130135910.00', '2020-01-30 14:41:30', '0', '0', '2');
INSERT INTO `grado` VALUES ('25', '8° Junior High Triana', '8800', '20200130135910.00', '2020-01-30 14:41:29', '0', '0', '2');
INSERT INTO `grado` VALUES ('26', '9° High School Triana', '8800', '20200130135910.00', '2020-01-30 14:11:12', '0', '0', '2');
INSERT INTO `grado` VALUES ('27', '10° High School Triana', '10600', '20200130135910.00', '2020-01-30 14:11:12', '0', '0', '2');
INSERT INTO `grado` VALUES ('28', '11° High School Triana', '10600', '20200130135910.00', '2020-01-30 14:11:12', '0', '0', '2');
INSERT INTO `grado` VALUES ('29', '12° High School Triana', '10600', '20200130135910.00', '2020-01-30 14:11:12', '0', '0', '2');
DROP TRIGGER IF EXISTS `grado`;
DELIMITER ;;
CREATE TRIGGER `grado` BEFORE INSERT ON `grado` FOR EACH ROW SET NEW.fecha_registro = NOW()
;;
DELIMITER ;

ALTER TABLE `estudio`
ADD COLUMN `acepto_aviso_wise`  tinyint(4) NULL DEFAULT '0';

ALTER TABLE `estudio`
ADD COLUMN `acepto_reglamento`  tinyint(4) NULL DEFAULT '0';

ALTER TABLE `estudio`
ADD COLUMN `leido`  tinyint(4) NULL DEFAULT '0';


DROP TABLE IF EXISTS `usuario_institucion_rel`;
CREATE TABLE `usuario_institucion_rel` (
  `usuario_id_ins` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) DEFAULT NULL,
  `institucion_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`usuario_id_ins`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

