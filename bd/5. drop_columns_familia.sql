alter table familia drop COLUMN profesion_papa;
alter table familia drop COLUMN ocupacion_papa;
alter table familia drop COLUMN empresa_papa;
alter table familia drop COLUMN puesto_papa;
alter table familia drop COLUMN giro_papa;
alter table familia drop COLUMN dueno_papa;
alter table familia drop COLUMN antiguedad_papa;
alter table familia drop COLUMN sueldo_papa;

alter table familia drop COLUMN profesion_mama;
alter table familia drop COLUMN ocupacion_mama;
alter table familia drop COLUMN empresa_mama;
alter table familia drop COLUMN puesto_mama;
alter table familia drop COLUMN giro_mama;
alter table familia drop COLUMN dueno_mama;
alter table familia drop COLUMN antiguedad_mama;
alter table familia drop COLUMN sueldo_mama;