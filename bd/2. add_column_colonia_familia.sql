alter table familia add column id_colonia int;
alter table familia add FOREIGN KEY (id_colonia) REFERENCES colonias(id_colonia) ON UPDATE CASCADE ON DELETE RESTRICT;