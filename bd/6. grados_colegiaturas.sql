
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS grado;

CREATE TABLE grado (
	id_grado INT (11) NOT NULL AUTO_INCREMENT,
	nombre_grado VARCHAR (255) COLLATE utf8_spanish_ci DEFAULT NULL,
	colegiatura VARCHAR (255) COLLATE utf8_spanish_ci DEFAULT NULL,
	fecha_registro DECIMAL(60,2),
	fecha_modificacion TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	borrado TINYINT (4) DEFAULT '0',
	PRIMARY KEY (id_grado)
);


DROP TRIGGER IF EXISTS grado;
DELIMITER ;;


CREATE TRIGGER grado BEFORE INSERT ON grado FOR EACH ROW
SET NEW.fecha_registro = NOW();;
DELIMITER ;



SET FOREIGN_KEY_CHECKS = 1;

insert into grado(nombre_grado, grado) values('Kinder I', '950');