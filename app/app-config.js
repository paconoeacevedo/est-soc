(function () { 
    'use strict';
        
    angular
        .module('app')
        .config(config);

    function config ($stateProvider, $urlRouterProvider, $mdThemingProvider, momentPickerProvider, moment) {
        $mdThemingProvider.disableTheming();
	momentPickerProvider.options(configDatePicker(moment));
        
	$urlRouterProvider.otherwise('/');
		
	$stateProvider
        
            .state('/app', {
                    abstract: true,
                    controller: 'DashboardController',
                    controllerAs: 'dash',
                    templateUrl: 'tpl.html?v='+new Date().getTime()+''
            })
            
            .state('/app.dashboard', { 
                url: '/',
                views: {
                    'page': { 
                        templateUrl: 'app/dashboard/views/dashboard.tpl.html?v='+new Date().getTime()+''
                    }
                }      
            })
            
            .state('/app.estudios', { 
                url: '/estudios/ver',
                views: {
                    'page': { 
                        templateUrl: 'app/estudios/views/estudios.ver.tpl.html?v='+new Date().getTime()+''
                    }
                }      
            })
            
            .state('/app.estudios_crear', { 
                url: '/estudios/crear',
                views: {
                    'page': { 
                        templateUrl: 'app/estudios/views/estudios.crear.tpl.html?v='+new Date().getTime()+''
                    }
                }      
            })
            
            .state('/app.estudios_detalle', { 
                url: '/estudios/detalle/:id',
                views: {
                    'page': { 
                        templateUrl: 'app/estudios/views/estudios.detalle.tpl.html?v='+new Date().getTime()+''
                    }
                }      
            })

            .state('/app.estudios_detalle_insigna', { 
                url: '/estudios/detalle-insigna/:id',
                views: {
                    'page': { 
                        templateUrl: 'app/estudios/views/estudios.detalle.insigna.tpl.html?v='+new Date().getTime()+''
                    }
                }      
            })
            
            .state('/app.estudios-seguimiento', { 
                url: '/estudios/detalle/:id/seguimiento',
                views: {
                    'page': { 
                        templateUrl: 'app/estudios/views/estudios.ver.seguimiento.tpl.html?v='+new Date().getTime()+''
                    }
                }      
            })
            
            .state('/estudios_reporte', { 
                url: '/estudios/reporte/:id',
                controller: 'EstudiosReporteController',
                controllerAs: 'scope',
                templateUrl: 'app/estudios/views/reporte.tpl.html?v='+new Date().getTime()+''
            })

            .state('/estudios_reporte_iter', { 
                url: '/estudios/reporte-iter/:id',
                controller: 'EstudiosReporteIterController',
                controllerAs: 'scope',
                templateUrl: 'app/estudios/views/reporte-iter.tpl.html?v='+new Date().getTime()+''
            })

            .state('/estudios_reporte_insigna', { 
                url: '/estudios/reporte-insigna/:id',
                controller: 'EstudiosReporteInsignaController',
                controllerAs: 'scope',
                templateUrl: 'app/estudios/views/reporte-insigna.tpl.html?v='+new Date().getTime()+''
            })
            
            .state('/app.admin', { 
                url: '/admin/index',
                views: {
                    'page': { 
                        templateUrl: 'app/admin/views/admin.tpl.html?v='+new Date().getTime()+''
                    }
                }      
            })
            
            .state('/app.admin_usuario', { 
                url: '/admin/usuario/ver',
                views: {
                    'page': { 
                        templateUrl: 'app/admin/views/usuario.ver.tpl.html?v='+new Date().getTime()+''
                    }
                }      
            })
            
            .state('/app.admin_institucion', { 
                url: '/admin/institucion/ver',
                views: {
                    'page': { 
                        templateUrl: 'app/admin/views/institucion.ver.tpl.html?v='+new Date().getTime()+''
                    }
                }      
            })
            
            .state('/app.user_info', { 
                url: '/user/info',
                views: {
                    'page': { 
                        templateUrl: 'app/user/views/user.info.tpl.html?v='+new Date().getTime()+''
                    }
                }      
            })
            
            .state('/app.admin_famlias', { 
                url: '/admin/fam/ver',
                views: {
                    'page': { 
                        templateUrl: 'app/admin/views/fam.ver.tpl.html?v='+new Date().getTime()+''
                    }
                }      
            })
            
            .state('/app.admin_colnias', { 
                url: '/admin/colonias',
                views: {
                    'page': { 
                        templateUrl: 'app/admin/views/colonias.tpl.html?v='+new Date().getTime()+''
                    }
                }      
            })
            
            .state('/app.admin_catciclos', { 
                url: '/admin/cat/ver',
                views: {
                    'page': { 
                        templateUrl: 'app/admin/views/cat.ver.tpl.html?v='+new Date().getTime()+''
                    }
                }      
            })
            
            .state('/about', { 
                url: '/about',
                controller: '',
                templateUrl: 'app/home/views/about.tpl.html?v='+new Date().getTime()+''
            })
            
            .state('/store', { 
                url: '/store',
                controller: '',
                templateUrl: 'app/store/views/store.tpl.html?v='+new Date().getTime()+''
            })
            
            .state('/account', { 
                url: '/account',
                controller: '',
                templateUrl: 'app/account/views/account.tpl.html?v='+new Date().getTime()+''
            })
            
            .state('/register', { 
                url: '/register',
                controller: '',
                templateUrl: 'app/login/views/signup.tpl.html?v='+new Date().getTime()+''
            })
            
            .state('login', { 
                url: '/login',
                controller: '',
                templateUrl: 'app/login/views/login.tpl.html?v='+new Date().getTime()+''
            })
            
            .state('/autocaptura', { 
                url: '/auto-captura/:token',
                controller: 'AutoCapturaController',
                controllerAs: 'cl',
                templateUrl: 'app/auto-captura/views/auto-captura.tpl.html?v='+new Date().getTime()+''
            })

            .state('/autocaptura.init', { 
                url: '/init',
                templateUrl: 'app/auto-captura/views/init.tpl.html?v='+new Date().getTime()+''
            })

            .state('/autocapturainsigna', { 
                url: '/auto-captura-insigna/:token',
                controller: 'AutoCapturaInsignaController',
                controllerAs: 'cl',
                templateUrl: 'app/auto-captura/views/auto-captura-insigna.tpl.html?v='+new Date().getTime()+''
            })

            .state('/autocapturainsigna.init', { 
                url: '/init',
                templateUrl: 'app/auto-captura/views/init-insigna.tpl.html?v='+new Date().getTime()+''
            })
            
            .state('/autocapturaiter', { 
                url: '/auto-captura-iter/:token',
                controller: 'AutoCapturaIterController',
                controllerAs: 'cl',
                templateUrl: 'app/auto-captura/views/auto-captura-iter.tpl.html?v='+new Date().getTime()+''
            })

            .state('/autocapturaiter.init', { 
                url: '/init',
                templateUrl: 'app/auto-captura/views/init-iter.tpl.html?v='+new Date().getTime()+''
            })
            
    }
    
    function configDatePicker (moment) {
        return {
            /* Picker properties */
            locale:        'es',
            format:        'L LTS', /*Sin segundos: LL LT*/
            minView:       'decade',
            maxView:       'minute',
            startView:     'month',
            autoclose:     true,
            today:         false,
            keyboard:      false,
            
            /* Extra: Views properties */
            leftArrow:     '&larr;',
            rightArrow:    '&rarr;',
            yearsFormat:   'YYYY',
            monthsFormat:  'MMM',
            daysFormat:    'D',
            hoursFormat:   'HH:[00]',
            minutesFormat: moment.localeData().longDateFormat('LT').replace(/[aA]/, ''),
            secondsFormat: 'ss',
            minutesStep:   5,
            secondsStep:   5
        };
    }

})();
