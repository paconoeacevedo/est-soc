(function () {
    'use strict';

    angular
            .module('app.admin')
            .controller('FamController', FamController);

    function FamController($localStorage, AdminService, ColoniaService, FamiliaService, $rootScope, $state, $mdDialog, $mdToast, DialogService, RestService, AuthenticationService, Constants, UserService) {
        /* jshint validthis: true */
        console.log('init FamController panel');
        show();
        //Check session
        if (!AuthenticationService.isAuth()) {
            window.location.href = '#/login';
            return;
        };
        var scope = this;
        scope.user = {};
        scope.tipoUsuario = 0;
        scope.rolUsuario = 0;
        scope.institucion = {};
        scope.user = $localStorage.data.user;
        scope.tipoUsuario = $localStorage.globals.type;
        scope.rolUsuario = $localStorage.globals.role;
        if (scope.tipoUsuario == '2') {
            scope.institucion = $localStorage.data.institucion;
        } 
        scope.familias = [];
        scope.familiasMostrar = [];
        scope.familia = {};

        scope.currentPage = 0;
        scope.pageSize = 7;
        scope.numberOfPages = numberOfPages;
        scope.colonias = [];
        scope.zonas = [];

        scope.busqueda = {familia: ""};
        scope.cambiaFiltro = cambiaFiltro;
        function cambiaFiltro() {
            console.log(scope.busqueda);
            scope.familiasMostrar = [];
            for (var i = 0; i < scope.familias.length; i++) {
                var banFam = false;
                console.log(1);
                if (scope.busqueda.familia === "") {
                    banFam = true;
                } else if (scope.familias[i].familia === scope.busqueda.familia) {
                    banFam = true;
                }
                ;

                if (banFam) {
                    scope.familiasMostrar.push(scope.familias[i]);
                }
                ;
            }
            ;

            scope.currentPage = 0;
        }
        
        function numberOfPages() {
            return Math.ceil(scope.familiasMostrar.length / scope.pageSize);
        }
        
        scope.verActualizarFamilia = verActualizarFamilia;
        scope.eliminarFamilia = eliminarFamilia;
        scope.guardarFamilia = guardarFamilia;
        scope.cancelar = cancelar;
        scope.getColoniasPorZona = getColoniasPorZona;
        scope.getFamilias = getFamilias;

        scope.getFamilias();
        scope.data = {};
        function getFamilias() {
            AdminService.getFamilias(scope.data).then(
                    function (response) {
                        scope.familias = response.data;
                        scope.familiasMostrar = scope.familias;
                        hide();
                    },
                    function (e) {}
            );
        }

        ColoniaService.getColonias().then(
                function (response) {
                    scope.colonias = response.data;
                },
                function (e) {}
        );

        ColoniaService.getZonas().then(
                function (response) {
                    scope.zonas = response.data;
                },
                function (e) {}
        );

        function verActualizarFamilia(f) {
            scope.familia = f;
            $("#modal_familia_update").modal('show');
        }

        function getColoniasPorZona(zona) {
            ColoniaService.getColoniasByZona(zona).then(
                    function (response) {
                        scope.colonias = response.data;
                    },
                    function (e) {}
            );
        }

        function guardarFamilia() {
            confirmaMsj("Confirmación de solicitud",
                    "¿Actualizar información?",
                    "Si",
                    function () {
                        delete scope.familia.clave_institucion;
                        delete scope.familia.nombre_colonia;
                        delete scope.familia.zona;
                        FamiliaService.actualizarFamilia(scope.familia).then(
                                function (response) {
                                    scope.getFamilias();
                                    $("#modal_familia_update").modal('hide');
                                    mensaje('success', 'Guardar información', 'Se han guardado los datos correctamente');
                                    show();
                                },
                                function (e) {
                                    mensaje('error', 'Guardar información', 'Ocurrio un error al guardar la información, intente mas tarde.');
                                });
                    },
                    "No",
                    function () {}
            );
        }

        function eliminarFamilia(f) {
            var data = {};
            data.id_familia = f.id_familia;
            data.borrado = '1';
            console.log(data);
            confirmaMsj("Confirmación de solicitud",
                    "¿Está seguro de eliminar a la familia?",
                    "Si",
                    function () {

                        FamiliaService.actualizarFamilia(data).then(
                                function (response) {
                                    mensaje('success', 'Guardar información', 'Se han guardado los datos correctamente');
                                    window.location.reload();
                                },
                                function (e) {
                                    mensaje('error', 'Guardar información', 'Ocurrio un error al guardar la información, intente mas tarde.');
                                });
                    },
                    "No",
                    function () {}
            );
        }

        function cancelar() {
            $("#modal_familia_update").modal('hide');
            scope.familia = {};
        }
    }
    ;//end controller

})();