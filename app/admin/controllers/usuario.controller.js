(function () {
    'use strict';

    angular
        .module('app.admin')
        .controller('UsuarioController', UsuarioController);

    function UsuarioController($localStorage, AdminService, $rootScope, $state, $mdDialog, $mdToast, DialogService, RestService, AuthenticationService, Constants, UserService, $q) {
        /* jshint validthis: true */
        console.log('init usuario controller');
        show();
        //Check session
        if (!AuthenticationService.isAuth()) {
            window.location.href = '#/login';
            return;
        };
        var scope = this;
        scope.user = {};
        scope.tipoUsuario = 0;
        scope.rolUsuario = 0;
        scope.institucion = {};
        scope.user = $localStorage.data.user;
        scope.tipoUsuario = $localStorage.globals.type;
        scope.rolUsuario = $localStorage.globals.role;
        if (scope.tipoUsuario == '2') {
            scope.institucion = $localStorage.data.institucion;
        }


        /*UserService.getUser().then(
            function(response){
                scope.user=scope.user=response.data.usuario;
                if(scope.tipoUsuario==='2'){
                    scope.institucion=scope.institucion=response.data.institucion;
                }
                hide();
            }, function(error){
                console.log('Error al guardar completar la solicitud: '+error);
            }
        );

        if(scope.tipoUsuario==='2'){
            if(scope.user===undefined||scope.user.id_institucion===undefined){
                error();
                location.href='#/';
            }
        }
        
        if ($localStorage.globals) {
            scope.isAuth=true;
            scope.tipoUsuario=$localStorage.globals.type;
            scope.rolUsuario=$localStorage.globals.role;
        }*/



        scope.cargaUsuarios = cargaUsuarios;
        scope.agregarUsuario = agregarUsuario;
        scope.verActualizarUsuario = verActualizarUsuario;
        scope.guardarUsuario = guardarUsuario;
        scope.eliminarUsuario = eliminarUsuario;
        scope.cancelar = cancelar;

        scope.usuarios = [];
        scope.usuario = {};
        scope.nuevoUsuario = true;
        scope.updateUsuario = false;
        scope.checkUser = checkUser;

        scope.validUser = false;
        scope.checkingUser = false;
        scope.NotvalidUser = false;

        scope.cargaUsuarios();


        function checkUser() {
            console.log(scope.usuario.username);
            if (scope.updateUsuario) {
                return;
            }
            if (scope.usuario.username === undefined) {
                scope.validUser = false;
                scope.NotvalidUser = false;
                scope.checkingUser = false;
                return;
            }
            if (scope.usuario.username !== null || scope.usuario.username !== "" || scope.usuario.username !== undefined) {
                scope.checkingUser = false;
                scope.NotvalidUser = false;
                scope.checkingUser = true;
                var data = {};
                data.tipo_usuario = scope.tipoUsuario;
                data.username = scope.usuario.username;
                UserService.checkUser(data).then(
                    function (response) {
                        var data = response.data;
                        if (data === 1) {
                            scope.NotvalidUser = true;
                            scope.validUser = false;
                        } else if (data === 0) {
                            scope.NotvalidUser = false;
                            scope.validUser = true;
                        }
                        scope.checkingUser = false;
                    },
                    function (err) {
                        console.log('Error al buscar username');
                        scope.validUser = false;
                        scope.NotvalidUser = false;
                        scope.checkingUser = false;
                    }
                );
            } else {
                scope.validUser = false;
                scope.NotvalidUser = false;
                scope.checkingUser = false;
            }

        }
        function cargaUsuarios() {
            AdminService.getUserList(scope.tipoUsuario, scope.user.id_institucion).then(
                function (response) {
                    scope.usuarios = response.data;
                    hide();
                },
                function (error) {
                    console.log('Error al obtener los usuarios' + error);
                    hide();
                    error();
                }
            );
        }

        function agregarUsuario() {
            scope.nuevoUsuario = true;
            scope.updateUsuario = false;
            scope.usuario = {};
            $("#modal_agregar_usuario").modal('show');
        }

        function verActualizarUsuario(usuario) {
            scope.nuevoUsuario = false;
            scope.updateUsuario = true;
            scope.usuario = usuario;
            $("#modal_agregar_usuario").modal('show');
        }

        function guardarUsuario() {
            if (scope.updateUsuario) {
                confirmaMsj(
                    "Confirmación de solicitud",
                    "¿Actualizar información del usuario?",
                    "Si",
                    function () {
                        var obj = scope.usuario;
                        obj.tipo_usuario = scope.tipoUsuario;
                        show();
                        var promesas = [];
                        promesas.push(AdminService.updateUser(obj));
                        $q.all(promesas)
                            .then(
                                function (response) {
                                    mensaje('success', 'Aviso.', 'Se actualizaron los datos del usuario correctamente.');
                                    scope.usuarios = response[0].data;
                                    scope.cancelar();
                                    hide();
                                }, function (error) {
                                    console.log('Error al guardar completar la solicitud: ' + error);
                                }
                            );
                    },
                    "No",
                    function () { }
                );
            }
            if (scope.nuevoUsuario) {
                if (scope.usuario.username === undefined ||
                    scope.usuario.username === "" || scope.NotvalidUser) {
                    mensaje('error', 'Error de validación', 'Verifique sus datos');
                    return;
                }
                confirmaMsj(
                    "Confirmación de solicitud",
                    "¿Crear nuevo usuario?",
                    "Si",
                    function () {
                        var obj = scope.usuario;
                        obj.tipo_usuario = scope.tipoUsuario;
                        obj.id_institucion = scope.user.id_institucion;
                        obj.estatus = 0;
                        show();
                        var promesas = [];
                        promesas.push(AdminService.addUser(obj));
                        $q.all(promesas)
                            .then(
                                function (response) {
                                    mensaje('success', 'Aviso.', 'Se dio de alta al nuevo usuario correctamente.');
                                    scope.usuarios = response[0].data;
                                    scope.cancelar();
                                    hide();
                                }
                            );
                    },
                    "No",
                    function () { }
                );
            }
        }

        function eliminarUsuario(usuario) {
            confirmaMsj(
                "Confirmación de solicitud",
                "¿Esta seguro de eliminar usuario? Esta es una acción permanente.",
                "Si",
                function () {
                    var obj = usuario;
                    obj.tipo_usuario = scope.tipoUsuario;
                    show();
                    var promesas = [];
                    promesas.push(AdminService.deleteUser(obj));
                    $q.all(promesas)
                        .then(
                            function (response) {
                                mensaje('success', 'Aviso.', 'Se eliminó al usuario correctamente.');
                                scope.usuarios = response[0].data;
                                scope.cancelar();
                                hide();
                            }
                        );
                },
                "No",
                function () { }
            );
        }

        function cancelar() {
            scope.usuario = {};
            scope.nuevoUsuario = true;
            scope.updateUsuario = false;
            scope.validUser = false;
            scope.NotvalidUser = false;
            scope.checkingUser = false;
            $("#modal_agregar_usuario").modal('hide');
        }


    };//end controller

})();