(function () {
    'use strict';

    angular
        .module('app')
        .controller('ColoniasController', ColoniasController);

    function ColoniasController(
        ColoniaService, $localStorage, $rootScope, $state, $mdDialog, $mdToast, DialogService, RestService, AuthenticationService, Constants, UserService, $q, $location) {
        /* jshint validthis: true */
        console.log('init ColoniasController');

        show();
        //Check session
        if (!AuthenticationService.isAuth()) {
            window.location.href = '#/login';
            return;
        };
        var scope = this;
        scope.user = {};
        scope.tipoUsuario = 0;
        scope.rolUsuario = 0;
        scope.institucion = {};
        scope.user = $localStorage.data.user;
        scope.tipoUsuario = $localStorage.globals.type;
        scope.rolUsuario = $localStorage.globals.role;
        if (scope.tipoUsuario == '2') {
            scope.institucion = $localStorage.data.institucion;
        }

        scope.listaColonias = [];
        scope.listaColoniasMostrar = [];
        scope.listaZonas = [];
        scope.colonia = {};

        scope.busqueda = { zona: "" };
        scope.cambiaFiltro = cambiaFiltro;
        scope.currentPage = 0;
        scope.pageSize = 7;
        scope.numberOfPages = numberOfPages;


        scope.view = view;
        scope.add = add;
        scope.save = save;
        scope.load = load;
        scope.delet = delet;

        scope.update = false;
        scope.is_saving = false;

        scope.load();


        function numberOfPages() {
            return Math.ceil(scope.listaColoniasMostrar.length / scope.pageSize);
        }

        function add() {
            scope.update = false;
            scope.colonia = {};
            $("#modalX").modal('show');
        }

        function view(colonia) {
            scope.colonia = colonia;
            scope.update = true;
            $("#modalX").modal('show');
        }

        function load() {
            ColoniaService.getColonias()
                .then(
                    function (response) {
                        scope.listaColonias = response.data;
                        scope.listaColoniasMostrar = scope.listaColonias;
                        ColoniaService.getZonas()
                            .then(
                                function (response) {
                                    scope.listaZonas = response.data;
                                    hide();
                                },
                                function (error) {
                                    errorRequest(error);
                                }
                            );
                    },
                    function (error) {
                        errorRequest(error);
                    }
                );
        }

        function save() {

            console.log(scope.colonia);
            //return;
            scope.is_saving = true;
            if (!scope.update) {
                ColoniaService.create(scope.colonia)
                    .then(
                        function (response) {
                            success();
                            $("#modalX").modal('hide');
                            scope.colonia = {};
                            scope.load();
                        },
                        function (error) {
                            errorRequest(error);
                        }
                    );
            } else {
                delete scope.colonia.fecha_modificacion;
                delete scope.colonia.fecha_registro;
                delete scope.colonia.borrado;
                ColoniaService.update(scope.colonia)
                    .then(
                        function (response) {
                            success();
                            $("#modalX").modal('hide');
                            scope.materia = {};
                            scope.load();
                        },
                        function (error) {
                            errorRequest(error);
                        }
                    );
            }
            scope.is_saving = false;

        }

        function delet(colonia, ev) {
            var quest = '¿Eliminar colonia?';
            var confirm = DialogService.dialogConfirm(ev, 'Confirmación', quest, 'Si', 'No');
            $mdDialog.show(confirm).then(function () {
                ColoniaService.delet(colonia.id_colonia)
                    .then(
                        function (response) {
                            success();
                            scope.colonia = {};
                            scope.load();
                        },
                        function (error) {
                            errorRequest(error);
                        }
                    );
            }, function () {
                $mdDialog.hide();
            });
        }

        function cambiaFiltro() {
            console.log(scope.busqueda.zona);
            scope.listaColoniasMostrar = [];
            for (var i = 0; i < scope.listaColonias.length; i++) {
                var banZona = false;

                if (scope.busqueda.zona === "") {
                    banZona = true;
                } else if (scope.listaColonias[i].zona === scope.busqueda.zona) {
                    banZona = true;
                }
                ;

                if (banZona) {
                    scope.listaColoniasMostrar.push(scope.listaColonias[i]);
                }
                ;
            }
            ;

            scope.currentPage = 0;
        }
        ;


    }
    ;//end controller

})();