(function () { 
    'use strict';
	
    angular
        .module('app.admin')
        .factory('ColoniaService', ColoniaService);

    function ColoniaService(RestService, Constants) {
        var service = {};
        
        service.create = create;
        service.update = update;
        service.getColoniaById = getColoniaById;
        service.getColonias = getColonias;
        service.getColoniasByZona = getColoniasByZona;
        service.getZonas = getZonas;
        service.delet = delet;
        
        return service;
        
        function create(data){
            var url = Constants.BaseURLBack+'/colonia/create';
            return RestService.post(url, '', data);
        }
        
        function update(data){
            var url = Constants.BaseURLBack+'/colonia/update';
            return RestService.post(url, '', data);
        }
        
        function getColoniaById(id){
            var url=Constants.BaseURLBack+'/colonia/get/'+id;
            return RestService.get(url,'');
        }
        
        function delet(id){
            var url=Constants.BaseURLBack+'/colonia/delete/'+id;
            return RestService.delet(url, '');
        }
        
        function getColonias(){
            var url=Constants.BaseURLBack+'/colonia/getAll';
            return RestService.get(url,'');
        }
        
        function getColoniasByZona(zona){
            var url=Constants.BaseURLBack+'/colonia/getColoniasPorZona/'+zona;
            return RestService.get(url,'');
        }
        
        function getZonas(){
            var url=Constants.BaseURLBack+'/colonia/getZonas';
            return RestService.get(url,'');
        }
        
        
        
    }

})();
