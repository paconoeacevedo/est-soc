(function () {
    'use strict';

    angular
        .module('app')
        .constant('Constants', {
            GRUPO_INSIGNA: "9",
            GRUPO_ITER: "10",

            //dev
            BaseURLBack: 'http://localhost/est-soc/api',
            BaseURLFront: 'http://localhost/est-soc/#/'


            //pre
            /*BaseURLBack : 'http://fnar.com.mx/est-soc/api',
            BaseURLFront : 'http://fnar.com.mx/est-soc/#/'*/

            /*
            //local
            BaseURLBack : 'http://hrwise.local/est-soc/api',
            BaseURLFront : 'http://hrwise.local/est-soc/#/'
            */

            //prod
            //BaseURLBack : 'https://www.hrwise.com.mx/est-soc/api',
            //BaseURLFront : 'https://www.hrwise.com.mx/est-soc/#/'

        });

})();
