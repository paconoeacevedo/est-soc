(function () { 
    'use strict';
	
    angular
        .module('app')
        .factory('GradoService', GradoService);

    function GradoService(RestService, Constants) {
        var service = {};
        
        service.create = create;
        service.update = update;
        service.get = get;
        service.delet = delet;
        service.getAll = getAll;
        service.getByNombrePost = getByNombrePost;
        service.getListaGradosByClaveIns = getListaGradosByClaveIns;
        
        return service;
        
        function create(data){
            var url = Constants.BaseURLBack+'/grado/create';
            return RestService.post(url, '', data);
        }
        
        function update(data){
            var url = Constants.BaseURLBack+'/grado/update';
            return RestService.post(url, '', data);
        }
        
        function getByNombrePost(data){
            var url = Constants.BaseURLBack+'/grado/getByNombrePost';
            return RestService.post(url, '', data);
        }

        function getListaGradosByClaveIns(data){
            var url = Constants.BaseURLBack+'/grado/getListaGradosByClaveIns';
            return RestService.post(url, '', data);
        }

        function getByNombrePost(data){
            var url = Constants.BaseURLBack+'/grado/getByNombrePost';
            return RestService.post(url, '', data);
        }
        
        function get(id){
            var url=Constants.BaseURLBack+'/grado/get/'+id;
            return RestService.get(url,'');
        }
        
        function delet(id){
            var url=Constants.BaseURLBack+'/grado/delete/'+id;
            return RestService.delet(url, '');
        }
        
        function getAll(){
            var url=Constants.BaseURLBack+'/grado/getAll';
            return RestService.get(url,'');
        }
        
    }

})();
