(function () {
    'use strict';

    angular
        .module('app.estudios')
        .factory('EstudiosService', EstudiosService);

    function EstudiosService($http, RestService, $localStorage, $rootScope, Base64Service, Constants) {
        var service = {};
        service.idEstudioSeleccionado = 0;
        service.obtenerEmpleados = obtenerEmpleados;
        service.guardarAsignado = guardarAsignado;
        service.obtenerFamilias = obtenerFamilias;
        service.guardarFamilia = guardarFamilia;
        service.guardarEstudio = guardarEstudio;
        service.obtenerEstudios = obtenerEstudios;
        service.guardarEstudioInstitucion = guardarEstudioInstitucion;
        service.obtenerDetalleEstudio = obtenerDetalleEstudio;
        service.actualizarEstudio = actualizarEstudio;
        service.cancelarEstudioInstitucion = cancelarEstudioInstitucion;
        service.obtenerCicloEscolar = obtenerCicloEscolar;
        service.obtenerCicloEscolarCat = obtenerCicloEscolarCat;
        service.obtenerEstatusCat = obtenerEstatusCat;
        service.actualizarComentarios = actualizarComentarios;
        service.obtenerFamilia = obtenerFamilia;
        service.descargarArchivo = descargarArchivo;
        service.descargarReportesEstudios = descargarReportesEstudios;
        service.obtenerFolio = obtenerFolio;
        service.reenviarLiga = reenviarLiga;
        service.sumarDiasByEstudio = sumarDiasByEstudio;
        return service;

        function sumarDiasByEstudio(id) {
            var url = Constants.BaseURLBack + '/estudio/sumarDiasByEstudio/'+id;
            return RestService.post(url, '', null);
        }
        
        function reenviarLiga(data) {
            var url = Constants.BaseURLBack + '/estudio/reenviarLigaCaptura';
            return RestService.post(url, '', data);
        }

        function obtenerFolio(data) {
            var url = Constants.BaseURLBack + '/estudio/obtenerFolio';
            return RestService.post(url, '', data);
        }

        function descargarReportesEstudios(params) {
            var url = Constants.BaseURLBack + '/reporte/solicitudesEstudios';
            return RestService.download(url, params);
        }

        function descargarArchivo(nombre) {
            var url = Constants.BaseURLBack + '/reporte/download/' + nombre;
            return RestService.download(url, null);
        }

        function cancelarEstudioInstitucion(idCancelacion) {
            var url = Constants.BaseURLBack + '/estudio/cancelEstudioInstitucion/' + idCancelacion;
            return RestService.get(url, '');
        }

        function obtenerEmpleados() {
            var url = Constants.BaseURLBack + '/user/getEmpleados';
            return RestService.get(url, '');
        }

        function actualizarEstudio(data) {
            var url = Constants.BaseURLBack + '/estudio/updateEstudio';
            return RestService.post(url, '', data);
        }

        function guardarAsignado(data) {
            var url = Constants.BaseURLBack + '/estudio/updateEstudio';
            return RestService.post(url, '', data);
        }

        function obtenerFamilia(id) {
            var url = Constants.BaseURLBack + '/estudio/getFamilia/' + id;
            return RestService.get(url, '');
        }

        function obtenerFamilias(data) {
            var url = Constants.BaseURLBack + '/estudio/getFamilias';
            return RestService.post(url, '', data);
        }

        function guardarFamilia(data) {
            var url = Constants.BaseURLBack + '/estudio/saveFamilia';
            return RestService.post(url, '', data);
        }

        function guardarEstudioInstitucion(data) {
            var url = Constants.BaseURLBack + '/estudio/saveEstudioInstitucion';
            return RestService.post(url, '', data);
        }

        function guardarEstudio(data) {
            var url = Constants.BaseURLBack + '/estudio/saveEstudio';
            return RestService.post(url, '', data);
        }

        function obtenerEstudios(params) {
            if (params.filtroFamilia === undefined || params.filtroFamilia === '') {
                params.filtroFamilia = 'all';
            }
            if (params.idInstitucion === undefined || params.idInstitucion === '') {
                params.idInstitucion = '0';
            }
            var url = Constants.BaseURLBack + '/estudio/getEstudios';
            return RestService.post(url, '', params);
        }

        function obtenerDetalleEstudio(idEstudio, idInstitucion) {
            var url = Constants.BaseURLBack + '/estudio/getEstudioDetalle/' + idEstudio + '/' + idInstitucion;
            return RestService.get(url, '');
        }

        function obtenerCicloEscolar() {
            var url = Constants.BaseURLBack + '/estudio/getCicloEscolar';
            return RestService.get(url, '');
        }

        function obtenerCicloEscolarCat() {
            var url = Constants.BaseURLBack + '/estudio/getCicloEscolarCat';
            return RestService.get(url, '');
        }

        function obtenerEstatusCat() {
            var url = Constants.BaseURLBack + '/estudio/getEstatusCat';
            return RestService.get(url, '');
        }

        function actualizarComentarios(data) {
            var url = Constants.BaseURLBack + '/estudio/actualizarComentarios';
            return RestService.post(url, '', data);
        }

    }

})();
