(function () {
    'use strict';

    angular
        .module('app.estudios')
        .controller('EstudiosVerController', EstudiosVerController);

    function EstudiosVerController(AdminService, ColoniaService, EstudiosService, ComentarioService, $localStorage, $rootScope, $state, $mdDialog, $mdToast, DialogService, RestService, AuthenticationService, Constants, UserService, $q) {
        /* jshint validthis: true */
        console.log('init estudios ver');
        show();
        //Check session
        if (!AuthenticationService.isAuth()) {
            window.location.href = '#/login';
            return;
        };
        var scope = this;
        scope.user = {};
        scope.tipoUsuario = 0;
        scope.rolUsuario = 0;
        scope.institucion = {};
        scope.grupo = {};
        scope.GRUPO_INSIGNA = Constants.GRUPO_INSIGNA;
        scope.GRUPO_ITER = Constants.GRUPO_ITER;
        scope.id_grupo = '';
        scope.instituciones = [];
        scope.user = $localStorage.data.user;
        scope.tipoUsuario = $localStorage.globals.type;
        scope.rolUsuario = $localStorage.globals.role;
        if (scope.tipoUsuario == '2') {
            scope.institucion = $localStorage.data.institucion;
            scope.instituciones = $localStorage.data.instituciones;
        }
        if (![null, undefined].includes(scope.institucion.grupo)) {
            scope.grupo = scope.institucion.grupo;
            scope.id_grupo = scope.grupo.id_institucion;
        }
        scope.listaInstituciones = [];
        scope.listaEstudios = [];
        scope.listaEstudiosMostrar = [];
        scope.busqueda = {
            id_ciclo_escolar: "", id_usuario_asignado: "", id_estatus_estudio: "", zona: "", id_grupo: "", filtro_uno: false
            , filtro_dos: false, filtro_tres: false
        };
        scope.cambiaFiltro = cambiaFiltro;
        scope.estudio = {};
        scope.familia = {};
        scope.familia.familia = '';
        scope.filtroFamilia = "";

        scope.currentPage = 0;
        scope.pageSize = 7;
        scope.numberPages = 0
        scope.numberOfPages = numberOfPages;
        scope.seleccionados = [];
        //Banderas
        scope.load = false;
        scope.verAccion = false;

        scope.entrevista = {};
        scope.listaDondePago = [
            { 'pago': 'TRIANA', 'label': 'Triana' },
            { 'pago': 'ENCINO', 'label': 'Encino' },
            { 'pago': 'KV', 'label': 'Kinder Villa' }
        ];

        //acctiones
        scope.buscarEstudios = buscarEstudios;
        scope.verDetalle = verDetalle;
        scope.agendarView = agendarView;
        scope.agendarEstudio = agendarEstudio;
        scope.reagendarEstudio = reagendarEstudio;
        scope.cancelarEstudio = cancelarEstudio;
        scope.agendarSave = agendarSave;
        scope.reagendarEstudioSave = reagendarEstudioSave;
        scope.cancelarCita = cancelarCita;
        scope.cancelarCitaSave = cancelarCitaSave;
        scope.cicloEscolar = {};
        scope.listaCiclos = [];
        scope.listaEmpleado = [];
        scope.listaEstatus = [];
        scope.noValidCiclo = false;
        scope.cancelar = cancelar;
        //paginador 
        scope.siguiente = siguiente;
        scope.anterior = anterior;
        scope.load = false;
        scope.zonas = [];
        scope.listaReporte = [];

        $rootScope.tipoUsuario = scope.tipoUsuario;
        $rootScope.rolUsuario = scope.rolUsuario;
        scope.init = init;
        scope.init();
        


        function init() {
            var parametros = {};
            parametros.tipoUsuario = scope.tipoUsuario;
            parametros.rolUsuario = scope.rolUsuario;
            parametros.idUsuario = scope.user.id_usuario;
            parametros.idInstitucion = scope.institucion.id_institucion;
            parametros.filtroFamilia = scope.filtroFamilia;

            scope.load = true;
            var promesas = [];
            promesas.push(EstudiosService.obtenerCicloEscolarCat());
            promesas.push(EstudiosService.obtenerEmpleados());
            promesas.push(EstudiosService.obtenerEstatusCat());
            promesas.push(EstudiosService.obtenerCicloEscolar());
            promesas.push(ColoniaService.getZonas());
            promesas.push(AdminService.getInstitucionListGrupo(scope.grupo.id_institucion));

            $q.all(promesas).then(
                function (response) {
                    scope.listaCiclos = response[0].data;
                    scope.listaEmpleados = response[1].data;
                    scope.listaEstatus = response[2].data;
                    var parametros = {};
                    parametros.tipoUsuario = scope.tipoUsuario;
                    parametros.rolUsuario = scope.rolUsuario;
                    parametros.idUsuario = scope.user.id_usuario;
                    parametros.idInstitucion = scope.institucion.id_institucion;
                    var l = scope.instituciones;
                    var ifs = [];
                    angular.forEach(l, function (value, key) {
                        ifs.push(value.institucion.id_institucion);
                    });

                    parametros.idInstituciones = ifs;
                    parametros.filtroFamilia = scope.filtroFamilia;
                    parametros.page = scope.currentPage;

                    scope.cicloEscolar = response[3].data[0];

                    scope.zonas = response[4].data;
                    scope.listaInstituciones = response[5].data;
                    parametros.id_ciclo_escolar = scope.listaCiclos[scope.listaCiclos.length - 1].id_ciclo_escolar;
                    parametros.ciclos = [];
                    parametros.ciclos.push(scope.listaCiclos[scope.listaCiclos.length - 1].id_ciclo_escolar);
                    scope.seleccionados.push(scope.listaCiclos[scope.listaCiclos.length - 1].id_ciclo_escolar);
                    scope.noValidCiclo = false;
                    if (scope.tipoUsuario === '2' && scope.cicloEscolar !== undefined) {
                        parametros.id_ciclo_escolar = scope.cicloEscolar.id_ciclo_escolar;
                        parametros.ciclos = [];
                        parametros.ciclos.push(scope.cicloEscolar.id_ciclo_escolar);
                        scope.seleccionados.push(scope.cicloEscolar.id_ciclo_escolar);
                        scope.noValidCiclo = false;
                    } else {
                        if (scope.tipoUsuario === '1' && scope.cicloEscolar !== undefined) {
                            parametros.id_ciclo_escolar = scope.cicloEscolar.id_ciclo_escolar;
                            parametros.ciclos = [];
                            parametros.ciclos.push(scope.cicloEscolar.id_ciclo_escolar);
                            scope.seleccionados.push(scope.cicloEscolar.id_ciclo_escolar);
                            scope.noValidCiclo = false;
                        } else {
                            parametros.ciclos = [];
                            scope.noValidCiclo = true;
                        }

                    }

                    if (scope.cicloEscolar) {
                        scope.noValidCiclo = false;
                    }
                    parametros.pago = [];
                    return EstudiosService.obtenerEstudios(parametros);

                },
                function (error) {
                    console.log('Error getEstudios: ' + error);
                }
            ).then(
                function (response) {
                    scope.load = false;
                    scope.numberPages = response.data.number_of_pages;
                    scope.currentPage = response.data.current_page;
                    scope.listaEstudios = response.data.data;
                    scope.listaEstudiosMostrar = scope.listaEstudios;
                    scope.seleccionados = [];
                    scope.setPagesObject();
                    scope.setSeleccionada();
                    scope.load = false;
                    hide();
                },
                function (error) {
                    scope.load = false;
                    console.log('Error al obtener los estudios');
                    error();
                }
            );

            scope.paginasObject = [];
            scope.setPagesObject = setPagesObject;
        }

        function setPagesObject() {
            scope.paginasObject = [];
            for (var i = 0; i < scope.numberPages; i++) {
                var obj = {
                    pagina: i,
                    seleccionada: false
                };
                scope.paginasObject.push(obj);
            }
        }



        scope.verSeguimiento = verSeguimiento;

        function verSeguimiento(estudio) {
            window.location.href = '#/estudios/detalle/' + estudio.id_estudio + '/seguimiento';
        }

        function siguiente() {
            if (scope.currentPage >= scope.numberOfPages() - 1) return;
            scope.currentPage = scope.currentPage + 1;
            scope.buscarEstudios();
        }

        scope.viewPage = viewPage;
        function viewPage(page) {
            scope.currentPage = page.pagina;
            scope.setSeleccionada();
            scope.buscarEstudios();
        }

        scope.setSeleccionada = setSeleccionada;
        function setSeleccionada() {
            for (var i = 0; i < scope.paginasObject.length; i++) {
                if (scope.paginasObject[i].pagina == scope.currentPage) {
                    scope.paginasObject[i].seleccionada = true;
                } else {
                    scope.paginasObject[i].seleccionada = false;
                }
            }
        }

        function anterior() {
            if (scope.currentPage == 0) return;
            scope.currentPage = scope.currentPage - 1;
            scope.buscarEstudios();
        }

        scope.reenviarURL = reenviarURL;

        function reenviarURL(estudio, ev) {
            var quest = 'Reenviar liga a la familia ' + estudio.familia;
            var confirm = DialogService.dialogConfirm(ev, '¿Reenviar liga de captura?', quest, 'Si', 'No');
            $mdDialog.show(confirm).then(function () {
                var data = { id_estudio: estudio.id_estudio };
                EstudiosService.reenviarLiga(data)
                    .then(
                        function (response) {
                            console.log(response);
                            $mdDialog.show(DialogService.dialogAlert(ev, 'Aviso.', 'Liga de captura reenviada correctamente.'));
                        }, function (error) {
                            console.log('Error al reenviarURL');
                            hide();
                            mensaje('error', 'Aviso.', 'Ocurrio un error, intente mas tarde.');
                            console.log('Error al obtener reenviarURL');
                        }
                    );

            }, function () {
                $mdDialog.hide();
            });
        }

        scope.reactivar = reactivar;

        function reactivar(estudio, ev) {
            var quest = 'Reactivar liga a la familia ' + estudio.familia + '\n Se activara por 3 semanas apartir de hoy.';
            var confirm = DialogService.dialogConfirm(ev, '¿Reactivar liga de captura?', quest, 'Si', 'No');
            $mdDialog.show(confirm).then(function () {
                EstudiosService.sumarDiasByEstudio(estudio.id_estudio)
                    .then(
                        function (response) {
                            console.log(response);
                            $mdDialog.show(DialogService.dialogAlert(ev, 'Aviso.', 'Liga de activada correctamente.'));
                            scope.init();
                        }, function (error) {
                            console.log('Error al reenviarURL');
                            hide();
                            mensaje('error', 'Aviso.', 'Ocurrio un error, intente mas tarde.');
                            console.log('Error al obtener reenviarURL');
                        }
                    );

            }, function () {
                $mdDialog.hide();
            });
        }

        scope.descargarCaratula = descargarCaratula;

        scope.descargarReporteEstatus = descargarReporteEstatus;

        function descargarReporteEstatus() {
            show();
            var parametros = {};
            parametros.tipoUsuario = scope.tipoUsuario;
            parametros.rolUsuario = scope.rolUsuario;
            parametros.idUsuario = scope.user.id_usuario;
            parametros.idInstitucion = scope.institucion.id_institucion;
            parametros.id_ciclo_escolar = scope.cicloEscolar.id_ciclo_escolar;

            var ciclos = [];
            var list = scope.listaCiclos;
            angular.forEach(list, function (value, key) {
                if (list[key].selected === list[key].ciclo_escolar) {
                    ciclos.push(list[key].id_ciclo_escolar);
                }
            });

            if(ciclos.length>0){
                parametros.id_ciclo_escolar = ciclos[0];
            }
            console.log(parametros);

            parametros.filterFamilia = 'all';
            var linkElement = document.createElement('a');

            var list = scope.listaInstituciones;

            var ids = [];
            var idsStr = '';
            var index = 1;
            angular.forEach(list, function (value, key) {
                if (value.selected) {
                    ids.push(value.clave_institucion);
                    idsStr += value.clave_institucion;
                    idsStr += '-';
                }

                index++;
            });

            var l = scope.listaInstituciones;
            var ifs = [];
            var ifsStr = '';
            angular.forEach(l, function (value, key) {
                //ifs.push(value.instituciones.id_institucion);
                if (value.selected) {
                    ifs.push(value.id_institucion);
                    ifsStr += value.id_institucion;
                    ifsStr += '-';
                }
            });
            parametros.idInstituciones = ifs;
            var idPago = [];
            var pagoStr = '';
            var list3 = scope.listaDondePago;
            angular.forEach(list3, function (value, key) {
                if (value.selected) {
                    idPago.push(value.pago);
                    pagoStr += value.pago;
                    pagoStr += '-';
                }
            });
            parametros.pago = idPago;
            var url = Constants.BaseURLBack + '/reporte/solicitudesEstudios/' + parametros.tipoUsuario +
                '/' + parametros.rolUsuario + '/' + parametros.idUsuario + '/' + scope.institucion.id_institucion + '/'
                + parametros.id_ciclo_escolar + '/' + parametros.filterFamilia;
            url += '?ids=' + idsStr;
            url += '&idInstituciones=' + ifsStr;
            url += '&pago=' + pagoStr;
            linkElement.setAttribute('href', url)

            var clickEvent = new MouseEvent("click", {
                "view": window,
                "bubbles": true,
                "cancelable": false
            });
            linkElement.dispatchEvent(clickEvent);
            hide();
        }

        function descargarCaratula(estudio) {
            var linkElement = document.createElement('a');
            var url = Constants.BaseURLBack + '/reporte/caratulaView/' + estudio.id_estudio;
        linkElement.setAttribute('href', url)

        var clickEvent = new MouseEvent("click", {
            "view": window,
            "bubbles": true,
            "cancelable": false
        });
        linkElement.dispatchEvent(clickEvent);

            /*show();
            EstudiosService.descargarArchivo(estudio.archivo_caratula)
                .then(
                    function (response) {
                        var headers = response.headers();
                        var contentType = headers['content-type'];
                        var linkElement = document.createElement('a');
                        var blob = new Blob([response.data], { type: contentType });
                        var url = window.URL.createObjectURL(blob);
                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", estudio.archivo_caratula);
                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                        hide();

                    }, function (error) {
                        console.log('Error al obtener descargarCaratula');
                        hide();
                        mensaje('error', 'Aviso.', 'Ocurrio un error, intente mas tarde.');
                        console.log('Error al obtener los estudios');
                    }
                );*/
        }

        function cambiaFiltro() {
            console.log("iniciaFiltro");
            scope.listaEstudiosMostrar = [];
            //scope.listaEstudiosMostrar=scope.listaEstudios;
            for (var i = 0; i < scope.listaEstudios.length; i++) {
                var banNombre = false;
                var banEstatus = false;
                var banTipo = false;
                var banZona = false;
                var banFuno = false;
                var banFdos = false;
                var banFtres = false;
                var banGrupo = false;

                if (scope.busqueda.id_ciclo_escolar === "") {
                    banNombre = true;
                } else if (scope.listaEstudios[i].id_ciclo_escolar === scope.busqueda.id_ciclo_escolar) {
                    banNombre = true;
                }
                ;

                if (scope.busqueda.id_usuario_asignado === "") {
                    banEstatus = true;
                } else if (scope.listaEstudios[i].id_usuario_asignado === scope.busqueda.id_usuario_asignado) {
                    banEstatus = true;
                }
                ;

                if (scope.busqueda.id_estatus_estudio === "") {
                    banTipo = true;
                } else if (scope.listaEstudios[i].id_estatus_estudio === scope.busqueda.id_estatus_estudio) {
                    banTipo = true;
                }
                ;

                if (scope.busqueda.zona === "") {
                    banZona = true;
                } else if (scope.listaEstudios[i].zona === scope.busqueda.zona) {
                    banZona = true;
                }
                ;

                if (scope.busqueda.id_grupo === "") {
                    banGrupo = true;
                } else if (scope.listaEstudios[i].id_grupo === scope.busqueda.id_grupo) {
                    banGrupo = true;
                }
                ;

                if (scope.busqueda.filtro_uno === false) {
                    banFuno = true;
                } else {
                    var aux = false;
                    if (scope.listaEstudios[i].filtro_uno === '1') {
                        aux = true;
                    } else {
                        aux = false;
                    }
                    if (aux === scope.busqueda.filtro_uno) {
                        banFuno = true;
                    }
                }

                if (scope.busqueda.filtro_dos === false) {
                    banFdos = true;
                } else {
                    var aux = false;
                    if (scope.listaEstudios[i].filtro_dos === '1') {
                        aux = true;
                    } else {
                        aux = false;
                    }
                    if (aux === scope.busqueda.filtro_dos) {
                        banFdos = true;
                    }
                }

                if (scope.busqueda.filtro_tres === false) {
                    banFtres = true;
                } else {
                    var aux = false;
                    if (scope.listaEstudios[i].filtro_tres === '1') {
                        aux = true;
                    } else {
                        aux = false;
                    }
                    if (aux === scope.busqueda.filtro_tres) {
                        banFtres = true;
                    }
                }


                if (banTipo && banEstatus && banNombre && banZona && banZona && banFuno && banFdos && banFtres) {
                    scope.listaEstudiosMostrar.push(scope.listaEstudios[i]);
                }
                ;
            }
            ;

            scope.currentPage = 0;
        }
        ;

        scope.filtroBus = false;
        function buscarEstudios() {
            if (scope.filtroFamilia !== '' && !scope.filtroBus) {
                scope.filtroBus = true;
                scope.currentPage = 0;
            } else {
                scope.filtroBus = false;
            }
            var ciclos = [];
            var list = scope.listaCiclos;
            angular.forEach(list, function (value, key) {
                if (list[key].selected === list[key].ciclo_escolar) {
                    ciclos.push(list[key].id_ciclo_escolar);
                }
            });

            var ids = [];
            var list2 = scope.listaInstituciones;
            angular.forEach(list2, function (value, key) {
                if (value.selected) {
                    ids.push(value.clave_institucion);
                }
            });

            scope.listaEstudiosMostrar = [];
            scope.bandera_busco = false;
            scope.load = true;
            var parametros = {};
            parametros.tipoUsuario = scope.tipoUsuario;
            parametros.rolUsuario = scope.rolUsuario;
            parametros.idUsuario = scope.user.id_usuario;
            parametros.idInstitucion = scope.institucion.id_institucion;
            parametros.filtroFamilia = scope.filtroFamilia;
            parametros.page = scope.currentPage;
            parametros.fInstitucionesHijos = ids;
            //parametros.idInstitucion = '0';
            //var l = scope.instituciones;
            var l = scope.listaInstituciones;
            var ifs = [];
            angular.forEach(l, function (value, key) {
                //ifs.push(value.instituciones.id_institucion);
                if (value.selected) {
                    ifs.push(value.id_institucion);
                }
            });
            parametros.idInstituciones = ifs;
            var idPago = [];
            var list3 = scope.listaDondePago;
            angular.forEach(list3, function (value, key) {
                if (value.selected) {
                    idPago.push(value.pago);
                }
            });
            parametros.pago = idPago;
            var push = [];
            if (ciclos.length === 0) {
                if (scope.tipoUsuario === '2' && scope.noValidCiclo) {
                    push.push(scope.listaCiclos[scope.listaCiclos.length - 1].id_ciclo_escolar);
                } else {
                    if (scope.cicloEscolar === undefined || scope.cicloEscolar.ciclo_escolar === undefined) {
                        push.push(scope.listaCiclos[scope.listaCiclos.length - 1].id_ciclo_escolar);
                    } else {
                        push.push(scope.cicloEscolar.id_ciclo_escolar);
                    }
                }

            } else {
                push = ciclos;
            }
            parametros.ciclos = push;
            if (scope.busqueda.id_estatus_estudio) {
                parametros.id_estatus_estudio = scope.busqueda.id_estatus_estudio;
            }
            if (scope.busqueda.zona) {
                parametros.zona = scope.busqueda.zona;
            }
            if (scope.busqueda.id_grupo) {
                parametros.id_grupo = scope.busqueda.id_grupo;
            }
            if (scope.busqueda.id_usuario_asignado) {
                parametros.id_usuario_asignado = scope.busqueda.id_usuario_asignado;
            }
            EstudiosService.obtenerEstudios(parametros)
                .then(
                    function (response) {
                        scope.load = false;
                        scope.numberPages = response.data.number_of_pages;
                        scope.currentPage = response.data.current_page;
                        scope.listaEstudios = response.data.data;
                        scope.listaEstudiosMostrar = scope.listaEstudios;
                        scope.setPagesObject();
                        scope.setSeleccionada();
                    },
                    function (error) {
                        scope.load = false;
                        console.log('Error al obtener los estudios');
                        error();
                    }
                );

        }
        ;

        function verDetalle(estudio) {
            show();
            EstudiosService.idEstudioSeleccionado = estudio.id_estudio;
            scope.id_grupo = estudio.grupo.id_grupo;
            if (scope.id_grupo == Constants.GRUPO_INSIGNA) {
              window.location.href = "#/estudios/detalle-insigna/" +EstudiosService.idEstudioSeleccionado;
              return;
            } else {
              window.location.href = "#/estudios/detalle/" + EstudiosService.idEstudioSeleccionado;
              return;
            }
            
        }
        ;


        function numberOfPages() {
            return scope.numberPages;
            //return Math.ceil(scope.listaEstudiosMostrar.length / scope.pageSize);
        }
        ;

        function agendarView(estudio) {
            show();
            scope.entrevista = {};
            scope.estudio = {};
            scope.estudio = estudio;
            EstudiosService.obtenerDetalleEstudio(scope.estudio.id_estudio, 0).then(
                function (response) {
                    scope.familia = response.data.familia;
                    scope.familia.clave_institucion = response.data.clave_institucion;
                    hide();
                    $("#modal_agendar").modal('show');
                },
                function (error) {
                    console.log('Error al obtener el detalle: ' + error);
                }
            );

        }

        function agendarEstudio() {
            scope.entrevista = {};
            $("#modal_agendar_entrevista").modal('show');
        }


        function cancelar() {
            scope.entrevista = {};
            $("#modal_agendar_entrevista").modal('hide');
        }
        function agendarSave() {
            if (scope.entrevista.fecha_entrevista === undefined || scope.entrevista.fecha_entrevista === "") {
                mensaje("alert", 'Error de validación', 'La fecha de entrevista es requerida.');
                return;
            }
            if (scope.entrevista.hora_entrevista === undefined || scope.entrevista.hora_entrevista === "") {
                mensaje("alert", 'Error de validación', 'La hora de entrevista es requerida.');
                return;
            }
            var obj = {};
            var obj2 = {};
            obj.id_estudio = scope.estudio.id_estudio;
            obj.id_estatus_estudio = 3;
            obj.fecha_entrevista = scope.entrevista.fecha_entrevista;
            obj.hora_entrevista = scope.entrevista.hora_entrevista;

            var fecha = new Date(obj.hora_entrevista);
            var time = fecha.getHours() + ':' + fecha.getMinutes() + ':00';

            obj.hora_entrevista = time;
            obj2.comentario = scope.entrevista.comentarios;
            obj2.tipo = 'ACEPTO_ENTREVISTA';
            obj2.id_estudio = scope.estudio.id_estudio;
            obj2.folio_estudio = scope.estudio.folio_estudio;
            obj2.id_familia = scope.estudio.id_familia;

            confirmaMsj(
                "Confirmación de solicitud",
                "¿Agendar estudio?",
                "Si",
                function () {
                    var promesas = [];
                    promesas.push(EstudiosService.actualizarEstudio(obj));
                    if (scope.entrevista.comentarios !== undefined && scope.entrevista.comentarios !== "") {
                        promesas.push(ComentarioService.guardarComentario(obj2));
                    }
                    $q.all(promesas).then(
                        function (data) {
                            console.log(data);
                            scope.familia = {};
                            scope.estudio = {};
                            mensaje('success', 'Aviso.', 'Se agendo el estudio correctamente.');
                            scope.buscarEstudios();
                            $("#modal_agendar_entrevista").modal('hide');
                            $("#modal_agendar").modal('hide');
                        },
                        function () {
                            console.log('error prom');
                        }
                    );
                },
                "No",
                function () { }
            );
        }

        function reagendarEstudio() {
            scope.entrevista = {};
            $("#modal_reagendar_entrevista").modal('show');
        }

        function reagendarEstudioSave() {
            if (scope.entrevista.fecha_reagendar_entrevista === undefined || scope.entrevista.fecha_reagendar_entrevista === "") {
                mensaje("alert", 'Error de validación', 'La fecha de reagendar entrevista es requerida.');
                return;
            }
            if (scope.entrevista.comentarios === undefined || scope.entrevista.comentarios === "") {
                mensaje("alert", 'Error de validación', 'El comentario es requerido para especificar el motivo por el cual se reagenda la entrevista.');
                return;
            }
            var obj = {};
            obj.id_estudio = scope.estudio.id_estudio;
            obj.id_estatus_estudio = 4;
            obj.fecha_reagendar_entrevista = scope.entrevista.fecha_reagendar_entrevista;
            var obj2 = {};
            obj2.comentario = scope.entrevista.comentarios;
            obj2.tipo = 'REAGENDO_ENTREVISTA';
            obj2.id_estudio = scope.estudio.id_estudio;
            obj2.folio_estudio = scope.estudio.folio_estudio;
            obj2.id_familia = scope.estudio.id_familia;

            confirmaMsj(
                "Confirmación de solicitud",
                "¿Reagendar estudio?",
                "Si",
                function () {
                    var promesas = [];
                    promesas.push(EstudiosService.actualizarEstudio(obj));
                    if (scope.entrevista.comentarios !== undefined && scope.entrevista.comentarios !== "") {
                        promesas.push(ComentarioService.guardarComentario(obj2));
                    }
                    $q.all(promesas).then(
                        function (data) {
                            console.log(data);
                            scope.familia = {};
                            scope.estudio = {};
                            mensaje('success', 'Aviso.', 'Se reagendo el estudio correctamente.');
                            scope.buscarEstudios();
                            $("#modal_agendar").modal('hide');
                            $("#modal_reagendar_entrevista").modal('hide');
                        },
                        function () {
                            console.log('error prom');
                        }
                    );
                },
                "No",
                function () { }
            );
        }

        function cancelarEstudio(estudio) {
            confirmaMsj("Confirmación de solicitud",
                "¿Está seguro que desea cancelar el estudio?",
                "Si",
                function () {
                    EstudiosService.cancelarEstudioInstitucion(estudio.id_estudio_institucion).then(
                        function (response) {
                            mensaje('success', 'Cancelar estudio', 'Se cancelo el estudio correctamente.');
                            scope.buscarEstudios();
                            window.location = '#/estudios/ver';
                        },
                        function (error) {
                            console.log('Error al cancelar el estudio: ' + error);
                        }
                    );
                },
                "No",
                function () { }
            );
        }


        function cancelarCita(estudio) {
            scope.estudio = estudio;
            scope.entrevista = {};
            $("#modal_cancelar_entrevista").modal('show');
        }

        function cancelarCitaSave() {
            if (scope.entrevista.fecha_reagendar_entrevista === undefined || scope.entrevista.fecha_reagendar_entrevista === "") {
                mensaje("alert", 'Error de validación', 'La fecha de reagendar entrevista es requerida.');
                return;
            }
            if (scope.entrevista.comentarios === undefined || scope.entrevista.comentarios === "") {
                mensaje("alert", 'Error de validación', 'El comentario es requerido para especificar el motivo de la cancelación de la entrevista.');
                return;
            }
            var obj = {};
            obj.id_estudio = scope.estudio.id_estudio;
            obj.id_estatus_estudio = 9;
            obj.fecha_reagendar_entrevista = scope.entrevista.fecha_reagendar_entrevista;
            var obj2 = {};
            obj2.comentario = scope.entrevista.comentarios;
            obj2.tipo = 'CANCELO_ENTREVISTA';
            obj2.id_estudio = scope.estudio.id_estudio;
            obj2.folio_estudio = scope.estudio.folio_estudio;
            obj2.id_familia = scope.estudio.id_familia;
            console.log(obj);
            console.log(obj2);

            confirmaMsj(
                "Confirmación de solicitud",
                "¿Cancelar entrevista del estudio?",
                "Si",
                function () {
                    var promesas = [];
                    promesas.push(EstudiosService.actualizarEstudio(obj));
                    if (scope.entrevista.comentarios !== undefined && scope.entrevista.comentarios !== "") {
                        promesas.push(ComentarioService.guardarComentario(obj2));
                    }
                    $q.all(promesas).then(
                        function (data) {
                            console.log(data);
                            scope.familia = {};
                            scope.estudio = {};
                            mensaje('success', 'Aviso.', 'Se cancelo entrevista del estudio correctamente.');
                            scope.buscarEstudios();
                            $("#modal_agendar").modal('hide');
                            $("#modal_cancelar_entrevista").modal('hide');
                        },
                        function () {
                            console.log('error prom');
                        }
                    );
                },
                "No",
                function () { }
            );
        }




    }
    ;//end controller

})();