(function () {
    'use strict';

    angular
        .module('app.estudios')
        .controller('EstudiosCrearController', EstudiosCrearController);

    function EstudiosCrearController(FamiliaService, AdminService, EstudiosService, $localStorage, $rootScope, $state, $mdDialog, $mdToast, DialogService, RestService, AuthenticationService, Constants, UserService) {
        /* jshint validthis: true */
        console.log('init estudios crear');
        //Check session
        if (!AuthenticationService.isAuth()) {
            window.location.href = '#/login';
            return;
        };
        var scope = this;
        scope.grupo = {};
        scope.GRUPO_INSIGNA = Constants.GRUPO_INSIGNA;
        scope.GRUPO_ITER = Constants.GRUPO_ITER;

        scope.user = {};
        scope.tipoUsuario = 0;
        scope.rolUsuario = 0;
        scope.institucion = {};
        scope.instituciones = [];
        scope.user = $localStorage.data.user;
        scope.tipoUsuario = $localStorage.globals.type;
        scope.rolUsuario = $localStorage.globals.role;
        if (scope.tipoUsuario == '2') {
            scope.institucion = $localStorage.data.institucion;
            scope.instituciones = $localStorage.data.instituciones;
        }
        if (![null, undefined].includes(scope.institucion.grupo)) {
            scope.grupo = scope.institucion.grupo;
        }
        scope.estudios = [];
        scope.familia = {};
        scope.estudio = {};
        scope.cicloEscolar = {};
        scope.estudioInstitucion = {};
        scope.filtroFamilia = "";
        scope.listaFamiliasEncontradas = [];
        scope.opcion = {};
        scope.currentPage = 0;
        scope.pageSize = 7;
        scope.numberPages = 0;
        scope.numberOfPages = numberOfPages;
        scope.id_institucion = scope.institucion.id;
        console.log(scope.id_institucion);
        scope.tomoFamilia = false;

        //Banderas
        scope.bandera_menu = true;
        scope.bandera = false;
        scope.bandera_busco = true;
        scope.bandera_selecciono = false;
        scope.guardo = false;
        scope.load = false;

        //acciones
        scope.accionInicial = accionInicial;
        scope.regresar = regresar;
        scope.guardar_solicitud = guardar_solicitud;
        scope.buscar_familias = buscar_familias;
        scope.seleccionarFamilia = seleccionarFamilia;
        scope.continuarFamilia = continuarFamilia;
        scope.guardarEstudio = guardarEstudio;
        scope.solicitarEstudio = solicitarEstudio;
        scope.cancelarEstudio = cancelarEstudio;

        //paginador 
        scope.siguiente = siguiente;
        scope.anterior = anterior;

        scope.listaEstatus = [];
        scope.listaInstuciones = [];

        AdminService.getInstitucionListGrupo(scope.grupo.id_institucion).then(
            function (response) {
                scope.listaInstuciones = response.data;
            },
            function (err) {
                console.log('Error al obtener listaInstuciones' + err);
            }
        );

        EstudiosService.obtenerEstatusCat().then(
            function (response) {
                scope.listaEstatus = response.data;
            }, function (error) {
                console.log('Error al obtener el obtenerEstatusCat: ' + error);
            }
        );

        EstudiosService.obtenerCicloEscolar().then(
            function (response) {
                if (response.data.length > 0) {
                    scope.cicloEscolar = response.data[0];
                }
            }, function (error) {
                console.log('Error al obtener el ciclo: ' + error);
            }
        );

        function accionInicial(accion) {
            scope.bandera_menu = false;
            if (accion === 1) {
                scope.buscar_familias();
            }
        }
        ;

        function regresar() {
            scope.bandera_menu = true;
            scope.estudio = {};
            scope.familia = {};
            scope.opcion = {};
        }
        ;

        function siguiente() {
            scope.currentPage = scope.currentPage + 1;
            scope.buscar_familias();
        }

        function anterior() {
            scope.currentPage = scope.currentPage - 1;
            scope.buscar_familias();
        }

        function buscar_familias() {
            scope.listaFamiliasEncontradas = [];
            scope.bandera_busco = false;
            scope.load = true;
            var obj = {};

            obj.id_institucion = scope.user.id_institucion;

            var l = scope.instituciones;
            var ifs = [];
            angular.forEach(l, function (value, key) {
                ifs.push(value.institucion.id_institucion);
            });

            obj.idInstituciones = ifs;

            obj.filtro_familia = scope.filtroFamilia;
            obj.page = scope.currentPage;
            EstudiosService.obtenerFamilias(obj)
                .then(
                    function (response) {
                        scope.numberPages = response.data.number_of_pages;
                        scope.currentPage = response.data.current_page;
                        scope.listaFamiliasEncontradas = response.data.data;
                        var temp = [];
                        for (var i = 0; i < scope.listaFamiliasEncontradas.length; i++) {
                            var estudio = scope.listaFamiliasEncontradas[i].estudio;
                            var es = false;
                            var xx = 0;
                            for (var j = 0; j < estudio.length; j++) {
                                console.log(estudio[j].id_institucion);
                                console.log(scope.id_institucion);
                                if (estudio[j].id_institucion === scope.id_institucion) {
                                    es = true;
                                    estudio[j].tieneEstudio = true;
                                    xx = j;
                                    break;
                                } else {
                                    estudio[j].tieneEstudio = false;
                                    xx = j;
                                }
                            }
                            var fam = scope.listaFamiliasEncontradas[i];
                            fam.estudio = estudio[xx];
                            temp.push(fam);
                        }
                        scope.listaFamiliasEncontradas = temp;
                        //scope.filtroFamilia = "";
                        scope.load = false;
                    },
                    function (error) {
                        scope.load = false;
                        console.log('Error al obtener familias filtradas');
                    }
                );

        }
        ;

        function seleccionarFamilia(familia) {
            scope.bandera_selecciono = true;
            scope.familia = familia;
        }
        ;

        function continuarFamilia() {
            if (scope.familia.id_familia === undefined) {
                mensaje('error', 'Solicitud estudio', 'Seleccione una familia para continuar.');
                return;
            }
            scope.opcion = '2';
        }
        ;

        function solicitarEstudio(familia, estudio) {
            scope.estudio = estudio;
            scope.familia = familia;
            scope.opcion = '2';
            scope.estudio.num_recibo = '';
        }

        function guardar_solicitud() {
            if (scope.cicloEscolar.id_ciclo_escolar === undefined) {
                mensaje('alert', 'Error ciclo escolar', 'No existe ciclo escolar activo, consulte al administrador.', 5000);
                return;
            }

            if (scope.familia.familia === '' ||
                scope.familia.familia === null ||
                scope.familia.familia === undefined
                || scope.estudio.pago === undefined
                || scope.estudio.num_recibo === undefined
                || scope.estudio.num_recibo === ''
            ) {
                mensaje('error', 'Error de validación', 'Completa todos los compos');
                return;
            }

            confirmaMsj("Confirmación de solicitud",
                "¿Está seguro que se ingresaron todos los datos correctamente?",
                "Si",
                function () {
                    guardarSolicitudAccion();
                },
                "No",
                function () { }
            );

        }
        ;

        scope.filtros = {};
        scope.filtro_uno = false;
        scope.filtro_dos = false;
        scope.filtro_tres = false;

        scope.obtenerFolio = obtenerFolio;
        function obtenerFolio() {
            var data = {};
            data.id_ciclo_escolar = scope.cicloEscolar.id_ciclo_escolar;
            data.pago = scope.estudio.pago;
            data.id_institucion = scope.institucion.id_institucion;
            scope.id_institucion = scope.estudio.pago;
            EstudiosService.obtenerFolio(data).then(
                function (response) {
                    scope.estudio.num_recibo = response.data;
                },
                function (error) {
                    console.log(error);
                }
            );
        }

        function guardarSolicitudAccion() {
            //Insertar familia nueva
            window.scrollTo(0, 0);
            show();
            if (scope.familia.id_familia === undefined) {
                scope.familia.id_grupo = scope.grupo.id_institucion;
                EstudiosService.guardarFamilia(scope.familia).then(
                    function (response) {
                        if (response.data.id_familia === undefined) {
                            mensaje('error', 'Solicitud de estudio', 'Ocurrio un error al guardar la información. Intente mas tarde.');
                            return;
                        } else {
                            //Guardar el estudio
                            scope.estudio.id_estatus_estudio = 1;//Enviado para solicitud
                            //scope.estudio.id_institucion_solicito = scope.familia.id_institucion;
                            scope.estudio.id_institucion_solicito = scope.user.id_institucion;
                            //scope.estudio.id_institucion_familia = scope.institucion.id_institucion;
                            scope.estudio.id_institucion_familia = scope.familia.id_institucion;
                            scope.estudio.id_familia = response.data.id_familia;
                            //scope.estudio.clave_institucion = scope.institucion.clave_institucion;
                            scope.estudio.clave_institucion = scope.familia.clave_institucion;
                            scope.estudio.id_usuario_asignado = 0;
                            scope.estudio.id_usuario_asigno = 0;
                            scope.estudio.id_ciclo_escolar = scope.cicloEscolar.id_ciclo_escolar;
                            EstudiosService.guardarEstudio(scope.estudio).then(
                                function (response) {
                                    if (response.data.id_estudio === undefined) {
                                        mensaje('error', 'Solicitud de estudio', 'Ocurrio un error al guardar la información. Intente mas tarde.');
                                        return;
                                    } else {
                                        scope.estudioInstitucion.id_estudio = response.data.id_estudio;
                                        scope.estudioInstitucion.pago = scope.estudio.pago;
                                        scope.estudioInstitucion.num_recibo = scope.estudio.num_recibo;
                                        //scope.estudioInstitucion.id_institucion = scope.user.id_institucion;
                                        if (scope.grupo.id_grupo == scope.GRUPO_INSIGNA) {
                                            scope.estudioInstitucion.id_institucion = parseInt(scope.user.id_institucion);
                                        } else {
                                            scope.estudioInstitucion.id_institucion = parseInt(scope.grupo.id_grupo);
                                        }
                                        //scope.estudioInstitucion.id_institucion = scope.familia.id_institucion;
                                        if (scope.filtros.filtro_uno) {
                                            scope.estudioInstitucion.filtro_uno = 1;
                                        } else {
                                            scope.estudioInstitucion.filtro_uno = 0;
                                        }

                                        if (scope.filtros.filtro_dos) {
                                            scope.estudioInstitucion.filtro_dos = 1;
                                        } else {
                                            scope.estudioInstitucion.filtro_dos = 0;
                                        }

                                        if (scope.filtros.filtro_tres) {
                                            scope.estudioInstitucion.filtro_tres = 1;
                                        } else {
                                            scope.estudioInstitucion.filtro_tres = 0;
                                        }
                                        EstudiosService.guardarEstudioInstitucion(scope.estudioInstitucion);
                                        setTimeout(function () {
                                            hide();
                                            mensaje('success', 'Solicitud de estudio', 'Estudio enviado correctamente');
                                            location.href = "#/estudios/ver";
                                            window.scrollTo(0, 0);
                                        }, 3000);
                                    }
                                },
                                function (error) {
                                    console.log('Error al guardar el estudio: ' + error);
                                }
                            );
                        }
                    },
                    function (error) {
                        hide();
                        mensaje('error', 'Solicitud de estudio', 'Ocurrio un error al guardar la información. Intente mas tarde.');
                        console.log('Error al guardar familia: ' + error);
                    }
                );

            } else {
                if (scope.familia.id_familia !== undefined && scope.estudio.id_estudio === undefined) {
                    scope.estudio.id_ciclo_escolar = scope.cicloEscolar.id_ciclo_escolar;
                    scope.estudio.id_estatus_estudio = 1;//Enviado para solicitud
                    scope.estudio.id_familia = scope.familia.id_familia;
                    //scope.estudio.clave_institucion = scope.institucion.clave_institucion;
                    scope.estudio.clave_institucion = scope.familia.clave_institucion;
                    scope.estudio.id_institucion_familia = scope.familia.id_institucion;
                    scope.familia.id_grupo = scope.grupo.id_institucion;
                    scope.estudio.id_institucion_solicito = scope.institucion.id_institucion;
                    //scope.estudio.id_institucion_solicito = scope.familia.id_institucion;
                    scope.estudio.id_usuario_asignado = 0;
                    scope.estudio.id_usuario_asigno = 0;
                    delete scope.familia.clave_institucion;
                    delete scope.familia.nombre_institucion;
                    delete scope.familia.estudio;
                    delete scope.familia.letra_identificacion;
                    FamiliaService.actualizarFamilia(scope.familia).then(
                        function (response) {
                            EstudiosService.guardarEstudio(scope.estudio).then(
                                function (response) {
                                    if (response.data.id_estudio === undefined) {
                                        mensaje('error', 'Solicitud de estudio', 'Ocurrio un error al guardar la información. Intente mas tarde.');
                                        return;
                                    } else {
                                        scope.estudioInstitucion.id_estudio = response.data.id_estudio;
                                        if (scope.grupo.id_grupo == scope.GRUPO_INSIGNA) {
                                            scope.estudioInstitucion.id_institucion = parseInt(scope.user.id_institucion);
                                        } else {
                                            scope.estudioInstitucion.id_institucion = parseInt(scope.grupo.id_grupo);
                                        }
                                        //scope.estudioInstitucion.id_institucion = scope.familia.id_institucion;
                                        scope.estudioInstitucion.pago = scope.estudio.pago;
                                        //scope.estudioInstitucion.num_recibo = scope.estudio.num_recibo;
                                        scope.estudioInstitucion.num_recibo = response.data.folio_estudio_pago;

                                        if (scope.filtros.filtro_uno) {
                                            scope.estudioInstitucion.filtro_uno = 1;
                                        } else {
                                            scope.estudioInstitucion.filtro_uno = 0;
                                        }

                                        if (scope.filtros.filtro_dos) {
                                            scope.estudioInstitucion.filtro_dos = 1;
                                        } else {
                                            scope.estudioInstitucion.filtro_dos = 0;
                                        }

                                        if (scope.filtros.filtro_tres) {
                                            scope.estudioInstitucion.filtro_tres = 1;
                                        } else {
                                            scope.estudioInstitucion.filtro_tres = 0;
                                        }
                                        EstudiosService.guardarEstudioInstitucion(scope.estudioInstitucion);
                                        setTimeout(function () {
                                            hide();
                                            mensaje('success', 'Solicitud de estudio', 'Estudio enviado correctamente');
                                            location.href = "#/estudios/ver";
                                            window.scrollTo(0, 0);
                                        }, 3000);
                                    }
                                },
                                function (error) {
                                    hide();
                                    mensaje('error', 'Solicitud de estudio', 'Ocurrio un error al guardar la información. Intente mas tarde.');
                                    console.log('Error al guardar el estudio: ' + error);
                                }
                            );
                        },
                        function (error) {
                            console.log('Error al actualizar familia: ' + error);
                        }
                    );
                } else {
                    if (scope.estudio.id_estudio !== undefined) {

                        scope.estudioInstitucion.id_estudio = scope.estudio.id_estudio;
                        //scope.estudioInstitucion.id_institucion = parseInt(scope.user.id_institucion);
                        if (scope.grupo.id_grupo == scope.GRUPO_INSIGNA) {
                            scope.estudioInstitucion.id_institucion = parseInt(scope.user.id_institucion);
                        } else {
                            scope.estudioInstitucion.id_institucion = parseInt(scope.grupo.id_grupo);
                        }
                        scope.estudioInstitucion.pago = scope.estudio.pago;
                        scope.estudioInstitucion.num_recibo = scope.estudio.num_recibo;

                        if (scope.filtros.filtro_uno) {
                            scope.estudioInstitucion.filtro_uno = 1;
                        } else {
                            scope.estudioInstitucion.filtro_uno = 0;
                        }

                        if (scope.filtros.filtro_dos) {
                            scope.estudioInstitucion.filtro_dos = 1;
                        } else {
                            scope.estudioInstitucion.filtro_dos = 0;
                        }

                        if (scope.filtros.filtro_tres) {
                            scope.estudioInstitucion.filtro_tres = 1;
                        } else {
                            scope.estudioInstitucion.filtro_tres = 0;
                        }

                        delete scope.familia.clave_institucion;
                        delete scope.familia.nombre_institucion;
                        delete scope.familia.estudio;
                        FamiliaService.actualizarFamilia(scope.familia).then(
                            function (response) {
                                EstudiosService.guardarEstudioInstitucion(scope.estudioInstitucion).then(
                                    function (response) {
                                        setTimeout(function () {
                                            hide();
                                            mensaje('success', 'Solicitud de estudio', 'Estudio enviado correctamente');
                                            location.href = "#/estudios/ver";
                                            window.scrollTo(0, 0);
                                        }, 3000);
                                    },
                                    function (error) {
                                        hide();
                                        mensaje('error', 'Solicitud de estudio', 'Ocurrio un error al guardar la información. Intente mas tarde.');
                                        console.log('Error al guardar el estudio institucion: ' + error);
                                    }
                                );
                            },
                            function (error) {
                                console.log('Error al actualizar familia: ' + error);
                            }
                        );
                    }
                }
            }


        }
        ;

        function guardarEstudio(data) {
            EstudiosService.guardarEstudio(data).then(
                function (response) {
                    return true;
                },
                function (error) {
                    console.log('Error al guardar el estudio: ' + error);
                    return false;
                }
            );
        }

        function numberOfPages() {
            return scope.numberPages;
            //return Math.ceil(scope.listaFamiliasEncontradas.length / scope.pageSize);
        }
        ;

        function cancelarEstudio(estudio) {
            confirmaMsj("Confirmación de solicitud",
                "¿Está seguro que desea cancelar el estudio?",
                "Si",
                function () {
                    EstudiosService.cancelarEstudioInstitucion(estudio.id_estudio_institucion).then(
                        function (response) {
                            mensaje('success', 'Cancelar estudio', 'Se cancelo el estudio correctamente.');
                            window.location = '#/estudios/ver';
                        },
                        function (error) {
                            console.log('Error al cancelar el estudio: ' + error);
                        }
                    );
                },
                "No",
                function () { }
            );
        }

    }
    ;//end controller

})();