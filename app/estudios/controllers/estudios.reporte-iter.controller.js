(function () {
    'use strict';

    angular
        .module('app.estudios')
        .controller('EstudiosReporteIterController', EstudiosReporteIterController);

    function EstudiosReporteIterController(
        VehiculoService,
        FamiliaService,
        EstudiosService,
        HijoService,
        PropiedadService,
        DependienteService,
        MotivoService,
        ComentarioService,
        AdminService,
        $localStorage,
        $rootScope,
        $state, GradoService,
        $mdDialog,
        $mdToast,
        DialogService, RestService, AuthenticationService, Constants, UserService, $q) {
        /* jshint validthis: true */
        console.log('init estudios REPORTE detalle');

        show();
        //Check session
        if (!AuthenticationService.isAuth()) {
            window.location.href = '#/login';
            return;
        };
        var scope = this;
        scope.user = {};
        scope.tipoUsuario = 0;
        scope.rolUsuario = 0;
        scope.institucion = {};
        scope.user = $localStorage.data.user;
        scope.tipoUsuario = $localStorage.globals.type;
        scope.rolUsuario = $localStorage.globals.role;
        if (scope.tipoUsuario == '2') {
            scope.institucion = $localStorage.data.institucion;
        }

        if (EstudiosService.idEstudioSeleccionado === 0) {
            EstudiosService.idEstudioSeleccionado = 10;
        }

        if ($state.params.id === '' || $state.params.id === undefined) {

        }


        EstudiosService.idEstudioSeleccionado = $state.params.id;
        scope.indexFolio = $state.params.index;

        var id = 0;
        if ($rootScope.tipoUsuario === '2') {
            id = $rootScope.institucion.id_institucion;
        }

        scope.grupo = {};
        scope.GRUPO_INSIGNA = Constants.GRUPO_INSIGNA;
        scope.GRUPO_ITER = Constants.GRUPO_ITER;
        scope.id_grupo = '';
        if (![null, undefined].includes(scope.institucion.grupo)) {
            scope.grupo = scope.institucion.grupo;
        }

        scope.listaInstuciones = [];


        scope.suma_col = 0.00;
        scope.suma_apoyo_soli = 0;
        scope.suma_apoyo_suge = 0;

        scope.estudio = {};
        scope.hijo = {};
        scope.dependiente = {};
        scope.motivo = {};
        scope.vehiculo = {};
        scope.propiedad = {};
        scope.comentario = {};
        scope.listaEmpleado = [];
        scope.empleadoAsignado = {};
        scope.ingresos = {};
        scope.egresos = {};
        scope.documentos = {};
        scope.evaluacion = {};
        //Banderas
        scope.load = false;
        scope.verAccion = false;
        scope.banderaActualizar = false;
        scope.total_2 = 0;
        scope.diferencia_2 = 0;
        scope.necesidad = 0;
        scope.confiabilidad = 0;

        scope.porcentaje = 0;
        scope.factor1 = 0;
        scope.factor2 = 0;
        scope.factor3 = 0;

        scope.factor1_1 = 0;
        scope.factor2_2 = 0;
        scope.factor3_3 = 0;

        scope.suma_docs = 0;

        scope.resta_porcentaje = 0;

        scope.total_ingresos2 = 0;
        hide();

        scope.calcula = calcula;
        scope.setSueldo = setSueldo;


        scope.calculaEgresos = calculaEgresos;
        scope.calculaEgresosServicios = calculaEgresosServicios;
        scope.calculaEgresosTotal = calculaEgresosTotal;
        scope.folio = scope.indexFolio;

        scope.estudio.total_depen = 0;
        scope.ingresos.ingreso_otros_miembros = 0;
        scope.ingresos.ingreso_renta = 0;
        scope.ingresos.ingreso_honorarios = 0;
        scope.ingresos.ingreso_inversiones = 0;
        scope.ingresos.ingreso_pensiones = 0;
        scope.ingresos.ingreso_ventas = 0;
        scope.ingresos.otros_ingresos = 0;
        scope.ingresos.total_otros_ingresos = 0;
        scope.ingresos.sueldo_papa = 0;
        scope.ingresos.sueldo_mama = 0;
        scope.ingresos.ingreso_percapita = 0;
        scope.ingresos.clasificacion = '';
        scope.ingresos.num_personas_aportan = 0;
        scope.ingresos.negocios_papa = 0;
        scope.ingresos.negocios_mama = 0;
        scope.ingresos.apoyo_familiar = 0;
        scope.ingresos.aguinaldo_papa = 0;
        scope.ingresos.aguinaldo_mama = 0;
        scope.ingresos.utilidades_papa = 0;
        scope.ingresos.utilidades_mama = 0;
        scope.ingresos.vales_papa = 0;
        scope.ingresos.vales_mama = 0;
        scope.ingresos.bono_mes_papa = 0;
        scope.ingresos.bono_mes_mama = 0;
        scope.ingresos.bono_anual_papa = 0;
        scope.ingresos.bono_anual_mama = 0;
        scope.ingresos.vacac_papa = 0;
        scope.ingresos.vacac_mama = 0;
        scope.ingresos.comisiones_papa = 0;
        scope.ingresos.comisiones_mama = 0;
        scope.ingresos.gratificacion_papa = 0;
        scope.ingresos.gratificacion_mama = 0;
        scope.ingresos.fda_papa = 0;
        scope.ingresos.fda_mama = 0;
        scope.ingresos.docencia_papa = 0;
        scope.ingresos.docencia_mama = 0;
        scope.ingresos.dev_impuestos = 0;
        scope.ingresos.total_papa = 0;
        scope.ingresos.total_mama = 0;

        scope.egresos.alimentacion_despensa = 0;
        scope.egresos.renta = 0;
        scope.egresos.credito_hipotecario = 0;
        scope.egresos.colegiaturas = 0;
        scope.egresos.otras_colegiaturas = 0;
        scope.egresos.clases_particulares = 0;
        scope.egresos.agua = 0;
        scope.egresos.luz = 0;
        scope.egresos.telefono = 0;
        scope.egresos.servicio_domestico = 0;
        scope.egresos.gas = 0;
        scope.egresos.total_servicios = 0;
        scope.egresos.gasolina = 0;
        scope.egresos.credito_auto = 0;
        scope.egresos.pago_tdc_mensual = 0;
        scope.egresos.saldo_tdc = 0;
        scope.egresos.creditos_comerciales = 0;
        scope.egresos.vestido_calzado = 0;
        scope.egresos.medico_medicinas = 0;
        scope.egresos.diversion_entretenimiento = 0;
        scope.egresos.clubes_deportivos = 0;
        scope.egresos.seguros = 0;
        scope.egresos.vacaciones = 0;
        scope.egresos.otros = 0;
        scope.egresos.otros2 = 0;
        scope.egresos.total_egresos = 0;
        scope.egresos.diferencia_egre_ingre = 0;
        scope.egresos.transporte_escolar = 0;
        scope.egresos.tarjeta_credito = 0;
        scope.egresos.credito_mobiliario_equipo = 0;
        scope.egresos.credito_familiar = 0;
        scope.egresos.credito_trabajo = 0;
        scope.egresos.credito_caja = 0;
        scope.egresos.telefono_movil = 0;
        scope.egresos.internet = 0;
        scope.egresos.cable = 0;
        scope.egresos.seguro_auto = 0;
        scope.egresos.seguro_retiro = 0;
        scope.egresos.seguro_vida = 0;
        scope.egresos.mant_fracc = 0;
        scope.egresos.mant_auto = 0;
        scope.egresos.mant_casa = 0;
        scope.egresos.reinscripciones = 0;
        scope.egresos.utiles_escolares = 0;
        scope.egresos.predial = 0;
        scope.egresos.apoyo_familiar = 0;
        scope.egresos.tenencia = 0;
        scope.egresos.gastos_contabilidad = 0;
        scope.listaGrados = [];
        scope.is_loading = true;

        scope.egresos.servicios_suma = 0;
        scope.egresos.gastos_ciclo_escolar = 0;
        scope.egresos.otros_creditos_total = 0;
        scope.egresos.seguros_total = 0;
        scope.egresos.diversion_total = 0;
        scope.egresos.servicios_mantenimiento_total = 0;

        load();

        scope.find = find;
        function find(hijo) {
            if (scope.listaInstuciones.filter(i => i.id_institucion == hijo.institucion).length == 1) {
                return scope.listaInstuciones.filter(i => i.id_institucion == hijo.institucion)[0].nombre_institucion;
            } else {
                if (scope.listaInstuciones.filter(i => parseInt(i.id_hijo_familia) == parseInt(hijo.id_hijo_familia)).length === 1) {
                    return scope.listaInstuciones.filter(i => parseInt(i.id_hijo_familia) == parseInt(hijo.id_hijo_familia))[0].nombre_institucion;
                }
                return '';
            }
        }

        scope.findGrado = findGrado;
        function findGrado(hijo) {
            if (scope.listaGrados.filter(i => parseInt(i.id_grado) == parseInt(hijo.nivel_grado_cursar)).length > 0) {
                return scope.listaGrados.filter(i => parseInt(i.id_grado) == parseInt(hijo.nivel_grado_cursar))[0].nombre_grado;
            } else {
                return '';
            }
        }

        function sumaCol() {
            for (var i = 0; i < scope.estudio.hijos.length; i++) {
                scope.suma_col += parseFloat(scope.estudio.hijos[i].colegiatura_pasado);
                console.log('Colegiatura: ' + parseFloat(scope.estudio.hijos[i].colegiatura_pasado));
                scope.suma_apoyo_soli += parseFloat(scope.estudio.hijos[i].apoyo_solicitado);
                console.log('Apoyo sol: ' + parseFloat(scope.estudio.hijos[i].apoyo_solicitado));
            }
            if(isNaN(scope.suma_apoyo_soli)){
                mensaje('error', 'Error.', 'Error en apoyo solicitado. Verifique los datos.');
            }
        }

        scope.validarEntero = validarEntero; 
        function validarEntero(valor) {
            valor = parseInt(valor)
            if (isNaN(valor)) {
                return false;
            } else {
                return valor;
            }
        }

        function load() {
            var promesas = [];
            promesas.push(EstudiosService.obtenerDetalleEstudio(EstudiosService.idEstudioSeleccionado, id));
            $q.all(promesas)
                .then(
                    function (response1) {
                        var response = response1[0].data;
                        console.log(response);
                        scope.estudio = response;
                        scope.folio = scope.estudio.folio_estudio_pago;
                        if (scope.id_grupo === '') {
                            scope.id_grupo = scope.estudio.id_grupo;
                        }

                        AdminService.getInstitucionListGrupo(scope.grupo.id_institucion).then(
                            function (response) {
                                scope.listaInstuciones = response.data;
                                if (scope.listaInstuciones.length == 0) {
                                    var id = 0;
                                    if ([null, undefined].includes(scope.grupo.id_institucion)) {
                                        id = scope.id_grupo;
                                    } else {
                                        id = scope.grupo.id_institucion;
                                    }
                                    AdminService.getInstitucionListGrupo(id).then(
                                        function (response) {
                                            scope.listaInstuciones = response.data;
                                            scope.estudio.hijos.forEach(element => {
                                                if (!scope.validarEntero(element.institucion)) {
                                                    var obj = {
                                                        id_hijo_familia: element.id_hijo_familia,
                                                        nombre_institucion: element.institucion
                                                    };
                                                    scope.listaInstuciones.push(obj);
                                                }
                                            });
                                        },
                                        function (err) {
                                            console.log('Error al obtener listaInstuciones' + err);
                                        }
                                    );
                                }
                                scope.estudio.hijos.forEach(element => {
                                    if (!scope.validarEntero(element.institucion)) {
                                        var obj = {
                                            id_hijo_familia: element.id_hijo_familia,
                                            nombre_institucion: element.institucion
                                        };
                                        scope.listaInstuciones.push(obj);
                                    }
                                });
                            },
                            function (err) {
                                console.log('Error al obtener listaInstuciones' + err);
                            }
                        );

                        scope.estudio.hijos.forEach(element => {
                            if (scope.validarEntero(element.institucion)) {
                                console.log(scope.validarEntero(element.institucion));
                                var data = {};
                                if (scope.id_grupo === scope.GRUPO_ITER) {
                                    data = {
                                        clave_institucion: element.institucion,
                                        id_grupo: scope.id_grupo
                                    };
                                }
                                GradoService.getListaGradosByClaveIns(data).then(
                                    function (response) {
                                        response.data.forEach(element => {
                                            scope.listaGrados.push(element);
                                        });
                                    },
                                    function (err) {
                                        console.log('Error al obtener grados' + err);
                                    }
                                );
                            } else {
                                var obj = {
                                    id_hijo_familia: element.id_hijo_familia,
                                    nombre_institucion: element.institucion
                                };
                                scope.listaGrados.push(obj);
                            }

                        });
                        if (response.evaluacion.length > 0) {
                            scope.evaluacion = scope.estudio.evaluacion[0];
                        }

                        if (response.documentos.length > 0) {
                            scope.documentos = scope.estudio.documentos[0];
                        }

                        //reset pasas
                        var papasAux = [];
                        for (var i = 0; i < scope.estudio.padres.length; i++) {
                            if (scope.estudio.padres[i].tipo_persona === 'PAPA') {
                                if (papasAux.length == 0) {
                                    papasAux.push(scope.estudio.padres[i]);
                                    console.log('add papa');
                                }
                            }
                            if (scope.estudio.padres[i].tipo_persona === 'MAMA') {
                                if (papasAux.length == 1) {
                                    papasAux.push(scope.estudio.padres[i]);
                                    console.log('add mama');
                                }
                            }
                        }


                        scope.estudio.padres = papasAux;

                        if (scope.estudio.padres.length > 0) {

                            for (var i = 0; i < scope.estudio.padres.length; i++) {
                                if (scope.estudio.padres[i].tipo_persona === 'PAPA') {
                                    scope.estudio.id_papa = scope.estudio.padres[i].id_padre_familia;
                                    scope.estudio.nombre_papa = scope.estudio.padres[i].nombre;
                                    scope.estudio.apellido_paterno_papa = scope.estudio.padres[i].apellido_paterno;
                                    scope.estudio.apellido_materno_papa = scope.estudio.padres[i].apellido_materno;
                                    scope.estudio.edad_papa = scope.estudio.padres[i].edad;
                                    scope.estudio.correo_papa = scope.estudio.padres[i].correo;
                                    scope.estudio.rfc_papa = scope.estudio.padres[i].rfc;
                                    scope.estudio.celular_papa = scope.estudio.padres[i].celular;
                                    scope.estudio.profesion_papa = scope.estudio.padres[i].profesion;
                                    scope.estudio.ocupacion_papa = scope.estudio.padres[i].ocupacion;
                                    scope.estudio.empresa_papa = scope.estudio.padres[i].empresa;
                                    scope.estudio.puesto_papa = scope.estudio.padres[i].puesto;
                                    scope.estudio.giro_papa = scope.estudio.padres[i].giro;
                                    scope.estudio.dueno_papa = scope.estudio.padres[i].dueno;
                                    scope.estudio.antiguedad_papa = scope.estudio.padres[i].antiguedad;
                                    scope.estudio.sueldo_papa = scope.estudio.padres[i].sueldo_neto;
                                    if (scope.estudio.padres[i].finado === '1') {
                                        scope.estudio.finado_papa = true;
                                    } else {
                                        scope.estudio.finado_papa = false;
                                    }
                                }
                                if (scope.estudio.padres[i].tipo_persona === 'MAMA') {
                                    scope.estudio.id_mama = scope.estudio.padres[i].id_padre_familia;
                                    scope.estudio.nombre_mama = scope.estudio.padres[i].nombre;
                                    scope.estudio.apellido_paterno_mama = scope.estudio.padres[i].apellido_paterno;
                                    scope.estudio.apellido_materno_mama = scope.estudio.padres[i].apellido_materno;
                                    scope.estudio.edad_mama = scope.estudio.padres[i].edad;
                                    scope.estudio.correo_mama = scope.estudio.padres[i].correo;
                                    scope.estudio.rfc_mama = scope.estudio.padres[i].rfc;
                                    scope.estudio.celular_mama = scope.estudio.padres[i].celular;
                                    scope.estudio.profesion_mama = scope.estudio.padres[i].profesion;
                                    scope.estudio.ocupacion_mama = scope.estudio.padres[i].ocupacion;
                                    scope.estudio.empresa_mama = scope.estudio.padres[i].empresa;
                                    scope.estudio.puesto_mama = scope.estudio.padres[i].puesto;
                                    scope.estudio.giro_mama = scope.estudio.padres[i].giro;
                                    scope.estudio.dueno_mama = scope.estudio.padres[i].dueno;
                                    scope.estudio.antiguedad_mama = scope.estudio.padres[i].antiguedad;
                                    scope.estudio.sueldo_mama = scope.estudio.padres[i].sueldo_neto;
                                    if (scope.estudio.padres[i].finado === '1') {
                                        scope.estudio.finado_mama = true;
                                    } else {
                                        scope.estudio.finado_mama = false;
                                    }
                                }
                            }

                        }


                        scope.estudio.sueldo_papa = parseFloat(scope.estudio.sueldo_papa);
                        scope.estudio.sueldo_mama = parseFloat(scope.estudio.sueldo_mama);
                        scope.ingresos.sueldo_papa = scope.estudio.sueldo_papa;
                        scope.ingresos.sueldo_mama = scope.estudio.sueldo_mama;


                        sumaCol();
                        if (response.ingresos.length > 0) {
                            scope.ingresos = response.ingresos[0];
                            parseIngresos();
                            calcula();
                        }

                        if (response.egresos.length > 0) {
                            scope.egresos = response.egresos[0];
                            parseEgresos();
                            scope.calculaEgresosServicios();
                            setBool();
                            calculaEgresos();
                            scope.is_loading = false;
                        }

                    },
                    function (error1) {
                        console.log('Error al obtener el detalle: ' + error1);
                        error();
                    }
                );
        }//end load


        /*FUNCIONES COMUNES*/
        function setSueldo() {
            scope.ingresos.sueldo_papa = scope.estudio.sueldo_papa;
            scope.ingresos.sueldo_mama = scope.estudio.sueldo_mama;
        }

        function calcula() {
            var suma = 0;
            var n = scope.estudio.dependientes.length + scope.estudio.hijos.length;
            //scope.total_ingresos2 = scope.ingresos.total_otros_ingresos;
            scope.total_ingresos2 = scope.ingresos.total_ingresos;
            //var n = scope.estudio.dependientes.length + scope.estudio.hijos.length;
            //scope.ingresos.total_ingresos = suma;

            if (scope.estudio.finado_papa === false && scope.estudio.depende_ingreso_papa) {
                //scope.ingresos.total_ingresos += scope.ingresos.sueldo_papa;
                n++;
            } else {
                //scope.ingresos.sueldo_papa = 0;
            }

            if (scope.estudio.finado_mama === false && scope.estudio.depende_ingreso_mama) {
                //scope.ingresos.total_ingresos += scope.ingresos.sueldo_mama;
                n++;
            } else {
                // scope.ingresos.sueldo_mama = 0;
            }
            scope.ingresos.ingreso_percapita = scope.ingresos.total_ingresos / n;

        }

        function calculaEgresos() {
            var renta = 0;
            var hipoteca = 0;
            var cole = 0;
            var otrasCole = 0;
            var creditoAuto = 0;

            for (var i = 0; i < scope.estudio.propiedades.length; i++) {
                if (scope.estudio.propiedades[i].status === 'Rentada') {
                    renta += parseFloat(scope.estudio.propiedades[i].monto_renta);
                }
                if (scope.estudio.propiedades[i].status === 'Hipotecada') {
                    hipoteca += parseFloat(scope.estudio.propiedades[i].monto_renta);
                }
            }

            for (var i = 0; i < scope.estudio.hijos.length; i++) {
                cole += parseFloat(scope.estudio.hijos[i].colegiatura_actual);
                //otrasCole+=parseFloat(scope.estudio.hijos[i].otras_colegiaturas);
            }

            for (var i = 0; i < scope.estudio.vehiculos.length; i++) {
                creditoAuto += parseFloat(scope.estudio.vehiculos[i].pago_mensual);
            }

            scope.egresos.renta = renta;
            scope.egresos.credito_hipotecario = hipoteca;
            scope.egresos.colegiaturas = cole;
            //scope.egresos.otras_colegiaturas=otrasCole;
            scope.egresos.credito_auto = creditoAuto;
            scope.calculaEgresosTotal();
        }

        function calculaEgresosServicios() {
            scope.egresos.servicios_suma = 0;
            scope.egresos.servicios_suma += scope.egresos.agua;
            scope.egresos.servicios_suma += scope.egresos.luz;
            scope.egresos.servicios_suma += scope.egresos.gas;
            scope.egresos.servicios_suma += scope.egresos.telefono;
            scope.egresos.servicios_suma += scope.egresos.telefono_movil;
            scope.egresos.servicios_suma += scope.egresos.internet;
            scope.egresos.servicios_suma += scope.egresos.cable;
            scope.egresos.servicios_suma += scope.egresos.mant_fracc;

            scope.egresos.gastos_ciclo_escolar = 0;
            scope.egresos.gastos_ciclo_escolar += scope.egresos.reinscripciones;
            scope.egresos.gastos_ciclo_escolar += scope.egresos.utiles_escolares;


            //calcular sueldos total
            scope.ingresos.total_papa = 0;
            scope.ingresos.total_mama = 0;

            scope.ingresos.total_papa += scope.ingresos.sueldo_papa;
            scope.ingresos.total_mama += scope.ingresos.sueldo_mama;

            scope.ingresos.total_papa += scope.ingresos.negocios_papa;
            scope.ingresos.total_mama += scope.ingresos.negocios_mama;

            scope.ingresos.total_papa += scope.ingresos.aguinaldo_papa;
            scope.ingresos.total_mama += scope.ingresos.aguinaldo_mama;

            scope.ingresos.total_papa += scope.ingresos.utilidades_papa;
            scope.ingresos.total_mama += scope.ingresos.utilidades_mama;

            scope.ingresos.total_papa += scope.ingresos.vales_papa;
            scope.ingresos.total_mama += scope.ingresos.vales_mama;

            scope.ingresos.total_papa += (scope.ingresos.bono_mes_papa + scope.ingresos.bono_anual_papa);
            scope.ingresos.total_mama += (scope.ingresos.bono_mes_mama + scope.ingresos.bono_anual_mama);

            scope.ingresos.total_papa += scope.ingresos.vacac_papa;
            scope.ingresos.total_mama += scope.ingresos.vacac_mama;

            scope.ingresos.total_papa += scope.ingresos.comisiones_papa;
            scope.ingresos.total_mama += scope.ingresos.comisiones_mama;

            scope.ingresos.total_papa += scope.ingresos.gratificacion_papa;
            scope.ingresos.total_mama += scope.ingresos.gratificacion_mama;

            scope.ingresos.total_papa += scope.ingresos.fda_papa;
            scope.ingresos.total_mama += scope.ingresos.fda_mama;

            scope.ingresos.total_papa += scope.ingresos.docencia_papa;
            scope.ingresos.total_mama += scope.ingresos.docencia_mama;

            scope.ingresos.total_papa += scope.ingresos.dev_impuestos / 2;
            scope.ingresos.total_mama += scope.ingresos.dev_impuestos / 2;

            scope.ingresos.total_papa += scope.ingresos.ingreso_renta / 2;
            scope.ingresos.total_mama += scope.ingresos.ingreso_renta / 2;

            scope.ingresos.total_papa += scope.ingresos.otros_ingresos / 2;
            scope.ingresos.total_mama += scope.ingresos.otros_ingresos / 2;

            scope.egresos.otros_creditos_total = 0;
            scope.egresos.otros_creditos_total += scope.egresos.credito_mobiliario_equipo;
            scope.egresos.otros_creditos_total += scope.egresos.credito_familiar;
            scope.egresos.otros_creditos_total += scope.egresos.credito_trabajo;
            scope.egresos.otros_creditos_total += scope.egresos.credito_caja;


            scope.egresos.seguros_total = 0;
            scope.egresos.seguros_total += scope.egresos.seguros;
            scope.egresos.seguros_total += scope.egresos.seguro_auto;
            scope.egresos.seguros_total += scope.egresos.seguro_retiro;
            scope.egresos.seguros_total += scope.egresos.seguro_vida;


            scope.egresos.diversion_total = 0;
            scope.egresos.diversion_total += scope.egresos.diversion_entretenimiento;
            scope.egresos.diversion_total += scope.egresos.vacaciones;


            scope.egresos.servicios_mantenimiento_total = 0;
            scope.egresos.servicios_mantenimiento_total += scope.egresos.mant_casa;
            scope.egresos.servicios_mantenimiento_total += scope.egresos.mant_auto;
        }

        function calculaEgresosTotal() {
            scope.egresos.total_egresos = 0;
            scope.egresos.total_egresos += scope.egresos.alimentacion_despensa;
            scope.egresos.total_egresos += scope.egresos.renta;
            scope.egresos.total_egresos += scope.egresos.credito_hipotecario;
            scope.egresos.total_egresos += scope.egresos.servicios_suma;
            scope.egresos.total_egresos += scope.egresos.servicio_domestico;
            scope.egresos.total_egresos += scope.egresos.gasolina;
            scope.egresos.total_egresos += scope.egresos.medico_medicinas;
            scope.egresos.total_egresos += scope.egresos.vestido_calzado;
            scope.egresos.total_egresos += scope.egresos.colegiaturas;
            scope.egresos.total_egresos += scope.egresos.transporte_escolar;
            scope.egresos.total_egresos += scope.egresos.gastos_ciclo_escolar;
            scope.egresos.total_egresos += scope.egresos.clases_particulares;
            scope.egresos.total_egresos += scope.egresos.clubes_deportivos;
            scope.egresos.total_egresos += scope.egresos.credito_auto;
            scope.egresos.total_egresos += scope.egresos.tarjeta_credito;
            scope.egresos.total_egresos += scope.egresos.otros_creditos_total;
            scope.egresos.total_egresos += scope.egresos.seguros_total;
            scope.egresos.total_egresos += scope.egresos.diversion_total;
            scope.egresos.total_egresos += scope.egresos.servicios_mantenimiento_total;
            scope.egresos.total_egresos += scope.egresos.tenencia;
            scope.egresos.total_egresos += scope.egresos.predial;
            scope.egresos.total_egresos += scope.egresos.gastos_contabilidad;
            scope.egresos.total_egresos += scope.egresos.apoyo_familiar;
            scope.egresos.total_egresos += scope.egresos.otros2;


            if (isNaN(scope.egresos.total_egresos) === true) {
                mensaje('error', 'Error.', 'Error en egresos.');
            }
            console.log('INGRESOS: ' + scope.ingresos.total_ingresos);
            console.log('EGRESOS: ' + scope.egresos.total_egresos);
            scope.egresos.diferencia_egre_ingre = scope.ingresos.total_ingresos - scope.egresos.total_egresos;
            scope.total_2 = scope.egresos.total_egresos;

            scope.diferencia_2 = scope.ingresos.total_ingresos - scope.total_2;

            //scope.porcentaje=(scope.suma_col/scope.estudio.ingresos[0].total_ingresos)*100;
            //var sueldo = scope.total_ingresos2;
            var sueldo = scope.ingresos.total_ingresos;
            console.log('sueldo total: ' + scope.total_ingresos2);
            console.log('scope.suma_col;' + scope.suma_col);
            scope.porcentaje = (scope.suma_col / sueldo) * 100;
            if (isNaN(scope.porcentaje)) {
                mensaje('error', 'Error.', 'Error en ingresos [Error formula].');
            }
            console.log('por: ' + scope.porcentaje);
            if (scope.porcentaje >= 30) {
                scope.factor1 = 3.33;
            } else {
                if (scope.porcentaje <= 10) {
                    scope.factor1 = 1.11;
                } else {
                    scope.factor1 = 2.22;
                }
            }
            console.log('Factor1: ' + scope.factor1);

            console.log(scope.ingresos.clasificacion);
            if (scope.ingresos.clasificacion === 'C') {
                scope.factor2 = 3.33;
            } else if (scope.ingresos.clasificacion === 'A') {
                scope.factor2 = 1.11;
            } else {
                scope.factor2 = 2.22;
            }
            console.log('Factor2: ' + scope.factor2);

            var hijos = parseInt(scope.estudio.hijos.length);
            console.log('hijos: ' + scope.estudio.hijos.length);
            if (hijos >= 5) {
                scope.factor3 = 3.33;
            } else if (hijos <= 2) {
                scope.factor3 = 1.11;
            } else {
                scope.factor3 = 2.22;
            }
            console.log('Factor3: ' + scope.factor3);
            scope.necesidad = scope.factor1 + scope.factor2 + scope.factor3;
            console.log('Necesidad: ' + scope.necesidad);

            console.log('-----------------------------');
            console.log(scope.evaluacion.apreciacion);
            if (scope.evaluacion.apreciacion === undefined) {
                mensaje('error', 'Error.', 'Falta sección evaluación.');
            }
            if (scope.evaluacion.apreciacion === 'NADA CONFIABLE') {
                scope.factor1_1 = 0;
            } else if (scope.evaluacion.apreciacion === 'POCO CONFIABLE') {
                scope.factor1_1 = 2.22;
            } else {
                scope.factor1_1 = 3.33;
            }
            console.log('factor 1_1: ' + scope.factor1_1);

            console.log('-----------------------------');
            console.log(scope.evaluacion.discrepancia);
            if (scope.evaluacion.discrepancia === 'NINGUNA') {
                scope.factor2_2 = 3.33;
            } else if (scope.evaluacion.discrepancia === 'GRAVE') {
                scope.factor2_2 = 1.11;
            } else {
                scope.factor2_2 = 2.22;
            }
            console.log('factor 2_2: ' + scope.factor2_2);
            console.log('-----------------------------');

            console.log('Suma docs: ' + scope.suma_docs);
            scope.factor3_3 = (scope.suma_docs * 3.3) / 13;
            console.log('factor 3_3: ' + scope.factor3_3);

            scope.confiabilidad = scope.factor1_1 + scope.factor2_2 + scope.factor3_3;
            console.log('confiabilidad:' + scope.confiabilidad);
            scope.calif_globlal = (scope.necesidad * .8) + (scope.confiabilidad * .2);

            //scope.resta_porcentaje=(scope.suma_col/scope.estudio.ingresos[0].total_ingresos);
            scope.resta_porcentaje = (scope.suma_col / scope.total_ingresos2);
            console.log('resta porcen: ' + scope.resta_porcentaje);
            scope.resta_porcentaje -= .20;
            //scope.resta_porcentaje=Math.ceil(scope.resta_porcentaje);;
            console.log('resta porcen: ' + scope.resta_porcentaje);
            console.log('porcentaje:' + scope.porcentaje);
            //scope.factorN=scope.estudio.ingresos[0].total_ingresos*scope.resta_porcentaje;
            scope.factorN = scope.total_ingresos2 * scope.resta_porcentaje;
            //scope.factorN=Math.ceil(scope.factorN);
            console.log('factorN: ' + scope.factorN);
            scope.suma_apoyo_suge = 0;
            for (var i = 0; i < scope.estudio.hijos.length; i++) {
                var x = scope.estudio.hijos[i].colegiatura_pasado;
                console.log('X: ' + x);
                var y = x / scope.suma_col;
                console.log('Y: ' + y);
                scope.estudio.hijos[i].apoyo_sugerido = scope.factorN * y;
                scope.suma_apoyo_suge += scope.estudio.hijos[i].apoyo_sugerido;
            }

        }

        function setBool() {
            console.log('000000000000000000000000000000000000');
            scope.suma_docs = 0;

            if (scope.documentos.id_documento_familia === undefined) {
                mensaje('error', 'Error.', 'Falta sección documentos.');
            }
            console.log(scope.documentos.id_documento_familia);
            if (scope.documentos.id_documento_familia === undefined || scope.documentos.id_documento_familia === null) {
                scope.suma_docs = 13 * 1.01;
            }
            if (scope.documentos.carta_no_adeudo === '1') {
                scope.documentos.carta_no_adeudo = true;
            }
            if (scope.documentos.carta_no_adeudo === '0') {
                scope.documentos.carta_no_adeudo = false;
                scope.suma_docs += 1.01;
            }

            if (scope.documentos.firma_reglamento === '1') {
                scope.documentos.firma_reglamento = true;
            }
            if (scope.documentos.firma_reglamento === '0') {
                scope.documentos.firma_reglamento = false;
                scope.suma_docs += 1.01;
            }

            if (scope.documentos.nomina_carta === '1') {
                scope.documentos.nomina_carta = true;
            }
            if (scope.documentos.nomina_carta === '0') {
                scope.documentos.nomina_carta = false;
                scope.suma_docs += 1.01;
            }

            if (scope.documentos.poliza === '1') {
                scope.documentos.poliza = true;
            }
            if (scope.documentos.poliza === '0') {
                scope.documentos.poliza = false;
                scope.suma_docs += 1.01;
            }

            if (scope.documentos.estado_cuenta === '1') {
                scope.documentos.estado_cuenta = true;
            }
            if (scope.documentos.estado_cuenta === '0') {
                scope.documentos.estado_cuenta = false;
                scope.suma_docs += 1.01;
            }

            if (scope.documentos.recibos_renta === '1') {
                scope.documentos.recibos_renta = true;
            }
            if (scope.documentos.recibos_renta === '0') {
                scope.documentos.recibos_renta = false;
                scope.suma_docs += 1.01;
            }

            if (scope.documentos.facturas_hospital === '1') {
                scope.documentos.facturas_hospital = true;
            }
            if (scope.documentos.facturas_hospital === '0') {
                scope.documentos.facturas_hospital = false;
                scope.suma_docs += 1.01;
            }

            if (scope.documentos.comprobante_finiquito === '1') {
                scope.documentos.comprobante_finiquito = true;
            }
            if (scope.documentos.comprobante_finiquito === '0') {
                scope.documentos.comprobante_finiquito = false;
                scope.suma_docs += 1.01;
            }

            if (scope.documentos.demandas_judiciales === '1') {
                scope.documentos.demandas_judiciales = true;
            }
            if (scope.documentos.demandas_judiciales === '0') {
                scope.documentos.demandas_judiciales = false;
                scope.suma_docs += 1.01;
            }

            if (scope.documentos.servicios === '1') {
                scope.documentos.servicios = true;
            }
            if (scope.documentos.servicios === '0') {
                scope.documentos.servicios = false;
                scope.suma_docs += 1.01;
            }

            if (scope.documentos.pagos_credito_hipo === '1') {
                scope.documentos.pagos_credito_hipo = true;
                console.log('Falto pagos credi');
            }
            if (scope.documentos.pagos_credito_hipo === '0') {
                scope.documentos.pagos_credito_hipo = false;
                scope.suma_docs += 1.01;
            }

            if (scope.documentos.pagos_credito_auto === '1') {
                scope.documentos.pagos_credito_auto = true;
            }
            if (scope.documentos.pagos_credito_auto === '0') {
                scope.documentos.pagos_credito_auto = false;
                scope.suma_docs += 1.01;
            }

            if (scope.documentos.otros === '1') {
                scope.documentos.otros = true;
                console.log('Falto otros');
            }
            if (scope.documentos.otros === '0') {
                scope.documentos.otros = false;
                scope.suma_docs += 1.01;
            }
        }

        function parseIngresos() {
            scope.estudio.sueldo_papa = parseFloat(scope.ingresos.sueldo_papa);
            scope.estudio.sueldo_mama = parseFloat(scope.ingresos.sueldo_mama);
            scope.ingresos.ingreso_otros_miembros = parseFloat(scope.ingresos.ingreso_otros_miembros);
            scope.ingresos.ingreso_renta = parseFloat(scope.ingresos.ingreso_renta);
            scope.ingresos.ingreso_honorarios = parseFloat(scope.ingresos.ingreso_honorarios);
            scope.ingresos.ingreso_inversiones = parseFloat(scope.ingresos.ingreso_inversiones);
            scope.ingresos.ingreso_pensiones = parseFloat(scope.ingresos.ingreso_pensiones);
            scope.ingresos.ingreso_ventas = parseFloat(scope.ingresos.ingreso_ventas);
            scope.ingresos.otros_ingresos = parseFloat(scope.ingresos.otros_ingresos);
            scope.ingresos.total_otros_ingresos = parseFloat(scope.ingresos.total_otros_ingresos);
            scope.ingresos.sueldo_papa = parseFloat(scope.ingresos.sueldo_papa);
            scope.ingresos.sueldo_mama = parseFloat(scope.ingresos.sueldo_mama);
            scope.ingresos.ingreso_percapita = parseFloat(scope.ingresos.ingreso_percapita);
            scope.ingresos.total_ingresos = parseFloat(scope.ingresos.total_ingresos);
            scope.ingresos.num_personas_aportan = parseFloat(scope.ingresos.num_personas_aportan);
            scope.ingresos.negocios_papa = parseFloat(scope.ingresos.negocios_papa);
            scope.ingresos.negocios_mama = parseFloat(scope.ingresos.negocios_mama);
            scope.ingresos.apoyo_familiar = parseFloat(scope.ingresos.apoyo_familiar);
            scope.ingresos.aguinaldo_papa = parseFloat(scope.ingresos.aguinaldo_papa);
            scope.ingresos.aguinaldo_mama = parseFloat(scope.ingresos.aguinaldo_mama);
            scope.ingresos.utilidades_papa = parseFloat(scope.ingresos.utilidades_papa);
            scope.ingresos.utilidades_mama = parseFloat(scope.ingresos.utilidades_mama);
            scope.ingresos.vales_papa = parseFloat(scope.ingresos.vales_papa);
            scope.ingresos.vales_mama = parseFloat(scope.ingresos.vales_mama);
            scope.ingresos.bono_mes_papa = parseFloat(scope.ingresos.bono_mes_papa);
            scope.ingresos.bono_mes_mama = parseFloat(scope.ingresos.bono_mes_mama);
            scope.ingresos.bono_anual_papa = parseFloat(scope.ingresos.bono_anual_papa);
            scope.ingresos.bono_anual_mama = parseFloat(scope.ingresos.bono_anual_mama);
            scope.ingresos.vacac_papa = parseFloat(scope.ingresos.vacac_papa);
            scope.ingresos.vacac_mama = parseFloat(scope.ingresos.vacac_mama);
            scope.ingresos.comisiones_papa = parseFloat(scope.ingresos.comisiones_papa);
            scope.ingresos.comisiones_mama = parseFloat(scope.ingresos.comisiones_mama);
            scope.ingresos.gratificacion_papa = parseFloat(scope.ingresos.gratificacion_papa);
            scope.ingresos.gratificacion_mama = parseFloat(scope.ingresos.gratificacion_mama);
            scope.ingresos.fda_papa = parseFloat(scope.ingresos.fda_papa);
            scope.ingresos.fda_mama = parseFloat(scope.ingresos.fda_mama);
            scope.ingresos.docencia_papa = parseFloat(scope.ingresos.docencia_papa);
            scope.ingresos.docencia_mama = parseFloat(scope.ingresos.docencia_mama);
            scope.ingresos.dev_impuestos = parseFloat(scope.ingresos.dev_impuestos);
            scope.ingresos.total_papa = parseFloat(scope.ingresos.total_papa);
            scope.ingresos.total_mama = parseFloat(scope.ingresos.total_mama);
        }

        function parseEgresos() {
            scope.egresos.alimentacion_despensa = parseFloat(scope.egresos.alimentacion_despensa);
            scope.egresos.renta = parseFloat(scope.egresos.renta);
            scope.egresos.credito_hipotecario = parseFloat(scope.egresos.credito_hipotecario);
            scope.egresos.colegiaturas = parseFloat(scope.egresos.colegiaturas);
            scope.egresos.otras_colegiaturas = parseFloat(scope.egresos.otras_colegiaturas);
            scope.egresos.clases_particulares = parseFloat(scope.egresos.clases_particulares);
            scope.egresos.agua = parseFloat(scope.egresos.agua);
            scope.egresos.luz = parseFloat(scope.egresos.luz);
            scope.egresos.telefono = parseFloat(scope.egresos.telefono);
            scope.egresos.servicio_domestico = parseFloat(scope.egresos.servicio_domestico);
            scope.egresos.gas = parseFloat(scope.egresos.gas);
            scope.egresos.total_servicios = parseFloat(scope.egresos.total_servicios);
            scope.egresos.gasolina = parseFloat(scope.egresos.gasolina);
            scope.egresos.credito_auto = parseFloat(scope.egresos.credito_auto);
            scope.egresos.pago_tdc_mensual = parseFloat(scope.egresos.pago_tdc_mensual);
            scope.egresos.saldo_tdc = parseFloat(scope.egresos.saldo_tdc);
            scope.egresos.creditos_comerciales = parseFloat(scope.egresos.creditos_comerciales);
            scope.egresos.vestido_calzado = parseFloat(scope.egresos.vestido_calzado);
            scope.egresos.medico_medicinas = parseFloat(scope.egresos.medico_medicinas);
            scope.egresos.diversion_entretenimiento = parseFloat(scope.egresos.diversion_entretenimiento);
            scope.egresos.clubes_deportivos = parseFloat(scope.egresos.clubes_deportivos);
            scope.egresos.seguros = parseFloat(scope.egresos.seguros);
            scope.egresos.vacaciones = parseFloat(scope.egresos.vacaciones);
            scope.egresos.otros2 = parseFloat(scope.egresos.otros2);
            scope.egresos.otros = parseFloat(scope.egresos.otros);
            scope.egresos.total_egresos = parseFloat(scope.egresos.total_egresos);
            scope.egresos.diferencia_egre_ingre = parseFloat(scope.egresos.diferencia_egre_ingre);
            scope.egresos.transporte_escolar = parseFloat(scope.egresos.transporte_escolar);
            scope.egresos.tarjeta_credito = parseFloat(scope.egresos.tarjeta_credito);
            scope.egresos.credito_mobiliario_equipo = parseFloat(scope.egresos.credito_mobiliario_equipo);
            scope.egresos.credito_familiar = parseFloat(scope.egresos.credito_familiar);
            scope.egresos.credito_trabajo = parseFloat(scope.egresos.credito_trabajo);
            scope.egresos.credito_caja = parseFloat(scope.egresos.credito_caja);
            scope.egresos.telefono_movil = parseFloat(scope.egresos.telefono_movil);
            scope.egresos.internet = parseFloat(scope.egresos.internet);
            scope.egresos.cable = parseFloat(scope.egresos.cable);
            scope.egresos.seguro_auto = parseFloat(scope.egresos.seguro_auto);
            scope.egresos.seguro_retiro = parseFloat(scope.egresos.seguro_retiro);
            scope.egresos.seguro_vida = parseFloat(scope.egresos.seguro_vida);
            scope.egresos.mant_fracc = parseFloat(scope.egresos.mant_fracc);
            scope.egresos.mant_auto = parseFloat(scope.egresos.mant_auto);
            scope.egresos.mant_casa = parseFloat(scope.egresos.mant_casa);
            scope.egresos.reinscripciones = parseFloat(scope.egresos.reinscripciones);
            scope.egresos.utiles_escolares = parseFloat(scope.egresos.utiles_escolares);
            scope.egresos.predial = parseFloat(scope.egresos.predial);
            scope.egresos.apoyo_familiar = parseFloat(scope.egresos.apoyo_familiar);
            scope.egresos.tenencia = parseFloat(scope.egresos.tenencia);
            scope.egresos.gastos_contabilidad = parseFloat(scope.egresos.gastos_contabilidad);
            scope.egresos.servicios_suma = parseFloat(scope.egresos.servicios_suma);
            scope.egresos.gastos_ciclo_escolar = parseFloat(scope.egresos.gastos_ciclo_escolar);
            scope.egresos.otros_creditos_total = parseFloat(scope.egresos.otros_creditos_total);
            scope.egresos.seguros_total = parseFloat(scope.egresos.seguros_total);
            scope.egresos.diversion_total = parseFloat(scope.egresos.diversion_total);
            scope.egresos.servicios_mantenimiento_total = parseFloat(scope.egresos.servicios_mantenimiento_total);
        }

    }
    ;//end controller

})();