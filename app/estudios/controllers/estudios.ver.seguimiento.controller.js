(function () {
    'use strict';

    angular
            .module('app.estudios')
            .controller('EstudiosVerSeguimientoController', EstudiosVerSeguimientoController);

    function EstudiosVerSeguimientoController(AdminService, ColoniaService, EstudiosService, ComentarioService, $localStorage, $rootScope, $state, $mdDialog, $mdToast, DialogService, AuthenticationService, Constants, UserService, $q) {
        /* jshint validthis: true */
        console.log('init EstudiosVerSeguimientoController');

        show();
        //Check session
        if (!AuthenticationService.isAuth()) {
            window.location.href = '#/login';
            return;
        };
        var scope = this;
        scope.user = {};
        scope.tipoUsuario = 0;
        scope.rolUsuario = 0;
        scope.institucion = {};
        scope.user = $localStorage.data.user;
        scope.tipoUsuario = $localStorage.globals.type;
        scope.rolUsuario = $localStorage.globals.role;
        if (scope.tipoUsuario == '2') {
            scope.institucion = $localStorage.data.institucion;
        }

        if ($state.params.id === '' || $state.params.id === undefined) {

        }
        scope.id = 0;
        if (scope.tipoUsuario === '2') {
            scope.id = scope.institucion.id_institucion;
        }

        if (scope.id === undefined) {
            //error();
        }

        scope.estudio = {};
        scope.get = get;
        scope.get();
        scope.comentarios = [];
        scope.listaComentarios = [];
        scope.listaEstatus = [];

        EstudiosService.obtenerEstatusCat().then(
            function (response) {
                scope.listaEstatus = response.data;
            }, function (error) {
                console.log('Error al obtener el obtenerEstatusCat: ' + error);
            }
        );


        function get() {
            EstudiosService.obtenerDetalleEstudio($state.params.id, '0').then(
                    function (response) {
                        scope.estudio = response.data;
                        scope.comentarios = scope.estudio.comentarios;

                        for (var i = 0; i < scope.comentarios.length; i++) {
                            if (scope.comentarios[i].tipo !== 'GENERAL') {
                                scope.listaComentarios.push(scope.comentarios[i]);
                            }
                        }
                        hide();
                        
                    },
                    function (err) {
                        console.log('Error al obtener grados' + err);
                    }
            );
        }



    }
    ;//end controller

})();