(function () {
    'use strict';

    angular
        .module('app.estudios')
        .controller('EstudiosDetalleInsignaController', EstudiosDetalleInsignaController);

    function EstudiosDetalleInsignaController(
        GradoService,
        VehiculoService,
        FamiliaService,
        EstudiosService,
        HijoService,
        PropiedadService,
        DependienteService,
        MotivoService,
        ComentarioService,
        $localStorage,
        $rootScope,
        $state,
        $mdDialog,
        $mdToast,
        DialogService, RestService, AuthenticationService, Constants, UserService, AdminService, $q) {
        /* jshint validthis: true */
        console.log('init estudios ver EstudiosDetalleInsignaController');
        show();
        //Check session
        if (!AuthenticationService.isAuth()) {
            window.location.href = '#/login';
            return;
        };
        var scope = this;
        scope.user = {};
        scope.tipoUsuario = 0;
        scope.rolUsuario = 0;
        scope.institucion = {};
        scope.user = $localStorage.data.user;
        scope.tipoUsuario = $localStorage.globals.type;
        scope.rolUsuario = $localStorage.globals.role;
        if (scope.tipoUsuario == '2') {
            scope.institucion = $localStorage.data.institucion;
            scope.instituciones = $localStorage.data.instituciones;
        }

        if ($state.params.id === '' || $state.params.id === undefined) {

        }

        scope.no_auto = false;
        scope.no_insigna = true;

        EstudiosService.idEstudioSeleccionado = $state.params.id;
        var id = 0;
        if (scope.tipoUsuario === '2') {
            id = scope.institucion.id_institucion;
        }

        scope.id = id;
        scope.grupo = {};
        scope.GRUPO_INSIGNA = Constants.GRUPO_INSIGNA;
        scope.GRUPO_ITER = Constants.GRUPO_ITER;
        scope.id_grupo = '';
        if (![null, undefined].includes(scope.institucion.grupo)) {
            scope.grupo = scope.institucion.grupo;
        }

        scope.no_auto = false;
        scope.no_insigna = true;

        scope.estudio = {};
        scope.hijo = {};
        scope.dependiente = {};
        scope.motivo = {};
        scope.vehiculo = {};
        scope.propiedad = {};
        scope.comentario = {};
        scope.listaEmpleado = [];
        scope.listaEstatus = [];
        scope.listaGrados = [];
        scope.grado = {};
        scope.empleadoAsignado = {};
        scope.ingresos = {};
        scope.egresos = {};
        scope.documentos = {};
        scope.evaluacion = {};
        scope.pasivo = {};

        scope.load = load;
        scope.verAccion = false;
        scope.banderaActualizar = false;

        scope.verReporte = verReporte;
        /*papa-mama*/
        scope.guardarPapa = guardarPapa;
        scope.guardarMama = guardarMama;

        /*hijo*/
        scope.verActualizarHijo = verActualizarHijo;
        scope.eliminarHijo = eliminarHijo;
        scope.agregarHijo = agregarHijo;
        scope.guardarHijo = guardarHijo;
        /*dependiente*/
        scope.verActualizarDependiente = verActualizarDependiente;
        scope.eliminarDependiente = eliminarDependiente;
        scope.agregarDependiente = agregarDependiente;
        scope.guardarDependiente = guardarDependiente;
        /*motivo*/
        scope.verActualizarMotivo = verActualizarMotivo;
        scope.eliminarMotivo = eliminarMotivo;
        scope.agregarMotivo = agregarMotivo;
        scope.guardarMotivo = guardarMotivo;

        /*vehiculo*/
        scope.verActualizarVehiculo = verActualizarVehiculo;
        scope.eliminarVehiculo = eliminarVehiculo;
        scope.agregarVehiculo = agregarVehiculo;
        scope.guardarVehiculo = guardarVehiculo;

        /*propiedad*/
        scope.verActualizarPropiedad = verActualizarPropiedad;
        scope.eliminarPropiedad = eliminarPropiedad;
        scope.agregarPropiedad = agregarPropiedad;
        scope.guardarPropiedad = guardarPropiedad;

        /*asignacion*/
        scope.asignarShow = asignarShow;
        scope.asignarEmpleado = asignarEmpleado;

        scope.cambiarEstatusView = cambiarEstatusView;
        scope.cambiarEstatus = cambiarEstatus;

        /*ingresos*/
        scope.guardarIngresos = guardarIngresos;

        /*Egresos*/
        scope.guardarEgresos = guardarEgresos;

        /*Docs*/
        scope.guardarDocumentos = guardarDocumentos;

        /*Evaluacion*/
        scope.guardarEvaluacion = guardarEvaluacion;

        /*Comentarios*/
        scope.verActualizarComentario = verActualizarComentario;
        scope.eliminarComentario = eliminarComentario;
        scope.agregarComentario = agregarComentario;
        scope.guardarComentario = guardarComentario;
        scope.calcula = calcula;
        scope.setSueldo = setSueldo;

        /*cambia estatus estudio*/
        scope.finalizarCaptura = finalizarCaptura;
        scope.enviarARevision = enviarARevision;
        scope.enviarATerminado = enviarATerminado;

        scope.calculaEgresos = calculaEgresos;
        scope.calculaEgresosServicios = calculaEgresosServicios;
        scope.calculaEgresosTotal = calculaEgresosTotal;

        scope.updateCals = function () {
            scope.calcula();
            scope.calculaEgresos();
            scope.calculaEgresosServicios();
            scope.calculaEgresosTotal();
            scope.validaCompletos();
        };

        scope.estudio.total_depen = 0;
        scope.ingresos.ingreso_otros_miembros = 0;
        scope.ingresos.ingreso_renta = 0;
        scope.ingresos.ingreso_honorarios = 0;
        scope.ingresos.ingreso_inversiones = 0;
        scope.ingresos.ingreso_pensiones = 0;
        scope.ingresos.ingreso_ventas = 0;
        scope.ingresos.otros_ingresos = 0;
        scope.ingresos.total_otros_ingresos = 0;
        scope.ingresos.sueldo_papa = 0;
        scope.ingresos.sueldo_mama = 0;
        scope.ingresos.ingreso_percapita = 0;
        scope.ingresos.clasificacion = '';
        scope.ingresos.num_personas_aportan = 0;
        scope.ingresos.negocios_papa = 0;
        scope.ingresos.negocios_mama = 0;
        scope.ingresos.apoyo_familiar = 0;
        scope.ingresos.aguinaldo_papa = 0;
        scope.ingresos.aguinaldo_mama = 0;
        scope.ingresos.utilidades_papa = 0;
        scope.ingresos.utilidades_mama = 0;
        scope.ingresos.vales_papa = 0;
        scope.ingresos.vales_mama = 0;
        scope.ingresos.bono_mes_papa = 0;
        scope.ingresos.bono_mes_mama = 0;
        scope.ingresos.bono_anual_papa = 0;
        scope.ingresos.bono_anual_mama = 0;
        scope.ingresos.vacac_papa = 0;
        scope.ingresos.vacac_mama = 0;
        scope.ingresos.comisiones_papa = 0;
        scope.ingresos.comisiones_mama = 0;
        scope.ingresos.gratificacion_papa = 0;
        scope.ingresos.gratificacion_mama = 0;
        scope.ingresos.fda_papa = 0;
        scope.ingresos.fda_mama = 0;
        scope.ingresos.docencia_papa = 0;
        scope.ingresos.docencia_mama = 0;
        scope.ingresos.dev_impuestos = 0;

        scope.egresos.alimentacion_despensa = 0;
        scope.egresos.renta = 0;
        scope.egresos.credito_hipotecario = 0;
        scope.egresos.colegiaturas = 0;
        scope.egresos.otras_colegiaturas = 0;
        scope.egresos.clases_particulares = 0;
        scope.egresos.agua = 0;
        scope.egresos.luz = 0;
        scope.egresos.telefono = 0;
        scope.egresos.celulares = 0;
        scope.egresos.servicio_domestico = 0;
        scope.egresos.gas = 0;
        scope.egresos.total_servicios = 0;
        scope.egresos.gasolina = 0;
        scope.egresos.credito_auto = 0;
        scope.egresos.pago_tdc_mensual = 0;
        scope.egresos.saldo_tdc = 0;
        scope.egresos.creditos_comerciales = 0;
        scope.egresos.vestido_calzado = 0;
        scope.egresos.medico_medicinas = 0;
        scope.egresos.diversion_entretenimiento = 0;
        scope.egresos.clubes_deportivos = 0;
        scope.egresos.seguros = 0;
        scope.egresos.vacaciones = 0;
        scope.egresos.otros = 0;
        scope.egresos.otros2 = 0;
        scope.egresos.total_egresos = 0;
        scope.egresos.diferencia_egre_ingre = 0;
        scope.egresos.transporte_escolar = 0;
        scope.egresos.tarjeta_credito = 0;
        scope.egresos.credito_mobiliario_equipo = 0;
        scope.egresos.credito_familiar = 0;
        scope.egresos.credito_trabajo = 0;
        scope.egresos.credito_caja = 0;
        scope.egresos.telefono_movil = 0;
        scope.egresos.internet = 0;
        scope.egresos.cable = 0;
        scope.egresos.seguro_auto = 0;
        scope.egresos.seguro_retiro = 0;
        scope.egresos.seguro_vida = 0;
        scope.egresos.mant_fracc = 0;
        scope.egresos.mant_auto = 0;
        scope.egresos.mant_casa = 0;
        scope.egresos.reinscripciones = 0;
        scope.egresos.utiles_escolares = 0;
        scope.egresos.predial = 0;
        scope.egresos.apoyo_familiar = 0;
        scope.egresos.tenencia = 0;
        scope.egresos.gastos_contabilidad = 0;


        scope.setColegaturaGrado = setColegaturaGrado;

        scope.fotoPapa;
        scope.fotoMama;
        scope.filePapa;
        scope.fileMama;
    
        scope.urlPapa = "";
        scope.urlMama = "";

        scope.load();

        scope.models = {
            selected: null,
            lists: { "A": [], "B": [] }
        };
        scope.comentarios = [];

        scope.filtros = {};
        scope.filtro_uno = false;
        scope.filtro_dos = false;
        scope.filtro_tres = false;

        scope.saving = false;
        scope.setSave = setSave;
        function setSave() {
            scope.saving = scope.saving == true ? false : true;
        }


        scope.getGrados = getGrados;
        function getGrados(institucion) {
            console.log(institucion);
            var data = {};
            if (scope.id_grupo === scope.GRUPO_INSIGNA) {
                data = {
                    clave_institucion: institucion,
                    id_grupo: scope.id_grupo
                };
            }
            if (scope.id_grupo === scope.GRUPO_ITER) {
                data = {
                    clave_institucion: institucion,
                    id_grupo: scope.id_grupo
                };
            }
            GradoService.getListaGradosByClaveIns(data).then(
                function (response) {
                    scope.listaGrados = response.data;
                },
                function (err) {
                    console.log('Error al obtener grados' + err);
                }
            );
        }

        scope.find = find;
        function find(hijo) {
            if (scope.listaInstuciones.filter(i => i.id_institucion == hijo.institucion).length == 1) {
                return scope.listaInstuciones.filter(i => i.id_institucion == hijo.institucion)[0].nombre_institucion;
            } else {
                if (scope.listaInstuciones.filter(i => parseInt(i.id_hijo_familia) == parseInt(hijo.id_hijo_familia)).length === 1) {
                    return scope.listaInstuciones.filter(i => parseInt(i.id_hijo_familia) == parseInt(hijo.id_hijo_familia))[0].nombre_institucion;
                }
                return '-';
            }
        }

        scope.validarEntero = validarEntero;
        function validarEntero(valor) {
            valor = parseInt(valor)
            if (isNaN(valor)) {
                return false;
            } else {
                return valor;
            }
        }

        function setColegaturaGrado() {
            var data = {};
            if (scope.id_grupo === scope.GRUPO_INSIGNA) {
                data = {
                    nombre_grado: scope.hijo.nivel_grado_cursar,
                    id_grupo: scope.id_grupo
                };
            }
            if (scope.id_grupo === scope.GRUPO_ITER) {
                data = {
                    nombre_grado: scope.hijo.nivel_grado_cursar,
                    id_grupo: scope.id_grupo
                };
            }

            GradoService.getByNombrePost(data).then(
                function (response) {
                    if (scope.id_grupo === scope.GRUPO_INSIGNA) {
                        scope.grado = response.data;
                        scope.hijo.nivel_grado_cursar = scope.grado.nombre_grado;
                        scope.hijo.colegiatura_pasado = parseFloat(scope.grado.colegiatura);
                    }
                    if (scope.id_grupo === scope.GRUPO_ITER) {
                        scope.grado = response.data;
                        scope.hijo.nivel_grado_cursar = scope.grado.id_grado;
                        scope.hijo.colegiatura_pasado = parseFloat(scope.grado.colegiatura);
                    }

                },
                function (err) {
                    console.log('Error al obtener grados' + err);
                }
            );
        }

        scope.institucionSeleccionada = {};
        scope.seleccionarInst = seleccionarInst;

        function seleccionarInst(i) {
            scope.institucionSeleccionada = i;
            console.log(scope.institucionSeleccionada);
        }

        scope.verReporteGo = verReporteGo;

        function verReporteGo() {
            if (scope.id_grupo === scope.GRUPO_ITER) {
                window.location.href = '#/estudios/reporte-iter/' + scope.estudio.id_estudio;
                return;
            }
            if (scope.id_grupo === scope.GRUPO_INSIGNA) {
                window.location.href = '#/estudios/reporte-insigna/' + scope.estudio.id_estudio;
                return;
            }
            if (scope.institucionSeleccionada.num_recibo === undefined) {
                alert('Seleccione un folio para el reporte.');
                return;
            }
            $("#modal_compartido").modal('hide');
            show();
            setTimeout(function () {
                $rootScope.idEstudioReporte = scope.estudio.id_estudio;
                window.location.href = '#/estudios/reporte/' + scope.estudio.id_estudio + '/' + scope.institucionSeleccionada.num_recibo;
            }, 2000);

        }

        function verReporte() {
            $("#modal_compartido").modal('show');
        }

        function enviarATerminado() {
            var obj = {};
            obj.id_estudio = scope.estudio.id_estudio;
            obj.id_estatus_estudio = 7;
            confirmaMsj(
                "Confirmación de solicitud",
                "¿Terminar estudio? Será visible para la institución.",
                "Si",
                function () {
                    show();
                    var promesas = [];
                    promesas.push(EstudiosService.actualizarEstudio(obj));
                    $q.all(promesas).then(
                        function (response) {
                            load();
                        }
                    );
                },
                "No",
                function () { }
            );
        }
        function finalizarCaptura(ev) {
            var obj = {};
            obj.id_estudio = scope.estudio.id_estudio;
            obj.id_estatus_estudio = 5;
            if ($rootScope.isLocal) {
                obj.sync = 1;
            }
            var quest = '¿Desea continuar?';
            var confirm = DialogService.dialogConfirm(ev, 'Finalizar captura', quest, 'Si', 'No');
            $mdDialog.show(confirm).then(function () {
                show();
                var promesas = [];
                promesas.push(EstudiosService.actualizarEstudio(obj));
                $q.all(promesas).then(
                    function (response) {
                        load();
                    }
                );

            }, function () {
                $mdDialog.hide();
            });

        }

        function enviarARevision(ev) {
            var obj = {};
            obj.id_estudio = scope.estudio.id_estudio;
            obj.id_estatus_estudio = 6;
            if ($rootScope.isLocal) {
                obj.sync = 1;
            }
            var quest = '¿Desea continuar?';
            var confirm = DialogService.dialogConfirm(ev, 'Enviar a revisión', quest, 'Si', 'No');
            $mdDialog.show(confirm).then(function () {
                show();
                var promesas = [];
                promesas.push(EstudiosService.actualizarEstudio(obj));
                $q.all(promesas).then(
                    function (response) {
                        load();
                    }
                );

            }, function () {
                $mdDialog.hide();
            });

        }

        function fecha(f) {
            if (f === "" || f === null) {
                return '';
            }
            var fecha = new Date(f);
            fecha.setMinutes(fecha.getMinutes() + fecha.getTimezoneOffset())
            return fecha;
            var x = f.split('-');
            console.log(f + ' -> ' + x[2] + '/' + x[1] + '/' + x[0]);
            return x[2] + '/' + x[1] + '/' + x[0];
        }

        function setPapas() {
            //scope.estudio.sueldo_papa = 0;
            //scope.estudio.sueldo_mama = 0;
            if (scope.estudio.padres.length > 0) {
              for (var i = 0; i < scope.estudio.padres.length; i++) {
                if (scope.estudio.padres[i].tipo_persona === "PAPA") {
                  scope.estudio.id_papa = scope.estudio.padres[i].id_padre_familia;
                  scope.estudio.nombre_papa = scope.estudio.padres[i].nombre;
                  scope.estudio.apellido_paterno_papa =
                    scope.estudio.padres[i].apellido_paterno;
                  scope.estudio.apellido_materno_papa =
                    scope.estudio.padres[i].apellido_materno;
                  scope.estudio.fecha_nacimiento_papa = fecha(
                    scope.estudio.padres[i].fecha_nacimiento
                  );
                  scope.estudio.correo_papa = scope.estudio.padres[i].correo;
                  scope.estudio.rfc_papa = scope.estudio.padres[i].rfc;
                  scope.estudio.celular_papa = scope.estudio.padres[i].celular;
                  scope.estudio.profesion_papa_list =
                    scope.estudio.padres[i].profesion_list;
                  scope.estudio.ocupacion_papa_list =
                    scope.estudio.padres[i].ocupacion_list;
                  scope.estudio.empresa_papa = scope.estudio.padres[i].empresa;
                  scope.estudio.puesto_papa = scope.estudio.padres[i].puesto;
                  scope.estudio.giro_papa_list = scope.estudio.padres[i].giro_list;
                  scope.estudio.papa_aporta_ingreso_familiar =
                    scope.estudio.padres[i].aporta_ingreso_familiar;
                  scope.estudio.dueno_papa = scope.estudio.padres[i].dueno;
                  scope.estudio.antiguedad_papa = scope.estudio.padres[i].antiguedad;
                  scope.estudio.sueldo_papa = parseFloat(
                    scope.estudio.padres[i].sueldo_neto
                  );
                  scope.estudio.sueldo_bruto_papa = parseFloat(
                    scope.estudio.padres[i].sueldo_bruto
                  );
                  scope.estudio.tel_oficina_papa =
                    scope.estudio.padres[i].telefono_oficina;
                  if (scope.estudio.padres[i].finado === "1") {
                    scope.estudio.finado_papa = true;
                  } else {
                    scope.estudio.finado_papa = false;
                  }
                  if (scope.estudio.padres[i].depende_ingreso === "1") {
                    scope.estudio.depende_ingreso_papa = true;
                  } else {
                    scope.estudio.depende_ingreso_papa = false;
                  }
                }
                if (scope.estudio.padres[i].tipo_persona === "MAMA") {
                  scope.estudio.id_mama = scope.estudio.padres[i].id_padre_familia;
                  scope.estudio.nombre_mama = scope.estudio.padres[i].nombre;
                  scope.estudio.apellido_paterno_mama =
                    scope.estudio.padres[i].apellido_paterno;
                  scope.estudio.apellido_materno_mama =
                    scope.estudio.padres[i].apellido_materno;
                  scope.estudio.fecha_nacimiento_mama = fecha(
                    scope.estudio.padres[i].fecha_nacimiento
                  );
                  scope.estudio.correo_mama = scope.estudio.padres[i].correo;
                  scope.estudio.rfc_mama = scope.estudio.padres[i].rfc;
                  scope.estudio.celular_mama = scope.estudio.padres[i].celular;
                  scope.estudio.profesion_mama_list =
                    scope.estudio.padres[i].profesion_list;
                  scope.estudio.ocupacion_mama_list =
                    scope.estudio.padres[i].ocupacion_list;
                  scope.estudio.empresa_mama = scope.estudio.padres[i].empresa;
                  scope.estudio.puesto_mama = scope.estudio.padres[i].puesto;
                  scope.estudio.giro_mama_list = scope.estudio.padres[i].giro_list;
                  scope.estudio.mama_aporta_ingreso_familiar =
                    scope.estudio.padres[i].aporta_ingreso_familiar;
                  scope.estudio.dueno_mama = scope.estudio.padres[i].dueno;
                  scope.estudio.antiguedad_mama = scope.estudio.padres[i].antiguedad;
                  scope.estudio.sueldo_mama = parseFloat(
                    scope.estudio.padres[i].sueldo_neto
                  );
                  scope.estudio.sueldo_bruto_mama = parseFloat(
                    scope.estudio.padres[i].sueldo_bruto
                  );
                  scope.estudio.tel_oficina_mama =
                    scope.estudio.padres[i].telefono_oficina;
                  if (scope.estudio.padres[i].finado === "1") {
                    scope.estudio.finado_mama = true;
                  } else {
                    scope.estudio.finado_mama = false;
                  }
                  if (scope.estudio.padres[i].depende_ingreso === "1") {
                    scope.estudio.depende_ingreso_mama = true;
                  } else {
                    scope.estudio.depende_ingreso_mama = false;
                  }
                }
              }
            }
          }

        scope.move = move;

        function move(index, item) {
            scope.comentarios.splice(index, 1);

            for (var i = 0; i < scope.comentarios.length; i++) {
                scope.comentarios[i].posicion = i;
            }

            EstudiosService.actualizarComentarios(scope.comentarios).then(
                function (response) {

                },
                function (error) {
                    console.log('Error: ' + error);
                }
            );
        }

        scope.familia = {};
        scope.guardarDatosGenerales = guardarDatosGenerales;
        function guardarDatosGenerales() {
            FamiliaService.actualizarFamilia(scope.familia).then(
                function (response) {
                    //console.log(response);
                    var obj = {};
                    obj.id_estudio = scope.estudio.id_estudio;
                    obj.guardo_datos = '1';
                    return EstudiosService.actualizarEstudio(obj);
                },
                function (e) {
                    mensaje('error', 'Aviso.', 'Ocurrio un error al guardar la información, intente mas tarde.');
                }).then(
                    function (response) {
                        //console.log(response);
                        return EstudiosService.obtenerFamilia(scope.familia.id_familia);
                    },
                    function (e) {
                        mensaje('error', 'Aviso.', 'Ocurrio un error al guardar la información, intente mas tarde.');
                    }).then(
                        function (response) {
                            //console.log(response);
                            mensaje('success', 'Aviso.', 'Se ha guardado la información correctamente.');
                            scope.familia = response.data;
                            scope.load();
                        },
                        function (e) {
                            mensaje('error', 'Aviso.', 'Ocurrio un error al guardar la información, intente mas tarde.');
                        });;

        }

        scope.guardarAutoCaptura = guardarAutoCaptura;
        function guardarAutoCaptura() {
            //actualizar ingresos_egresos
            scope.calcula();
            scope.ingresos.id_estudio = scope.estudio.id_estudio;
            scope.ingresos.folio_estudio = scope.estudio.folio_estudio;
            scope.ingresos.id_familia = scope.estudio.id_familia
            FamiliaService.actualizarIngresos(scope.ingresos).then(function (data) {
                console.log('guardo ingre');
                calcula();
                calculaEgresos();
                calculaEgresosServicios();
                calculaEgresosTotal();
                scope.egresos.id_estudio = scope.estudio.id_estudio;
                scope.egresos.folio_estudio = scope.estudio.folio_estudio;
                scope.egresos.id_familia = scope.estudio.id_familia;
                return FamiliaService.actualizarEgresos(scope.egresos);

            },
                function (error) {
                    console.log('Error actualizar papa: ' + error);
                }
            ).then(function (data) {
                console.log('guardo egre');
                var obj = {};
                obj.id_estudio = scope.estudio.id_estudio;
                obj.id_estatus_estudio = 11;
                obj.guardo_captura = '1';
                return EstudiosService.actualizarEstudio(obj);
            }, function (error) {
                console.log('Error actualizar papa: ' + error);
            }
            ).then(function (data) {
                console.log('updateo estudio');
                mensaje('success', 'Aviso.', 'Se guardaron los datos correctamente.');
                scope.load();

            }, function (error) {
                console.log('Error actualizar papa: ' + error);
            }
            );

        }

        scope.finalizarAutoCaptura = finalizarAutoCaptura;

        function finalizarAutoCaptura(ev) {

            var quest = '¿Desea continuar?';
            var confirm = DialogService.dialogConfirm(ev, 'Enviar estudio', quest, 'Si', 'No');
            $mdDialog.show(confirm).then(function () {
                show();
                scope.calcula();
                scope.ingresos.id_estudio = scope.estudio.id_estudio;
                scope.ingresos.folio_estudio = scope.estudio.folio_estudio;
                scope.ingresos.id_familia = scope.estudio.id_familia
                FamiliaService.actualizarIngresos(scope.ingresos).then(function (data) {
                    console.log(data);
                    calcula();
                    calculaEgresos();
                    calculaEgresosServicios();
                    calculaEgresosTotal();
                    scope.egresos.id_estudio = scope.estudio.id_estudio;
                    scope.egresos.folio_estudio = scope.estudio.folio_estudio;
                    scope.egresos.id_familia = scope.estudio.id_familia;
                    return FamiliaService.actualizarEgresos(scope.egresos);

                },
                    function (error) {
                        console.log('Error actualizar papa: ' + error);
                    }
                ).then(function (data) {
                    console.log(data);
                    var obj = {};
                    obj.id_estudio = scope.estudio.id_estudio;
                    obj.id_estatus_estudio = 12;
                    obj.finalizo_captura = '1';
                    return EstudiosService.actualizarEstudio(obj);
                }, function (error) {
                    console.log('Error actualizar papa: ' + error);
                }
                ).then(function (data) {
                    console.log(data);
                    return AutoCapturaService.generarCaratula(scope.estudio.id_estudio);
                }, function (error) {
                    console.log('Error actualizar papa: ' + error);
                }
                ).then(function (data) {
                    console.log(data);
                    mensaje('success', 'Aviso.', 'Se guardaron los datos correctamente. Gracias por su participación.');
                    scope.init();
                }, function (error) {
                    console.log('Error actualizar papa: ' + error);
                }
                );

            }, function () {
                $mdDialog.hide();
            });

        }

        scope.guardoDependientes = false;
        scope.validaCompletos = validaCompletos;

        scope.guardoMotivo = false;
        scope.guardoVehiculos = false;
        scope.guardoPropiedades = false;
        scope.guardoIngresos = false;
        scope.guardoEgresos = false;
        scope.guardoAutocaptura = false;
        scope.envioAutoCaptura = false;
        scope.guardo_casa_habitacion = false;

        function validaCompletos() {
            if (scope.estudio.guardo_datos === '1') {
                scope.guardoDatos = true;
            } else {
                scope.guardoDatos = false;
            }
            if (scope.estudio.padres.length > 0) {
                scope.guardoPapaMama = true;
            } else {
                scope.guardoPapaMama = false;
            }
            if (scope.estudio.hijos.length > 0) {
                scope.guardoHijos = true;
            } else {
                scope.guardoHijos = false;
            }
            if (scope.estudio.dependientes.length > 0) {
                scope.guardoDependientes = true;
            } else {
                scope.guardoDependientes = false;
            }
            if (scope.estudio.motivos.length > 0) {
                scope.guardoMotivo = true;
            } else {
                scope.guardoMotivo = false;
            }
            scope.guardo_casa_habitacion = false;
            if (scope.estudio.propiedades.length > 0) {
                scope.guardoPropiedades = true;
                var hab = 0;
                angular.forEach(scope.estudio.propiedades, function (value, key) {
                    if (value.casa_habitacion === '1') {
                        hab++;
                    }
                });
                if (hab > 0) {
                    scope.guardo_casa_habitacion = true;
                } else {
                    scope.guardo_casa_habitacion = false;
                }
            } else {
                scope.guardoPropiedades = false;
                scope.guardo_casa_habitacion = false;
            }
            if (scope.estudio.vehiculos.length > 0) {
                scope.guardoVehiculos = true;
            } else {
                scope.guardoVehiculos = false;
            }
            if (scope.estudio.guardo_ingresos === '1') {
                scope.guardoIngresos = true;
            } else {
                scope.guardoIngresos = false;
            }
            if (scope.estudio.guardo_egresos === '1') {
                scope.guardoEgresos = true;
            } else {
                scope.guardoEgresos = false;
            }

            if (scope.estudio.guardo_captura === '1') {
                scope.guardoAutocaptura = true;
            } else {
                scope.guardoAutocaptura = false;
            }
        }

        scope.listaInstuciones = [];

        function load() {
            //scope.estudio.sueldo_papa = 0;
            //scope.estudio.sueldo_mama = 0;
            EstudiosService.obtenerDetalleEstudio(EstudiosService.idEstudioSeleccionado, '0').then(
                function (response) {
                    EstudiosService.obtenerEmpleados().then(
                        function (response) {
                            scope.listaEmpleado = response.data;
                        },
                        function (err) {
                            console.log('Error obtenerEmpleados' + err);
                        }
                    );
                    EstudiosService.obtenerEstatusCat().then(
                        function (response) {
                            scope.listaEstatus = response.data;
                        },
                        function (err) {
                            console.log('Error al obtener cat estatus' + err);
                        }
                    );

                    GradoService.getAll().then(
                        function (response) {
                            scope.listaGrados = response.data;
                        },
                        function (err) {
                            console.log('Error al obtener grados' + err);
                        }
                    );

                    scope.estudio = response.data;
                    if (scope.id_grupo === '') {
                        scope.id_grupo = scope.estudio.id_grupo;
                    }

                    AdminService.getInstitucionListGrupo(scope.grupo.id_institucion).then(
                        function (response) {
                            scope.listaInstuciones = response.data;
                            if (scope.listaInstuciones.length == 0) {
                                var id = 0;
                                if ([null, undefined].includes(scope.grupo.id_institucion)) {
                                    id = scope.id_grupo;
                                } else {
                                    id = scope.grupo.id_institucion;
                                }
                                AdminService.getInstitucionListGrupo(id).then(
                                    function (response) {
                                        scope.listaInstuciones = response.data;
                                        scope.setHijos();
                                        scope.estudio.hijos.forEach(element => {
                                            if (!scope.validarEntero(element.institucion)) {
                                                var obj = {
                                                    id_hijo_familia: element.id_hijo_familia,
                                                    nombre_institucion: element.institucion
                                                };
                                                scope.listaInstuciones.push(obj);
                                            }
                                        });
                                    },
                                    function (err) {
                                        console.log('Error al obtener listaInstuciones' + err);
                                    }
                                );
                            }
                            scope.estudio.hijos.forEach(element => {
                                if (!scope.validarEntero(element.institucion)) {
                                    var obj = {
                                        id_hijo_familia: element.id_hijo_familia,
                                        nombre_institucion: element.institucion
                                    };
                                    scope.listaInstuciones.push(obj);
                                }
                            });
                        },
                        function (err) {
                            console.log('Error al obtener listaInstuciones' + err);
                        }
                    );

                    scope.estudio.hijos.forEach(element => {
                        if (scope.validarEntero(element.institucion)) {
                            console.log(scope.validarEntero(element.institucion));
                            var data = {};
                            if (scope.id_grupo === scope.GRUPO_ITER) {
                                data = {
                                    clave_institucion: element.institucion,
                                    id_grupo: scope.id_grupo
                                };
                            }
                            GradoService.getListaGradosByClaveIns(data).then(
                                function (response) {
                                    response.data.forEach(element => {
                                        scope.listaGrados.push(element);
                                    });
                                },
                                function (err) {
                                    console.log('Error al obtener grados' + err);
                                }
                            );
                        } else {
                            var obj = {
                                id_hijo_familia: element.id_hijo_familia,
                                nombre_institucion: element.institucion
                            };
                            scope.listaGrados.push(obj);
                        }

                    });




                    scope.familia = scope.estudio.familia;

                    if (scope.estudio.guardo_datos === '1') {
                        scope.guardoDatos = true;
                    }

                    if (response.data.evaluacion.length > 0) {
                        scope.evaluacion = scope.estudio.evaluacion[0];
                    }

                    if (response.data.documentos.length > 0) {
                        scope.documentos = scope.estudio.documentos[0];
                    }

                    setPapas();
                    scope.estudio.fecha_nacimiento_papa = fecha(scope.estudio.fecha_nacimiento_papa);
                    scope.estudio.fecha_nacimiento_mama = fecha(scope.estudio.fecha_nacimiento_mama);
                    scope.urlPapa = "api/storage/fotos/" + scope.estudio.foto_papa;
                    scope.urlMama = "api/storage/fotos/" + scope.estudio.foto_mama;
                    

                    scope.estudio.sueldo_papa = parseFloat(scope.estudio.sueldo_papa);
                    scope.estudio.sueldo_mama = parseFloat(scope.estudio.sueldo_mama);
                    scope.ingresos.sueldo_papa = scope.estudio.sueldo_papa;
                    scope.ingresos.sueldo_mama = scope.estudio.sueldo_mama;

                    scope.models.lists.A = scope.estudio.comentarios;
                    scope.comentarios = scope.estudio.comentarios;

                    scope.updateCals();

                    if (scope.estudio.filtro_uno === '1') {
                        scope.filtros.filtro_uno = true;
                    }

                    if (scope.estudio.filtro_dos === '1') {
                        scope.filtros.filtro_dos = true;
                    }

                    if (scope.estudio.filtro_tres === '1') {
                        scope.filtros.filtro_tres = true;
                    }

                    if (response.data.padres.length > 0) {
                        scope.guardoPapaMama = true;
                    }

                    if (response.data.ingresos.length > 0) {
                        scope.ingresos = response.data.ingresos[0];
                        parseIngresos();
                        calcula();
                    }

                    if (response.data.egresos.length > 0) {
                        scope.egresos = response.data.egresos[0];
                        parseEgresos();
                    }
                    if (response.data.documentos.length > 0) {
                        setBool();
                    }

                    //calcula();
                    calculaEgresos();
                    calculaEgresosServicios();
                    calculaEgresosTotal();
                    scope.egresos.id_estudio = scope.estudio.id_estudio;
                    scope.egresos.folio_estudio = scope.estudio.folio_estudio;
                    scope.egresos.id_familia = scope.estudio.id_familia;
                    if (scope.egresos.id_egreso_familia !== undefined) {
                        var promesas = [];
                        promesas.push(FamiliaService.actualizarEgresos(scope.egresos));
                        if (promesas.length === 1) {
                            $q.all(promesas).then(
                                function (response) {
                                    //console.log(response);
                                    scope.egresos = response[0].data[0];
                                    parseEgresos();
                                    //mensaje('success', 'Aviso.', 'Se actualizaron los datos de los egresos correctamente.');
                                },
                                function (error) {
                                    console.log('Error update ingresos: ' + error);
                                }
                            );
                        }
                    } else {
                        var promesas = [];
                        promesas.push(FamiliaService.guardarEgresos(scope.egresos));
                        if (promesas.length === 1) {
                            $q.all(promesas).then(
                                function (response) {
                                    //console.log(response);
                                    scope.egresos = response[0].data[0];
                                    parseEgresos();
                                    //mensaje('success', 'Aviso.', 'Se guardaron los datos de los egresos correctamente.');
                                },
                                function (error) {
                                    console.log('Error update ingresos: ' + error);
                                }
                            );
                        }
                    }

                    //scope.calcula();
                    scope.ingresos.id_estudio = scope.estudio.id_estudio;
                    scope.ingresos.folio_estudio = scope.estudio.folio_estudio;
                    scope.ingresos.id_familia = scope.estudio.id_familia;

                    if (scope.ingresos.id_ingreso_familia !== undefined) {
                        var promesas = [];
                        promesas.push(FamiliaService.actualizarIngresos(scope.ingresos));
                        if (promesas.length === 1) {
                            $q.all(promesas).then(
                                function (response) {
                                    //console.log(response);
                                    scope.ingresos = response[0].data[0];
                                    parseIngresos();
                                    //mensaje('success', 'Aviso.', 'Se actualizaron los datos de los ingresos correctamente.');
                                },
                                function (error) {
                                    console.log('Error update ingresos: ' + error);
                                }
                            );
                        }
                    } else {
                        var promesas = [];
                        promesas.push(FamiliaService.guardarIngresos(scope.ingresos));
                        if (promesas.length === 1) {
                            $q.all(promesas).then(
                                function (response) {
                                    //console.log(response);
                                    scope.ingresos = response[0].data[0];
                                    parseIngresos();
                                    //mensaje('success', 'Aviso.', 'Se guardaron los datos de los ingresos correctamente.');
                                },
                                function (error) {
                                    console.log('Error guardar ingresos: ' + error);
                                }
                            );
                        }
                    }

                    scope.validaCompletos();
                    hide();
                },
                function (error1) {
                    console.log('Error al obtener el detalle: ' + error1);
                    //error();
                }
            );
        }//end load

        function verActualizarHijo(hijo) {
            scope.banderaActualizar = true;
            scope.hijo = hijo;
            scope.hijo.colegiatura_pasado = parseFloat(scope.hijo.colegiatura_pasado);
            scope.hijo.colegiatura_actual = parseFloat(scope.hijo.colegiatura_actual);
            scope.hijo.apoyo_solicitado = parseFloat(scope.hijo.apoyo_solicitado);
            AdminService.getInstitucionListGrupo(scope.id_grupo).then(
                function (response) {
                    scope.listaInstuciones = response.data;
                    scope.getGrados(scope.hijo.institucion)
                    scope.setColegaturaGrado();
                    $("#modal_agregar_hijo").modal('show');
                },
                function (err) {
                    console.log('Error al obtener listaInstuciones' + err);
                }
            );
        }

        function agregarHijo(idFamilia) {
            AdminService.getInstitucionListGrupo(scope.id_grupo).then(
                function (response) {
                    scope.listaInstuciones = response.data;
                },
                function (err) {
                    console.log('Error al obtener listaInstuciones' + err);
                }
            );
            //$("myFormHijo").reset();
            scope.hijo = {};
            scope.hijo.colegiatura_pasado = parseFloat('0');
            scope.hijo.colegiatura_actual = parseFloat('0');
            scope.hijo.apoyo_solicitado = parseFloat('0');
            scope.hijo.solicita_beca = '';
            scope.hijo.es_estudiante = '';
            scope.hijo.institucion_diferente = '';
            scope.banderaActualizar = false;
            scope.hijo.id_familia = idFamilia;
            $("#modal_agregar_hijo").modal('show');
        }

        function eliminarHijo(hijo) {
            HijoService.eliminarHijo(hijo).then(
                function (response) {
                    scope.estudio.hijos = response.data;
                    mensaje('success', 'Aviso.', 'Se eliminó al hijo seleccionado correctamente.');
                    scope.hijo = {};
                    scope.banderaActualizar = false;
                    scope.calculaEgresos();
                    scope.updateCals();
                },
                function (error) {
                    console.log('Error al guardar hijo: ' + error);
                }
            );
        }

        function guardarHijo() {
            scope.hijo.id_estudio = scope.estudio.id_estudio;
            scope.hijo.folio_estudio = scope.estudio.folio_estudio;

            if (scope.hijo.institucion_diferente === '1') {
                scope.hijo.no_insigna = 1;
            } else {
                scope.hijo.no_insigna = 0;
            }
            if (scope.banderaActualizar) {
                if ($rootScope.isLocal) {
                    scope.hijo.sych = 1;
                    scope.hijo.updated = 1;
                }

                HijoService.actualizarHijo(scope.hijo).then(
                    function (response) {
                        scope.estudio.hijos = response.data;
                        mensaje('success', 'Aviso.', 'Se actualizaron los datos del hijo correctamente.');
                        $("#modal_agregar_hijo").modal('hide');
                        scope.hijo = {};
                        scope.banderaActualizar = false;
                        scope.calculaEgresos();
                        scope.updateCals();
                    },
                    function (error) {
                        console.log('Error al guardar hijo: ' + error);
                    }
                );

                return;
            }
            if ($rootScope.isLocal) {
                scope.hijo.sych = 1;
                scope.hijo.inserted = 1;
            }

            HijoService.guardarHijo(scope.hijo).then(
                function (response) {
                    scope.estudio.hijos = response.data;
                    mensaje('success', 'Aviso.', 'Se guardo al hijo correctamente.');
                    $("#modal_agregar_hijo").modal('hide');
                    scope.hijo = {};
                    scope.banderaActualizar = false;
                    scope.calculaEgresos();
                    scope.updateCals();
                },
                function (error) {
                    console.log('Error al guardar hijo: ' + error);
                }
            );

        }

        /*Dependiente*/
        function verActualizarDependiente(dependiente) {
            scope.banderaActualizar = true;
            scope.dependiente = dependiente;
            console.log("fecha: " + scope.dependiente.fecha_nacimiento);
            $("#modal_agregar_dependiente").modal('show');
        }

        function agregarDependiente(idFamilia) {
            scope.dependiente = {};
            scope.banderaActualizar = false;
            scope.dependiente.id_familia = idFamilia;
            $("#modal_agregar_dependiente").modal('show');
        }

        function eliminarDependiente(dependiente) {
            DependienteService.eliminarDependiente(dependiente).then(
                function (response) {
                    scope.estudio.dependientes = response.data;
                    mensaje('success', 'Aviso.', 'Se eliminó al dependiente seleccionado correctamente.');
                    scope.dependiente = {};
                    scope.banderaActualizar = false;
                },
                function (error) {
                    console.log('Error al guardar hijo: ' + error);
                }
            );
        }

        function guardarDependiente() {
            scope.dependiente.id_estudio = scope.estudio.id_estudio;
            scope.dependiente.folio_estudio = scope.estudio.folio_estudio;
            if ($rootScope.isLocal) {
                scope.dependiente.sych = 1;
            }
            if (scope.banderaActualizar) {

                DependienteService.actualizarDependiente(scope.dependiente).then(
                    function (response) {
                        scope.estudio.dependientes = response.data;
                        mensaje('success', 'Aviso.', 'Se actualizaron los datos del dependiente correctamente.');
                        $("#modal_agregar_dependiente").modal('hide');
                        scope.dependiente = {};
                        scope.banderaActualizar = false;
                    },
                    function (error) {
                        console.log('Error al guardar hijo: ' + error);
                    }
                );

                return;
            }

            DependienteService.guardarDependiente(scope.dependiente).then(
                function (response) {
                    scope.estudio.dependientes = response.data;
                    mensaje('success', 'Aviso.', 'Se guardo al dependiente correctamente.');
                    $("#modal_agregar_dependiente").modal('hide');
                    scope.dependiente = {};
                    scope.banderaActualizar = false;
                },
                function (error) {
                    console.log('Error al guardar hijo: ' + error);
                }
            );

            scope.validaCompletos();
        }

        /*motivo*/
        function verActualizarMotivo(motivo) {
            scope.banderaActualizar = true;
            scope.motivo = motivo;
            $("#modal_agregar_motivo").modal('show');
        }

        function agregarMotivo(idFamilia) {
            scope.motivo = {};
            scope.banderaActualizar = false;
            scope.motivo.id_familia = idFamilia;
            $("#modal_agregar_motivo").modal('show');
        }

        function eliminarMotivo(motivo) {
            MotivoService.eliminarMotivo(motivo).then(
                function (response) {
                    scope.estudio.motivos = response.data;
                    mensaje('success', 'Aviso.', 'Se eliminó el motivo seleccionado correctamente.');
                    scope.motivo = {};
                    scope.banderaActualizar = false;
                },
                function (error) {
                    console.log('Error al guardar hijo: ' + error);
                }
            );

        }

        function guardarMotivo() {
            scope.motivo.id_estudio = scope.estudio.id_estudio;
            scope.motivo.folio_estudio = scope.estudio.folio_estudio;
            if ($rootScope.isLocal) {
                scope.motivo.sych = 1;
            }
            if (scope.banderaActualizar) {

                MotivoService.actualizarMotivo(scope.motivo).then(
                    function (response) {
                        scope.estudio.dependientes = response.data;
                        mensaje('success', 'Aviso.', 'Se actualizaron los datos del motivo correctamente.');
                        $("#modal_agregar_motivo").modal('hide');
                        scope.motivo = {};
                        scope.banderaActualizar = false;
                    },
                    function (error) {
                        console.log('Error al guardar hijo: ' + error);
                    }
                );

                return;
            }

            MotivoService.guardarMotivo(scope.motivo).then(
                function (response) {
                    scope.estudio.motivos = response.data;
                    mensaje('success', 'Aviso.', 'Se guardo el motivo correctamente.');
                    $("#modal_agregar_motivo").modal('hide');
                    scope.dependiente = {};
                    scope.banderaActualizar = false;
                },
                function (error) {
                    console.log('Error al guardar hijo: ' + error);
                }
            );

            scope.validaCompletos();
        }

        function asignarShow() {
            scope.empleadoAsignado.id_usuario_asignado = scope.estudio.id_usuario_asignado;
            $("#modal_asignar").modal('show');
        }
        ;

        function cambiarEstatusView() {
            $("#modal_cambiar_status").modal('show');
        }
        ;

        function asignarEmpleado(ev) {
            scope.empleadoAsignado.id_estudio = scope.estudio.id_estudio;
            scope.empleadoAsignado.id_usuario_asigno = $localStorage.globals.id;
            scope.empleadoAsignado.id_estatus_estudio = '2';
            console.log(scope.empleadoAsignado);
            $("#modal_asignar").modal('hide');

            var quest = '¿Desea continuar?';
            var confirm = DialogService.dialogConfirm(ev, '¿Asignar usuario?', quest, 'Si', 'No');
            $mdDialog.show(confirm).then(function () {
                EstudiosService.guardarAsignado(scope.empleadoAsignado).then(
                    function (response) {
                        scope.estudio.id_usuario_asignado = scope.empleadoAsignado.id_usuario_asignado;
                        scope.estudio.id_estatus_estudio = scope.empleadoAsignado.id_estatus_estudio;
                        mensaje('success', 'Aviso.', 'Se asigno el estudio al usuario correctamente.');
                        $("#modal_asignar").modal('hide');
                        scope.empleadoAsignado = {};
                    },
                    function (error) {
                        console.log('Error al guardar hijo: ' + error);
                    }
                );

            }, function () {
                $mdDialog.hide();
            });

        }
        ;

        function cambiarEstatus() {
            var obj = {};
            obj.id_estudio = scope.estudio.id_estudio;
            obj.id_estatus_estudio = scope.estudio.id_estatus_estudio;

            EstudiosService.actualizarEstudio(obj).then(
                function (response) {
                    mensaje('success', 'Aviso.', 'Se cambio el estatus del estudio correctamente.');
                    $("#modal_cambiar_status").modal('hide');
                },
                function (error) {
                    console.log('Error al guardar hijo: ' + error);
                }
            );
        }
        ;

        scope.guardarPapaMama = guardarPapaMama;

        function guardarPapaMama() {
            scope.guardarPapa();
        }

        function guardarPapa() {
            scope.setSave();
            FamiliaService.actualizarFamilia(scope.familia).then(
              function (response) {
                //console.log(response);
                var obj = {};
                obj.id_estudio = scope.estudio.id_estudio;
                obj.guardo_datos = "1";
              },
              function (e) {
                mensaje(
                  "error",
                  "Aviso.",
                  "Ocurrio un error al guardar la información, intente mas tarde."
                );
              }
            );
      
            if (isNaN(scope.estudio.sueldo_papa)) {
              //mensaje('error', 'Aviso.', 'El sueldo del padre o tutor debe ser mayor a 0.');
              scope.estudio.sueldo_papa = 0;
              //scope.setSave();
              //return false;
            }
      
            if (scope.estudio.id_papa !== undefined) {
              console.log("Actualiza papa");
            } else {
              if (scope.estudio.id_papa === undefined) {
                console.log("guarda papa");
              }
            }
            if (scope.estudio.id_papa !== undefined) {
              var obj = {};
              obj.id_padre_familia = scope.estudio.id_papa;
              obj.id_familia = scope.estudio.id_familia;
              obj.id_estudio = scope.estudio.id_estudio;
              obj.nombre = scope.estudio.nombre_papa;
              obj.apellido_paterno = scope.estudio.apellido_paterno_papa;
              obj.apellido_materno = scope.estudio.apellido_materno_papa;
              obj.edad = scope.estudio.edad_papa;
              obj.correo = scope.estudio.correo_papa;
              obj.rfc = scope.estudio.rfc_papa;
              obj.celular = scope.estudio.celular_papa;
              obj.profesion_list = scope.estudio.profesion_papa_list;
              obj.ocupacion_list = scope.estudio.ocupacion_papa_list;
              obj.empresa = scope.estudio.empresa_papa;
              obj.puesto = scope.estudio.puesto_papa;
              obj.giro_list = scope.estudio.giro_papa_list;
              obj.dueno = scope.estudio.dueno_papa;
              obj.antiguedad = scope.estudio.antiguedad_papa;
              obj.sueldo_neto = scope.estudio.sueldo_papa;
              obj.sueldo_bruto = scope.estudio.sueldo_bruto_papa;
              obj.aporta_ingreso_familiar =
                scope.estudio.papa_aporta_ingreso_familiar;
      
              obj.fecha_nacimiento = scope.estudio.fecha_nacimiento_papa;
              obj.finado = scope.estudio.finado_papa;
              obj.depende_ingreso = scope.estudio.depende_ingreso_papa;
              obj.telefono_oficina = scope.estudio.tel_oficina_papa;
              if ($rootScope.isLocal) {
                obj.sych = 1;
                obj.updated = 1;
              }
      
              var promesas = [];
              promesas.push(FamiliaService.actualizarPapa(obj));
              if (promesas.length === 1) {
                $q.all(promesas)
                  .then(
                    function (response) {
                      scope.estudio.padres = response[0].data;
                      scope.calcula();
                      scope.ingresos.id_estudio = scope.estudio.id_estudio;
                      scope.ingresos.folio_estudio = scope.estudio.folio_estudio;
                      scope.ingresos.id_familia = scope.estudio.id_familia;
                      if ($rootScope.isLocal) {
                        scope.ingresos.sych = 1;
                      }
                      return FamiliaService.actualizarIngresos(scope.ingresos);
                    },
                    function (error) {
                      console.log("Error actualizar papa: " + error);
                    }
                  )
                  .then(function (data) {
                    console.log("guardo ingr");
                    scope.ingresos = data.data[0];
                    parseIngresos();
      
                    //mensaje('success', 'Aviso.', 'Se guardaron los datos de los ingresos correctamente.');
      
                    calcula();
                    calculaEgresos();
                    calculaEgresosServicios();
                    calculaEgresosTotal();
                    scope.egresos.id_estudio = scope.estudio.id_estudio;
                    scope.egresos.folio_estudio = scope.estudio.folio_estudio;
                    scope.egresos.id_familia = scope.estudio.id_familia;
                    if ($rootScope.isLocal) {
                      scope.egresos.sych = 1;
                    }
                    return FamiliaService.actualizarEgresos(scope.egresos);
                  })
                  .then(function (data) {
                    console.log("guardo egre");
                    parseIngresos();
                    //mensaje('success', 'Aviso.', 'Se actualizaron los datos del papa correctamente.');
                    scope.guardarMama();
                  });
              }
            } else {
              if (scope.estudio.id_papa === undefined) {
                var obj = {};
                obj.id_familia = scope.estudio.id_familia;
                obj.id_estudio = scope.estudio.id_estudio;
                obj.nombre = scope.estudio.nombre_papa;
                obj.apellido_paterno = scope.estudio.apellido_paterno_papa;
                obj.apellido_materno = scope.estudio.apellido_materno_papa;
                obj.edad = scope.estudio.edad_papa;
                obj.correo = scope.estudio.correo_papa;
                obj.rfc = scope.estudio.rfc_papa;
                obj.celular = scope.estudio.celular_papa;
                obj.profesion_list = scope.estudio.profesion_papa_list;
                obj.ocupacion_list = scope.estudio.ocupacion_papa_list;
                obj.empresa = scope.estudio.empresa_papa;
                obj.puesto = scope.estudio.puesto_papa;
                obj.giro_list = scope.estudio.giro_papa_list;
                obj.dueno = scope.estudio.dueno_papa;
                obj.antiguedad = scope.estudio.antiguedad_papa;
                obj.sueldo_neto = scope.estudio.sueldo_papa;
                obj.sueldo_bruto = scope.estudio.sueldo_bruto_papa;
                obj.aporta_ingreso_familiar =
                  scope.estudio.papa_aporta_ingreso_familiar;
                obj.tipo_persona = "PAPA";
                obj.fecha_nacimiento = scope.estudio.fecha_nacimiento_papa;
                obj.finado = scope.estudio.finado_papa;
                obj.depende_ingreso = scope.estudio.depende_ingreso_papa;
                obj.telefono_oficina = scope.estudio.tel_oficina_papa;
                if ($rootScope.isLocal) {
                  obj.sych = 1;
                  obj.inserted = 1;
                }
                console.log(obj);
                var promesas = [];
                promesas.push(FamiliaService.guardarPapa(obj));
                if (promesas.length === 1) {
                  $q.all(promesas).then(
                    function (response) {
                      scope.estudio.padres = response[0].data;
                      setPapas();
                      scope.estudio.sueldo_papa = parseFloat(
                        scope.estudio.sueldo_papa
                      );
                      scope.estudio.sueldo_mama = parseFloat(
                        scope.estudio.sueldo_mama
                      );
                      scope.ingresos.sueldo_papa = scope.estudio.sueldo_papa;
                      scope.ingresos.sueldo_mama = scope.estudio.sueldo_mama;
                      calcula();
                      calculaEgresos();
                      //mensaje('success', 'Aviso.', 'Se guardaron los datos del papa correctamente.');
                      scope.guardarMama();
                    },
                    function (error) {
                      console.log("Error guardar papa: " + error);
                    }
                  );
                }
              }
            }
          }
      
          function guardarMama() {
            if (isNaN(scope.estudio.sueldo_mama)) {
              //mensaje('error', 'Aviso.', 'El sueldo del padre o tutor debe ser mayor a 0.');
              scope.estudio.sueldo_mama = 0;
              //scope.setSave();
              //return false;
            }
            if (scope.estudio.id_mama !== undefined) {
              var obj = {};
              obj.id_padre_familia = scope.estudio.id_mama;
              obj.id_familia = scope.estudio.id_familia;
              obj.id_estudio = scope.estudio.id_estudio;
              obj.nombre = scope.estudio.nombre_mama;
              obj.apellido_paterno = scope.estudio.apellido_paterno_mama;
              obj.apellido_materno = scope.estudio.apellido_materno_mama;
              obj.edad = scope.estudio.edad_mama;
              obj.correo = scope.estudio.correo_mama;
              obj.rfc = scope.estudio.rfc_mama;
              obj.celular = scope.estudio.celular_mama;
              obj.profesion_list = scope.estudio.profesion_mama_list;
              obj.ocupacion_list = scope.estudio.ocupacion_mama_list;
              obj.empresa = scope.estudio.empresa_mama;
              obj.puesto = scope.estudio.puesto_mama;
              obj.giro_list = scope.estudio.giro_mama_list;
              obj.dueno = scope.estudio.dueno_mama;
              obj.antiguedad = scope.estudio.antiguedad_mama;
              obj.sueldo_neto = scope.estudio.sueldo_mama;
              obj.sueldo_bruto = scope.estudio.sueldo_bruto_mama;
              obj.aporta_ingreso_familiar =
                scope.estudio.mama_aporta_ingreso_familiar;
              obj.fecha_nacimiento = scope.estudio.fecha_nacimiento_mama;
              obj.finado = scope.estudio.finado_mama;
              obj.depende_ingreso = scope.estudio.depende_ingreso_mama;
              obj.telefono_oficina = scope.estudio.tel_oficina_mama;
              if ($rootScope.isLocal) {
                obj.sych = 1;
                obj.updated = 1;
              }
      
              FamiliaService.actualizarPapa(obj)
                .then(
                  function (response) {
                    scope.estudio.padres = response.data;
                    setPapas();
                    scope.estudio.sueldo_papa = parseFloat(scope.estudio.sueldo_papa);
                    scope.estudio.sueldo_mama = parseFloat(scope.estudio.sueldo_mama);
                    scope.ingresos.sueldo_papa = scope.estudio.sueldo_papa;
                    scope.ingresos.sueldo_mama = scope.estudio.sueldo_mama;
                    scope.calcula();
                    scope.ingresos.id_estudio = scope.estudio.id_estudio;
                    scope.ingresos.folio_estudio = scope.estudio.folio_estudio;
                    scope.ingresos.id_familia = scope.estudio.id_familia;
                    if ($rootScope.isLocal) {
                      scope.ingresos.sych = 1;
                    }
                    return FamiliaService.actualizarIngresos(scope.ingresos);
                  },
                  function (error) {
                    console.log("Error actualizar papa: " + error);
                  }
                )
                .then(function (data) {
                  console.log("guardo ingr");
                  scope.ingresos = data.data[0];
                  parseIngresos();
                  //mensaje('success', 'Aviso.', 'Se guardaron los datos de los ingresos correctamente.');
                  calcula();
                  calculaEgresos();
                  calculaEgresosServicios();
                  calculaEgresosTotal();
                  scope.egresos.id_estudio = scope.estudio.id_estudio;
                  scope.egresos.folio_estudio = scope.estudio.folio_estudio;
                  scope.egresos.id_familia = scope.estudio.id_familia;
                  if ($rootScope.isLocal) {
                    scope.egresos.sych = 1;
                  }
                  return FamiliaService.actualizarEgresos(scope.egresos);
                })
                .then(function (data) {
                  console.log("guardo egre");
                  parseIngresos();
                  mensaje(
                    "success",
                    "Aviso.",
                    "Se guardaron los datos correctamente."
                  );
                  scope.guardoPapaMama = true;
                  scope.setSave();
                });
            } else {
              var obj = {};
              obj.id_familia = scope.estudio.id_familia;
              obj.id_estudio = scope.estudio.id_estudio;
              obj.nombre = scope.estudio.nombre_mama;
              obj.apellido_paterno = scope.estudio.apellido_paterno_mama;
              obj.apellido_materno = scope.estudio.apellido_materno_mama;
              obj.edad = scope.estudio.edad_mama;
              obj.correo = scope.estudio.correo_mama;
              obj.rfc = scope.estudio.rfc_mama;
              obj.celular = scope.estudio.celular_mama;
              obj.profesion_list = scope.estudio.profesion_mama_list;
              obj.ocupacion_list = scope.estudio.ocupacion_mama_list;
              obj.empresa = scope.estudio.empresa_mama;
              obj.puesto = scope.estudio.puesto_mama;
              obj.giro_list = scope.estudio.giro_mama_list;
              obj.dueno = scope.estudio.dueno_mama;
              obj.antiguedad = scope.estudio.antiguedad_mama;
              obj.sueldo_neto = scope.estudio.sueldo_mama;
              obj.sueldo_bruto = scope.estudio.sueldo_bruto_mama;
              obj.aporta_ingreso_familiar =
                scope.estudio.mama_aporta_ingreso_familiar;
              obj.tipo_persona = "MAMA";
              obj.fecha_nacimiento = scope.estudio.fecha_nacimiento_mama;
              obj.depende_ingreso = scope.estudio.depende_ingreso_mama;
              obj.finado = scope.estudio.finado_mama;
              obj.telefono_oficina = scope.estudio.tel_oficina_mama;
              if ($rootScope.isLocal) {
                obj.sych = 1;
                obj.inserted = 1;
              }
              console.log(obj);
      
              FamiliaService.guardarPapa(obj).then(
                function (response) {
                  scope.estudio.padres = response.data;
                  setPapas();
                  scope.estudio.sueldo_papa = parseFloat(scope.estudio.sueldo_papa);
                  scope.estudio.sueldo_mama = parseFloat(scope.estudio.sueldo_mama);
                  scope.ingresos.sueldo_papa = scope.estudio.sueldo_papa;
                  scope.ingresos.sueldo_mama = scope.estudio.sueldo_mama;
                  calcula();
                  calculaEgresos();
                  mensaje(
                    "success",
                    "Aviso.",
                    "Se guardaron los datos correctamente."
                  );
                  scope.guardoPapaMama = true;
                  scope.setSave();
                },
                function (error) {
                  console.log("Error al guardar papa: " + error);
                }
              );
            }
          }


        /*vehiculos*/
        function verActualizarVehiculo(vehiculo) {
            scope.banderaActualizar = true;
            scope.vehiculo = vehiculo;

            if (scope.vehiculo.no_auto === '1') {
                scope.no_auto = true;
            } else {
                scope.no_auto = false;
            }
            console.log(scope.no_auto);
            scope.vehiculo.valor_comercial_actual = parseFloat(scope.vehiculo.valor_comercial_actual);
            scope.vehiculo.cantidad_adeuda = parseFloat(scope.vehiculo.cantidad_adeuda);
            scope.vehiculo.pago_mensual = parseFloat(scope.vehiculo.pago_mensual);
            $("#modal_agregar_vehiculo").modal('show');
        }

        function agregarVehiculo(idFamilia) {
            scope.vehiculo = {};
            scope.banderaActualizar = false;
            scope.vehiculo.id_familia = idFamilia;
            scope.vehiculo.valor_comercial_actual = parseFloat('0');
            scope.vehiculo.cantidad_adeuda = parseFloat('0');
            scope.vehiculo.pago_mensual = parseFloat('0');
            $("#modal_agregar_vehiculo").modal('show');
        }

        function eliminarVehiculo(vehiculo) {
            VehiculoService.eliminarVehiculo(vehiculo).then(
                function (response) {
                    scope.estudio.vehiculos = response.data;
                    mensaje('success', 'Aviso.', 'Se eliminó el vehiculo seleccionado correctamente.');
                    scope.vehiculo = {};
                    scope.banderaActualizar = false;
                    scope.calculaEgresos();
                },
                function (error) {
                    console.log('Error al guardar hijo: ' + error);
                }
            );
        }

        function guardarVehiculo() {
            scope.vehiculo.id_estudio = scope.estudio.id_estudio;
            scope.vehiculo.folio_estudio = scope.estudio.folio_estudio;
            if (scope.no_auto) {
                scope.vehiculo.no_auto = 1;
            } else {
                scope.vehiculo.no_auto = 0;
            }
            if ($rootScope.isLocal) {
                scope.vehiculo.sych = 1;
            }
            if (scope.banderaActualizar) {

                VehiculoService.actualizarVehiculo(scope.vehiculo).then(
                    function (response) {
                        scope.estudio.vehiculos = response.data;
                        mensaje('success', 'Aviso.', 'Se actualizaron los datos del vehiculo correctamente.');
                        $("#modal_agregar_vehiculo").modal('hide');
                        scope.vehiculo = {};
                        scope.banderaActualizar = false;
                        scope.calculaEgresos();
                    },
                    function (error) {
                        console.log('Error al guardar hijo: ' + error);
                    }
                );

                return;
            }

            VehiculoService.guardarVehiculo(scope.vehiculo).then(
                function (response) {
                    scope.estudio.vehiculos = response.data;
                    mensaje('success', 'Aviso.', 'Se guardo el vehiculo correctamente.');
                    $("#modal_agregar_vehiculo").modal('hide');
                    scope.vehiculo = {};
                    scope.banderaActualizar = false;
                    scope.calculaEgresos();
                },
                function (error) {
                    console.log('Error al guardar hijo: ' + error);
                }
            );


        }

        /*propiedad*/
        function verActualizarPropiedad(propiedad) {
            scope.banderaActualizar = true;
            scope.propiedad = propiedad;
            $("#modal_agregar_propiedad").modal('show');
        }

        function agregarPropiedad(idFamilia) {
            scope.propiedad = {};
            if (scope.guardo_casa_habitacion) {
                scope.propiedad.casa_habitacion = '0';
            }
            scope.banderaActualizar = false;
            scope.propiedad.id_familia = idFamilia;
            scope.propiedad.monto_renta = 0;
            $("#modal_agregar_propiedad").modal('show');
        }

        function eliminarPropiedad(propiedad) {
            PropiedadService.eliminarPropiedad(propiedad).then(
                function (response) {
                    scope.estudio.propiedades = response.data;
                    mensaje('success', 'Aviso.', 'Se eliminó el propiedad seleccionado correctamente.');
                    scope.propiedad = {};
                    scope.banderaActualizar = false;
                    scope.calculaEgresos();
                    scope.validaCompletos();
                },
                function (error) {
                    console.log('Error al guardar hijo: ' + error);
                }
            );
        }

        function guardarPropiedad() {
            scope.propiedad.id_estudio = scope.estudio.id_estudio;
            scope.propiedad.folio_estudio = scope.estudio.folio_estudio;
            if ($rootScope.isLocal) {
                scope.propiedad.sych = 1;
            }
            if (scope.banderaActualizar) {

                PropiedadService.actualizarPropiedad(scope.propiedad).then(
                    function (response) {
                        scope.estudio.propiedades = response.data;
                        mensaje('success', 'Aviso.', 'Se actualizaron los datos del propiedad correctamente.');
                        $("#modal_agregar_propiedad").modal('hide');
                        scope.propiedad = {};
                        scope.banderaActualizar = false;
                        scope.calculaEgresos();
                        scope.validaCompletos();
                    },
                    function (error) {
                        console.log('Error al guardar hijo: ' + error);
                    }
                );

                return;
            }

            PropiedadService.guardarPropiedad(scope.propiedad).then(
                function (response) {
                    scope.estudio.propiedades = response.data;
                    mensaje('success', 'Aviso.', 'Se guardo el propiedad correctamente.');
                    $("#modal_agregar_propiedad").modal('hide');
                    scope.propiedad = {};
                    scope.banderaActualizar = false;
                    scope.calculaEgresos();
                    scope.validaCompletos();
                },
                function (error) {
                    console.log('Error al guardar hijo: ' + error);
                }
            );


        }

        function guardarIngresos() {
            console.log(scope.ingresos);
            scope.calcula();
            scope.ingresos.id_estudio = scope.estudio.id_estudio;
            scope.ingresos.folio_estudio = scope.estudio.folio_estudio;
            scope.ingresos.id_familia = scope.estudio.id_familia;
            if ($rootScope.isLocal) {
                scope.ingresos.sych = 1;
            }

            if (scope.ingresos.id_ingreso_familia !== undefined) {
                var promesas = [];
                promesas.push(FamiliaService.actualizarIngresos(scope.ingresos));
                if (promesas.length === 1) {
                    $q.all(promesas).then(
                        function (response) {
                            console.log(response);
                            scope.ingresos = response[0].data[0];
                            parseIngresos();
                            var obj = {};
                            obj.id_estudio = scope.estudio.id_estudio;
                            obj.guardo_ingresos = '1';
                            var promesas = [];
                            promesas.push(EstudiosService.actualizarEstudio(obj));
                            $q.all(promesas).then(
                                function (response) {
                                    mensaje('success', 'Aviso.', 'Se guardaron los datos correctamente.');
                                    scope.load();
                                }
                            );
                        },
                        function (error) {
                            console.log('Error update ingresos: ' + error);
                        }
                    );
                }
            } else {
                var promesas = [];
                promesas.push(FamiliaService.guardarIngresos(scope.ingresos));
                if (promesas.length === 1) {
                    $q.all(promesas).then(
                        function (response) {
                            console.log(response);
                            scope.ingresos = response[0].data[0];
                            parseIngresos();

                            var obj = {};
                            obj.id_estudio = scope.estudio.id_estudio;
                            obj.guardo_ingresos = '1';
                            var promesas = [];
                            promesas.push(EstudiosService.actualizarEstudio(obj));
                            $q.all(promesas).then(
                                function (response) {
                                    mensaje('success', 'Aviso.', 'Se guardaron los datos correctamente.');
                                    scope.load();
                                }
                            );
                        },
                        function (error) {
                            console.log('Error guardar ingresos: ' + error);
                        }
                    );
                }
            }


        }

        function guardarEgresos() {
            console.log(scope.egresos);
            calcula();
            calculaEgresos();
            calculaEgresosServicios();
            calculaEgresosTotal();
            scope.egresos.id_estudio = scope.estudio.id_estudio;
            scope.egresos.folio_estudio = scope.estudio.folio_estudio;
            scope.egresos.id_familia = scope.estudio.id_familia;
            if ($rootScope.isLocal) {
                scope.egresos.sych = 1;
            }
            if (scope.egresos.id_egreso_familia !== undefined) {
                var promesas = [];
                promesas.push(FamiliaService.actualizarEgresos(scope.egresos));
                if (promesas.length === 1) {
                    $q.all(promesas).then(
                        function (response) {
                            console.log(response);
                            scope.egresos = response[0].data[0];
                            parseEgresos();
                            var obj = {};
                            obj.id_estudio = scope.estudio.id_estudio;
                            obj.guardo_egresos = '1';
                            var promesas = [];
                            promesas.push(EstudiosService.actualizarEstudio(obj));
                            $q.all(promesas).then(
                                function (response) {
                                    mensaje('success', 'Aviso.', 'Se guardaron los datos correctamente.');
                                    scope.load();
                                }
                            );
                        },
                        function (error) {
                            console.log('Error update ingresos: ' + error);
                        }
                    );
                }
            } else {
                var promesas = [];
                promesas.push(FamiliaService.guardarEgresos(scope.egresos));
                if (promesas.length === 1) {
                    $q.all(promesas).then(
                        function (response) {
                            console.log(response);
                            scope.egresos = response[0].data[0];
                            parseEgresos();
                            var obj = {};
                            obj.id_estudio = scope.estudio.id_estudio;
                            obj.guardo_egresos = '1';
                            var promesas = [];
                            promesas.push(EstudiosService.actualizarEstudio(obj));
                            $q.all(promesas).then(
                                function (response) {
                                    mensaje('success', 'Aviso.', 'Se guardaron los datos correctamente.');
                                    scope.load();
                                }
                            );
                        },
                        function (error) {
                            console.log('Error update ingresos: ' + error);
                        }
                    );
                }
            }


        }


        /*pasivo*/
    scope.verActualizarPasivo = verActualizarPasivo;
    function verActualizarPasivo(pasivo) {
      scope.banderaActualizar = true;
      scope.pasivo = pasivo;
      $("#modal_agregar_pasivo").modal("show");
    }

    scope.agregarPasivo = agregarPasivo;
    function agregarPasivo(idFamilia) {
      scope.pasivo = {};
      scope.banderaActualizar = false;
      scope.pasivo.id_familia = idFamilia;
      $("#modal_agregar_pasivo").modal("show");
    }

    scope.eliminarPasivo = eliminarPasivo;
    function eliminarPasivo(pasivo) {
      confirmaMsj(
        "Confirmación de solicitud",
        "¿Eliminar pasivo?",
        "Si",
        function () {
          FamiliaService.eliminarPasivo(pasivo).then(
            function (response) {
              scope.estudio.pasivos = response.data;
              mensaje(
                "success",
                "Aviso.",
                "Se eliminó el pasivo seleccionado correctamente."
              );
              scope.pasivo = {};
              scope.banderaActualizar = false;
            },
            function (error) {
              console.log("Error al guardar pasivo: " + error);
            }
          );
        },
        "No",
        function () {}
      );
    }

    scope.guardarPasivo = guardarPasivo;
    function guardarPasivo() {
      scope.setSave();
      scope.pasivo.id_estudio = scope.estudio.id_estudio;
      if (scope.banderaActualizar) {
        FamiliaService.actualizarPasivo(scope.pasivo).then(
          function (response) {
            scope.estudio.pasivos = response.data;
            mensaje(
              "success",
              "Aviso.",
              "Se actualizaron los datos del pasivo correctamente."
            );
            $("#modal_agregar_pasivo").modal("hide");
            scope.pasivo = {};
            scope.banderaActualizar = false;
            scope.setSave();
          },
          function (error) {
            console.log("Error al guardar pasivo: " + error);
          }
        );

        return;
      }

      FamiliaService.guardarPasivo(scope.pasivo).then(
        function (response) {
          scope.estudio.pasivos = response.data;
          mensaje("success", "Aviso.", "Se guardo el pasivo correctamente.");
          $("#modal_agregar_pasivo").modal("hide");
          scope.dependiente = {};
          scope.banderaActualizar = false;
          scope.setSave();
        },
        function (error) {
          console.log("Error al guardar pasivo: " + error);
        }
      );

      scope.validaCompletos();
    }


        function guardarDocumentos() {
            scope.documentos.id_estudio = scope.estudio.id_estudio;
            scope.documentos.folio_estudio = scope.estudio.folio_estudio;
            scope.documentos.id_familia = scope.estudio.id_familia;
            if ($rootScope.isLocal) {
                scope.documentos.sych = 1;
            }
            console.log(scope.documentos);

            if (scope.documentos.id_documento_familia !== undefined) {
                FamiliaService.actualizarDocumentos(scope.documentos).then(
                    function (response) {
                        scope.documentos = response.data[0];
                        setBool();
                        mensaje('success', 'Aviso.', 'Se actualizaron los datos de los documentos correctamente.');

                    },
                    function (error) {
                        console.log('Error al guardar hijo: ' + error);
                    }
                );
            } else {
                FamiliaService.guardarDocumentos(scope.documentos).then(
                    function (response) {
                        scope.documentos = response.data[0];
                        setBool();
                        mensaje('success', 'Aviso.', 'Se guardaron los datos de los documentos correctamente.');

                    },
                    function (error) {
                        console.log('Error al guardar hijo: ' + error);
                    }
                );
            }

        }

        function guardarEvaluacion() {
            if (scope.evaluacion.apreciacion === undefined
                || scope.evaluacion.discrepancia === undefined
                || scope.evaluacion.apreciacion === ''
                || scope.evaluacion.discrepancia === ''
                || scope.evaluacion.apreciacion === null
                || scope.evaluacion.discrepancia === null) {
                mensaje('alert', 'Campos obligatorios', 'Por favor complete los campos.');
                return;
            }
            scope.evaluacion.id_estudio = scope.estudio.id_estudio;
            scope.evaluacion.folio_estudio = scope.estudio.folio_estudio;
            scope.evaluacion.id_familia = scope.estudio.id_familia;
            if ($rootScope.isLocal) {
                scope.evaluacion.sych = 1;
            }
            console.log(scope.evaluacion);

            if (scope.evaluacion.id_evaluacion_familia !== undefined) {
                FamiliaService.actualizarEvaluacion(scope.evaluacion).then(
                    function (response) {
                        scope.evaluacion = response.data[0];
                        mensaje('success', 'Aviso.', 'Se actualizaron los datos de la evaluación correctamente.');

                    },
                    function (error) {
                        console.log('Error al guardar hijo: ' + error);
                    }
                );
            } else {
                FamiliaService.guardarEvaluacion(scope.evaluacion).then(
                    function (response) {
                        scope.evaluacion = response.data[0];
                        mensaje('success', 'Aviso.', 'Se guardaron los datos de la evaluación correctamente.');

                    },
                    function (error) {
                        console.log('Error al guardar hijo: ' + error);
                    }
                );
            }


        }

        /*comentario*/
        function verActualizarComentario(comentario) {
            scope.banderaActualizar = true;
            scope.comentario = comentario;
            $("#modal_agregar_comentario").modal('show');
        }

        function agregarComentario(idFamilia) {
            scope.comentario = {};
            scope.banderaActualizar = false;
            scope.comentario.id_familia = idFamilia;
            $("#modal_agregar_comentario").modal('show');
        }

        function eliminarComentario(comentario) {
            ComentarioService.eliminarComentario(comentario).then(
                function (response) {
                    scope.estudio.comentarios = response.data;
                    mensaje('success', 'Aviso.', 'Se eliminó el comentario seleccionado correctamente.');
                    scope.comentario = {};
                    scope.banderaActualizar = false;
                },
                function (error) {
                    console.log('Error al guardar hijo: ' + error);
                }
            );

        }

        function guardarComentario() {
            console.log(scope.comentario);
            scope.comentario.id_estudio = scope.estudio.id_estudio;
            scope.comentario.folio_estudio = scope.estudio.folio_estudio;
            if ($rootScope.isLocal) {
                scope.comentario.sych = 1;
            }
            if (scope.banderaActualizar) {
                delete scope.comentario.tipo;

                ComentarioService.actualizarComentario(scope.comentario).then(
                    function (response) {
                        scope.estudio.comentarios = response.data;
                        mensaje('success', 'Aviso.', 'Se actualizaron los datos del comentario correctamente.');
                        $("#modal_agregar_comentario").modal('hide');
                        scope.comentario = {};
                        scope.banderaActualizar = false;
                    },
                    function (error) {
                        console.log('Error al guardar hijo: ' + error);
                    }
                );

                return;
            }
            scope.comentario.tipo = 'GENERAL';

            ComentarioService.guardarComentario(scope.comentario).then(
                function (response) {
                    scope.estudio.comentarios = response.data;
                    mensaje('success', 'Aviso.', 'Se guardo el comentario correctamente.');
                    $("#modal_agregar_comentario").modal('hide');
                    scope.comentario = {};
                    scope.banderaActualizar = false;
                },
                function (error) {
                    console.log('Error al guardar hijo: ' + error);
                }
            );


        }

        /*FUNCIONES COMUNES*/
        function setSueldo() {
            scope.ingresos.sueldo_papa = scope.estudio.sueldo_papa;
            scope.ingresos.sueldo_mama = scope.estudio.sueldo_mama;
        }

        /*scope.find = find;
        function find(hijo) {
            if (scope.listaInstuciones.filter(i => i.id_institucion == hijo.institucion).length == 1) {
                return scope.listaInstuciones.filter(i => i.id_institucion == hijo.institucion)[0].nombre_institucion;
            }else{
                return '-';
            }
        }*/

        scope.setHijos = setHijos;
        function setHijos() {
            scope.estudio.hijos.forEach(element => {
                element = scope.find(element)
                console.log(element);
            });
        }

        function calcula() {
            if (scope.id_grupo === scope.GRUPO_INSIGNA) {
                var suma = 0;
                var suma2 = 0;
                console.log(scope.estudio.sueldo_papa);
                console.log(scope.estudio.sueldo_mama);
                scope.ingresos.sueldo_papa = scope.estudio.sueldo_papa;
                scope.ingresos.sueldo_mama = scope.estudio.sueldo_mama;
                suma += scope.ingresos.sueldo_papa;
                suma += scope.ingresos.sueldo_mama;
                suma2 += scope.ingresos.ingreso_otros_miembros;
                suma2 += scope.ingresos.ingreso_renta;
                suma2 += scope.ingresos.ingreso_honorarios;
                suma2 += scope.ingresos.ingreso_inversiones;
                suma2 += scope.ingresos.ingreso_pensiones;
                suma2 += scope.ingresos.ingreso_ventas;
                suma2 += scope.ingresos.otros_ingresos;
                //scope.ingresos.total_otros_ingresos = suma;
                //scope.ingresos.total_otros_ingresos = scope.ingresos.otros_ingresos;
                scope.ingresos.total_otros_ingresos = suma2;
                var n = scope.estudio.dependientes.length + scope.estudio.hijos.length;
                // scope.ingresos.total_ingresos = scope.ingresos.total_otros_ingresos;
                scope.ingresos.total_ingresos = suma + scope.ingresos.total_otros_ingresos ;
                if (scope.estudio.finado_papa === false && scope.estudio.depende_ingreso_papa) {
                    //scope.ingresos.total_ingresos += scope.ingresos.sueldo_papa;
                    console.log('depende papa');
                    n++;
                } else {
                    //scope.ingresos.sueldo_papa = 0;
                }

                if (scope.estudio.finado_mama === false && scope.estudio.depende_ingreso_mama) {
                    //scope.ingresos.total_ingresos += scope.ingresos.sueldo_mama;
                    console.log('depende mama');
                    n++;
                } else {
                    //scope.ingresos.sueldo_mama = 0;
                }
                scope.estudio.total_depen = n;
                console.log(n);
                if (n > 0) {
                    scope.ingresos.ingreso_percapita = scope.ingresos.total_ingresos / n;
                } else {
                    if (n == 0) {
                        scope.ingresos.ingreso_percapita = scope.ingresos.total_ingresos / 1;
                    }
                }


                if (scope.ingresos.ingreso_percapita <= 10000) {
                    scope.ingresos.clasificacion = 'C';
                } else if (scope.ingresos.ingreso_percapita >= 20000) {
                    scope.ingresos.clasificacion = 'A';
                } else {
                    scope.ingresos.clasificacion = 'B';
                }
                console.log('Calculo ingresos');
                console.log(scope.ingresos);
            }

            if (scope.id_grupo === scope.GRUPO_ITER) {

                var suma = 0;
                console.log(scope.estudio.sueldo_papa);
                console.log(scope.estudio.sueldo_mama);
                scope.ingresos.sueldo_papa = scope.estudio.sueldo_papa;
                scope.ingresos.sueldo_mama = scope.estudio.sueldo_mama;
                suma += scope.ingresos.sueldo_papa;
                suma += scope.ingresos.sueldo_mama;
                suma += scope.ingresos.negocios_papa;
                suma += scope.ingresos.negocios_mama;
                suma += scope.ingresos.ingreso_renta;
                suma += scope.ingresos.ingreso_inversiones;
                suma += scope.ingresos.apoyo_familiar;
                suma += scope.ingresos.aguinaldo_papa;
                suma += scope.ingresos.aguinaldo_mama;
                suma += scope.ingresos.utilidades_papa;
                suma += scope.ingresos.utilidades_mama;
                suma += scope.ingresos.vales_papa;
                suma += scope.ingresos.vales_mama;
                suma += scope.ingresos.bono_mes_papa;
                suma += scope.ingresos.bono_mes_mama;
                suma += scope.ingresos.bono_anual_papa;
                suma += scope.ingresos.bono_anual_mama;
                suma += scope.ingresos.vacac_papa;
                suma += scope.ingresos.vacac_mama;
                suma += scope.ingresos.comisiones_papa;
                suma += scope.ingresos.comisiones_mama;
                suma += scope.ingresos.gratificacion_papa;
                suma += scope.ingresos.gratificacion_papa;
                suma += scope.ingresos.fda_papa;
                suma += scope.ingresos.fda_mama;
                suma += scope.ingresos.docencia_papa;
                suma += scope.ingresos.docencia_mama;
                suma += scope.ingresos.dev_impuestos;
                suma += scope.ingresos.otros_ingresos;
                //scope.ingresos.total_otros_ingresos = suma;

                var n = scope.estudio.dependientes.length + scope.estudio.hijos.length;
                scope.ingresos.total_ingresos = suma;

                if (scope.estudio.finado_papa === false && scope.estudio.depende_ingreso_papa) {
                    //scope.ingresos.total_ingresos += scope.ingresos.sueldo_papa;
                    n++;
                } else {
                    //scope.ingresos.sueldo_papa = 0;
                }

                if (scope.estudio.finado_mama === false && scope.estudio.depende_ingreso_mama) {
                    //scope.ingresos.total_ingresos += scope.ingresos.sueldo_mama;
                    n++;
                } else {
                    // scope.ingresos.sueldo_mama = 0;
                }

                scope.estudio.total_depen = n;
                scope.ingresos.ingreso_percapita = scope.ingresos.total_ingresos / n;

                if (scope.ingresos.ingreso_percapita <= 10000) {
                    scope.ingresos.clasificacion = 'C';
                } else if (scope.ingresos.ingreso_percapita >= 20000) {
                    scope.ingresos.clasificacion = 'A';
                } else {
                    scope.ingresos.clasificacion = 'B';
                }
                console.log('Calculo ingresos');
            }

        }

        function calculaEgresos() {
            var renta = 0;
            var hipoteca = 0;
            var cole = 0;
            var otrasCole = 0;
            var creditoAuto = 0;

            for (var i = 0; i < scope.estudio.propiedades.length; i++) {
                if (scope.estudio.propiedades[i].status === 'Rentada') {
                    renta += parseFloat(scope.estudio.propiedades[i].monto_renta);
                }
                if (scope.estudio.propiedades[i].status === 'Hipotecada') {
                    hipoteca += parseFloat(scope.estudio.propiedades[i].monto_renta);
                }
            }

            for (var i = 0; i < scope.estudio.hijos.length; i++) {
                cole += parseFloat(scope.estudio.hijos[i].colegiatura_actual);
                otrasCole += parseFloat(scope.estudio.hijos[i].otras_colegiaturas);
            }

            for (var i = 0; i < scope.estudio.vehiculos.length; i++) {
                creditoAuto += parseFloat(scope.estudio.vehiculos[i].pago_mensual);
            }

            scope.egresos.renta = renta;
            scope.egresos.credito_hipotecario = hipoteca;
            scope.egresos.colegiaturas = cole;
            //scope.egresos.otras_colegiaturas=otrasCole;
            scope.egresos.credito_auto = creditoAuto;
            scope.calculaEgresosTotal();
        }

        function calculaEgresosServicios() {
            //scope.egresos.total_servicios+=scope.egresos.clases_particulares;
            scope.egresos.total_servicios = 0;
            scope.egresos.total_servicios += parseFloat(scope.egresos.agua);
            scope.egresos.total_servicios += parseFloat(scope.egresos.luz);
            scope.egresos.total_servicios += parseFloat(scope.egresos.telefono);
            scope.egresos.total_servicios += parseFloat(scope.egresos.celulares);
            //scope.egresos.total_servicios+=scope.egresos.servicio_domestico;
            scope.egresos.total_servicios += scope.egresos.otros;
            scope.egresos.total_servicios += parseFloat(scope.egresos.gas);
        }

        function calculaEgresosTotal() {
            scope.egresos.total_egresos = 0;
            scope.egresos.total_egresos += parseFloat(scope.egresos.alimentacion_despensa);
            scope.egresos.total_egresos += parseFloat(scope.egresos.renta);
            scope.egresos.total_egresos += parseFloat(scope.egresos.credito_hipotecario);
            scope.egresos.total_egresos += parseFloat(scope.egresos.colegiaturas);
            scope.egresos.total_egresos += parseFloat(scope.egresos.otras_colegiaturas);
            scope.egresos.total_egresos += parseFloat(scope.egresos.clases_particulares);
            scope.egresos.total_egresos += parseFloat(scope.egresos.servicio_domestico);
            scope.egresos.total_egresos += parseFloat(scope.egresos.total_servicios);
            scope.egresos.total_egresos += parseFloat(scope.egresos.creditos_comerciales);
            //scope.egresos.total_egresos+=parseFloat(scope.egresos.otros);
            scope.egresos.total_egresos += parseFloat(scope.egresos.gasolina);
            scope.egresos.total_egresos += parseFloat(scope.egresos.credito_auto);
            scope.egresos.total_egresos += parseFloat(scope.egresos.pago_tdc_mensual);
            scope.egresos.total_egresos += parseFloat(scope.egresos.vestido_calzado);
            scope.egresos.total_egresos += parseFloat(scope.egresos.medico_medicinas);
            scope.egresos.total_egresos += parseFloat(scope.egresos.diversion_entretenimiento);
            scope.egresos.total_egresos += parseFloat(scope.egresos.clubes_deportivos);
            scope.egresos.total_egresos += parseFloat(scope.egresos.seguros);
            scope.egresos.total_egresos += parseFloat(scope.egresos.vacaciones);
            scope.egresos.total_egresos += parseFloat(scope.egresos.otros2);

            scope.egresos.total_egresos += parseFloat(scope.egresos.transporte_escolar);
            scope.egresos.total_egresos += parseFloat(scope.egresos.tarjeta_credito);
            scope.egresos.total_egresos += parseFloat(scope.egresos.credito_mobiliario_equipo);
            scope.egresos.total_egresos += parseFloat(scope.egresos.credito_familiar);
            scope.egresos.total_egresos += parseFloat(scope.egresos.credito_trabajo);
            scope.egresos.total_egresos += parseFloat(scope.egresos.credito_caja);
            scope.egresos.total_egresos += parseFloat(scope.egresos.telefono_movil);
            scope.egresos.total_egresos += parseFloat(scope.egresos.internet);
            scope.egresos.total_egresos += parseFloat(scope.egresos.cable);
            scope.egresos.total_egresos += parseFloat(scope.egresos.seguro_auto);
            scope.egresos.total_egresos += parseFloat(scope.egresos.seguro_retiro);
            scope.egresos.total_egresos += parseFloat(scope.egresos.seguro_vida);
            scope.egresos.total_egresos += parseFloat(scope.egresos.mant_fracc);
            scope.egresos.total_egresos += parseFloat(scope.egresos.mant_auto);
            scope.egresos.total_egresos += parseFloat(scope.egresos.mant_casa);
            scope.egresos.total_egresos += parseFloat(scope.egresos.reinscripciones);
            scope.egresos.total_egresos += parseFloat(scope.egresos.utiles_escolares);
            scope.egresos.total_egresos += parseFloat(scope.egresos.predial);
            scope.egresos.total_egresos += parseFloat(scope.egresos.apoyo_familiar);
            scope.egresos.total_egresos += parseFloat(scope.egresos.tenencia);
            scope.egresos.total_egresos += parseFloat(scope.egresos.gastos_contabilidad);


            scope.egresos.diferencia_egre_ingre = scope.ingresos.total_ingresos - scope.egresos.total_egresos;
        }

        function setBool() {
            if (scope.documentos.carta_no_adeudo === '1') {
                scope.documentos.carta_no_adeudo = true;
            }
            if (scope.documentos.carta_no_adeudo === '0') {
                scope.documentos.carta_no_adeudo = false;
            }

            if (scope.documentos.firma_reglamento === '1') {
                scope.documentos.firma_reglamento = true;
            }
            if (scope.documentos.firma_reglamento === '0') {
                scope.documentos.firma_reglamento = false;
            }

            if (scope.documentos.nomina_carta === '1') {
                scope.documentos.nomina_carta = true;
            }
            if (scope.documentos.nomina_carta === '0') {
                scope.documentos.nomina_carta = false;
            }

            if (scope.documentos.poliza === '1') {
                scope.documentos.poliza = true;
            }
            if (scope.documentos.poliza === '0') {
                scope.documentos.poliza = false;
            }

            if (scope.documentos.estado_cuenta === '1') {
                scope.documentos.estado_cuenta = true;
            }
            if (scope.documentos.estado_cuenta === '0') {
                scope.documentos.estado_cuenta = false;
            }

            if (scope.documentos.recibos_renta === '1') {
                scope.documentos.recibos_renta = true;
            }
            if (scope.documentos.recibos_renta === '0') {
                scope.documentos.recibos_renta = false;
            }

            if (scope.documentos.facturas_hospital === '1') {
                scope.documentos.facturas_hospital = true;
            }
            if (scope.documentos.facturas_hospital === '0') {
                scope.documentos.facturas_hospital = false;
            }

            if (scope.documentos.comprobante_finiquito === '1') {
                scope.documentos.comprobante_finiquito = true;
            }
            if (scope.documentos.comprobante_finiquito === '0') {
                scope.documentos.comprobante_finiquito = false;
            }

            if (scope.documentos.demandas_judiciales === '1') {
                scope.documentos.demandas_judiciales = true;
            }
            if (scope.documentos.demandas_judiciales === '0') {
                scope.documentos.demandas_judiciales = false;
            }

            if (scope.documentos.servicios === '1') {
                scope.documentos.servicios = true;
            }
            if (scope.documentos.servicios === '0') {
                scope.documentos.servicios = false;
            }

            if (scope.documentos.pagos_credito_hipo === '1') {
                scope.documentos.pagos_credito_hipo = true;
            }
            if (scope.documentos.pagos_credito_hipo === '0') {
                scope.documentos.pagos_credito_hipo = false;
            }

            if (scope.documentos.pagos_credito_auto === '1') {
                scope.documentos.pagos_credito_auto = true;
            }
            if (scope.documentos.pagos_credito_auto === '0') {
                scope.documentos.pagos_credito_auto = false;
            }

            if (scope.documentos.otros === '1') {
                scope.documentos.otros = true;
            }
            if (scope.documentos.otros === '0') {
                scope.documentos.otros = false;
            }
        }

        function parseIngresos() {
            scope.estudio.sueldo_papa = parseFloat(scope.ingresos.sueldo_papa);
            scope.estudio.sueldo_mama = parseFloat(scope.ingresos.sueldo_mama);
            scope.ingresos.ingreso_otros_miembros = parseFloat(scope.ingresos.ingreso_otros_miembros);
            scope.ingresos.ingreso_renta = parseFloat(scope.ingresos.ingreso_renta);
            scope.ingresos.ingreso_honorarios = parseFloat(scope.ingresos.ingreso_honorarios);
            scope.ingresos.ingreso_inversiones = parseFloat(scope.ingresos.ingreso_inversiones);
            scope.ingresos.ingreso_pensiones = parseFloat(scope.ingresos.ingreso_pensiones);
            scope.ingresos.ingreso_ventas = parseFloat(scope.ingresos.ingreso_ventas);
            scope.ingresos.otros_ingresos = parseFloat(scope.ingresos.otros_ingresos);
            scope.ingresos.total_otros_ingresos = parseFloat(scope.ingresos.total_otros_ingresos);
            scope.ingresos.sueldo_papa = parseFloat(scope.ingresos.sueldo_papa);
            scope.ingresos.sueldo_mama = parseFloat(scope.ingresos.sueldo_mama);
            scope.ingresos.ingreso_percapita = parseFloat(scope.ingresos.ingreso_percapita);
            scope.ingresos.total_ingresos = parseFloat(scope.ingresos.total_ingresos);
            scope.ingresos.num_personas_aportan = parseFloat(scope.ingresos.num_personas_aportan);
            scope.ingresos.negocios_papa = parseFloat(scope.ingresos.negocios_papa);
            scope.ingresos.negocios_mama = parseFloat(scope.ingresos.negocios_mama);
            scope.ingresos.apoyo_familiar = parseFloat(scope.ingresos.apoyo_familiar);
            scope.ingresos.aguinaldo_papa = parseFloat(scope.ingresos.aguinaldo_papa);
            scope.ingresos.aguinaldo_mama = parseFloat(scope.ingresos.aguinaldo_mama);
            scope.ingresos.utilidades_papa = parseFloat(scope.ingresos.utilidades_papa);
            scope.ingresos.utilidades_mama = parseFloat(scope.ingresos.utilidades_mama);
            scope.ingresos.vales_papa = parseFloat(scope.ingresos.vales_papa);
            scope.ingresos.vales_mama = parseFloat(scope.ingresos.vales_mama);
            scope.ingresos.bono_mes_papa = parseFloat(scope.ingresos.bono_mes_papa);
            scope.ingresos.bono_mes_mama = parseFloat(scope.ingresos.bono_mes_mama);
            scope.ingresos.bono_anual_papa = parseFloat(scope.ingresos.bono_anual_papa);
            scope.ingresos.bono_anual_mama = parseFloat(scope.ingresos.bono_anual_mama);
            scope.ingresos.vacac_papa = parseFloat(scope.ingresos.vacac_papa);
            scope.ingresos.vacac_mama = parseFloat(scope.ingresos.vacac_mama);
            scope.ingresos.comisiones_papa = parseFloat(scope.ingresos.comisiones_papa);
            scope.ingresos.comisiones_mama = parseFloat(scope.ingresos.comisiones_mama);
            scope.ingresos.gratificacion_papa = parseFloat(scope.ingresos.gratificacion_papa);
            scope.ingresos.gratificacion_mama = parseFloat(scope.ingresos.gratificacion_mama);
            scope.ingresos.fda_papa = parseFloat(scope.ingresos.fda_papa);
            scope.ingresos.fda_mama = parseFloat(scope.ingresos.fda_mama);
            scope.ingresos.docencia_papa = parseFloat(scope.ingresos.docencia_papa);
            scope.ingresos.docencia_mama = parseFloat(scope.ingresos.docencia_mama);
            scope.ingresos.dev_impuestos = parseFloat(scope.ingresos.dev_impuestos);
        }

        function parseEgresos() {
            scope.egresos.alimentacion_despensa = parseFloat(scope.egresos.alimentacion_despensa);
            scope.egresos.renta = parseFloat(scope.egresos.renta);
            scope.egresos.credito_hipotecario = parseFloat(scope.egresos.credito_hipotecario);
            scope.egresos.colegiaturas = parseFloat(scope.egresos.colegiaturas);
            scope.egresos.otras_colegiaturas = parseFloat(scope.egresos.otras_colegiaturas);
            scope.egresos.clases_particulares = parseFloat(scope.egresos.clases_particulares);
            scope.egresos.agua = parseFloat(scope.egresos.agua);
            scope.egresos.luz = parseFloat(scope.egresos.luz);
            scope.egresos.telefono = parseFloat(scope.egresos.telefono);
            scope.egresos.celulares = parseFloat(scope.egresos.celulares);
            scope.egresos.servicio_domestico = parseFloat(scope.egresos.servicio_domestico);
            scope.egresos.gas = parseFloat(scope.egresos.gas);
            scope.egresos.total_servicios = parseFloat(scope.egresos.total_servicios);
            scope.egresos.gasolina = parseFloat(scope.egresos.gasolina);
            scope.egresos.credito_auto = parseFloat(scope.egresos.credito_auto);
            scope.egresos.pago_tdc_mensual = parseFloat(scope.egresos.pago_tdc_mensual);
            scope.egresos.saldo_tdc = parseFloat(scope.egresos.saldo_tdc);
            scope.egresos.creditos_comerciales = parseFloat(scope.egresos.creditos_comerciales);
            scope.egresos.vestido_calzado = parseFloat(scope.egresos.vestido_calzado);
            scope.egresos.medico_medicinas = parseFloat(scope.egresos.medico_medicinas);
            scope.egresos.diversion_entretenimiento = parseFloat(scope.egresos.diversion_entretenimiento);
            scope.egresos.clubes_deportivos = parseFloat(scope.egresos.clubes_deportivos);
            scope.egresos.seguros = parseFloat(scope.egresos.seguros);
            scope.egresos.vacaciones = parseFloat(scope.egresos.vacaciones);
            scope.egresos.otros2 = parseFloat(scope.egresos.otros2);
            scope.egresos.otros = parseFloat(scope.egresos.otros);
            scope.egresos.total_egresos = parseFloat(scope.egresos.total_egresos);
            scope.egresos.diferencia_egre_ingre = parseFloat(scope.egresos.diferencia_egre_ingre);
            scope.egresos.transporte_escolar = parseFloat(scope.egresos.transporte_escolar);
            scope.egresos.tarjeta_credito = parseFloat(scope.egresos.tarjeta_credito);
            scope.egresos.credito_mobiliario_equipo = parseFloat(scope.egresos.credito_mobiliario_equipo);
            scope.egresos.credito_familiar = parseFloat(scope.egresos.credito_familiar);
            scope.egresos.credito_trabajo = parseFloat(scope.egresos.credito_trabajo);
            scope.egresos.credito_caja = parseFloat(scope.egresos.credito_caja);
            scope.egresos.telefono_movil = parseFloat(scope.egresos.telefono_movil);
            scope.egresos.internet = parseFloat(scope.egresos.internet);
            scope.egresos.cable = parseFloat(scope.egresos.cable);
            scope.egresos.seguro_auto = parseFloat(scope.egresos.seguro_auto);
            scope.egresos.seguro_retiro = parseFloat(scope.egresos.seguro_retiro);
            scope.egresos.seguro_vida = parseFloat(scope.egresos.seguro_vida);
            scope.egresos.mant_fracc = parseFloat(scope.egresos.mant_fracc);
            scope.egresos.mant_auto = parseFloat(scope.egresos.mant_auto);
            scope.egresos.mant_casa = parseFloat(scope.egresos.mant_casa);
            scope.egresos.reinscripciones = parseFloat(scope.egresos.reinscripciones);
            scope.egresos.utiles_escolares = parseFloat(scope.egresos.utiles_escolares);
            scope.egresos.predial = parseFloat(scope.egresos.predial);
            scope.egresos.apoyo_familiar = parseFloat(scope.egresos.apoyo_familiar);
            scope.egresos.tenencia = parseFloat(scope.egresos.tenencia);
            scope.egresos.gastos_contabilidad = parseFloat(scope.egresos.gastos_contabilidad);
            scope.egresos.celulares = parseFloat(scope.egresos.celulares);
        }



    };//end controller

})();