/**
 * Description:
 *     removes white space from text. useful for html values that cannot have spaces
 * Usage:
 *   {{some_text | nospace}}
 */
angular.module('app').filter('nospace', function () {
    return function (value) {
        return (!value) ? '' : value.replace(/ /g, '');
    };
}).filter('fecha', function () {
    return function (x) {
        var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        if(x !== null){
            var date = new Date(x);
            return date.getUTCDate() + "/" + meses[date.getUTCMonth()] + "/" + date.getUTCFullYear();
        }else{
            return "";
        }
        
    };
}).filter('toJSDate', function(){
    return function (dateString) {
        if(dateString===undefined||dateString===null)return;
        var y=dateString.split(' ');
        if(y[1]!==undefined){
            var x=y[0].split('-');
            var xx=y[1].split(':');
            //return x[2]+'/'+x[1]+'/'+x[0]+' '+xx[0]+':'+xx[1]
            //console.log("fecha: "+x[2]+'/'+x[1]+'/'+x[0]);;
            return x[2]+'/'+x[1]+'/'+x[0];
        }else{
            var x=dateString.split('-');
            
            //console.log("fecha: "+x[2]+'/'+x[1]+'/'+x[0]);
            return x[2]+'/'+x[1]+'/'+x[0];
        }
    };
}).filter('toTime', function(){
    return function (time) {
        var date = new Date();
        console.log(time);
        return new Date().toString("hh:mm tt");
    };
}).filter('estatusEstudio', function(){
    return function (status) {
        switch (status){
            case '1':
                return '<span class="label label-danger">Solicitado</span>';
        }
    };
}).filter('tipoAdmin', function(){
    return function (tipo) {
        switch (tipo){
            case '1':
                return 'Administrador local';
            case '2':
                return 'Solicitante';
        }
    };
}).filter('tipoAdminWise', function(){
    return function (tipo) {
        switch (tipo){
            case '1':
                return 'Administrador';
            case '2':
                return 'Captura';
        }
    };
}).filter('estatusUsuario', function(){
    return function (tipo) {
        switch (tipo){
            case '1':
                return 'Activo';
            case '0':
                return 'Inactivo';
        }
    };
});
angular
        .module('app')
        .filter('startFrom', 
        function() {
            return function(input, start) {
                    start = +start; //parse to int
                    return input.slice(start);
                };
            });
/* Diretivas */
angular.module('app')
.directive("resize", function () {
    return function (scope, element, attrs) {
        element.bind("mousedown", function () {
            scope.resize(element);
            scope.$apply();
        });
    };
})
        
.directive("remove", function () {
    return function (scope, element, attrs) {
        element.bind("mousedown", function () {
            scope.remove(element);
            scope.$apply();
        });
    };
})
       
.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}])
    
.directive('pwCheck', [function () {
    return {
      require: 'ngModel',
      link: function (scope, elem, attrs, ctrl) {
        var firstPassword = '#' + attrs.pwCheck;
        elem.add(firstPassword).on('keyup', function () {
          scope.$apply(function () {
            var v = elem.val()===$(firstPassword).val();
            ctrl.$setValidity('pwmatch', v);
          });
        });
      }
    }
  }])

  .directive('fileChange', ['$parse', function($parse) {

    return {
      require: 'ngModel',
      restrict: 'A',
      link: function ($scope, element, attrs, ngModel) {
        // Get the function provided in the file-change attribute.
        // Note the attribute has become an angular expression,
        // which is what we are parsing. The provided handler is 
        // wrapped up in an outer function (attrHandler) - we'll 
        // call the provided event handler inside the handler()
        // function below.
        var attrHandler = $parse(attrs['fileChange']);

        // This is a wrapper handler which will be attached to the
        // HTML change event.
        var handler = function (e) {

          $scope.$apply(function () {

            // Execute the provided handler in the directive's scope.
            // The files variable will be available for consumption
            // by the event handler.
            attrHandler($scope, { $event: e, files: e.target.files });
          });
        };

        // Attach the handler to the HTML change event 
        element[0].addEventListener('change', handler, false);
      }
    }
}])

.directive('fileChangeMama', ['$parse', function($parse) {

    return {
      require: 'ngModel',
      restrict: 'A',
      link: function ($scope, element, attrs, ngModel) {
        // Get the function provided in the file-change attribute.
        // Note the attribute has become an angular expression,
        // which is what we are parsing. The provided handler is 
        // wrapped up in an outer function (attrHandler) - we'll 
        // call the provided event handler inside the handler()
        // function below.
        var attrHandler = $parse(attrs['fileChange']);

        // This is a wrapper handler which will be attached to the
        // HTML change event.
        var handler = function (e) {

          $scope.$apply(function () {

            // Execute the provided handler in the directive's scope.
            // The files variable will be available for consumption
            // by the event handler.
            attrHandler($scope, { $event: e, files: e.target.files });
          });
        };

        // Attach the handler to the HTML change event 
        element[0].addEventListener('change', handler, false);
      }
    }
}])

.directive('onlyNumbers', function () {
    return  {
        restrict: 'A',
        link: function (scope, elm, attrs, ctrl) {
            elm.on('keydown', function (event) {
                if(event.shiftKey || event.keyCode === 9){
                    //event.preventDefault(); 
                    return false;
                }

                if ([8, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {
                    // backspace, enter, escape, arrows
                    return true;
                } else if (event.which >= 48 && event.which <= 57) {
                    // numbers 0 to 9
                    return true;
                } else if (event.which >= 96 && event.which <= 105) {
                    // numpad number
                    return true;
                } 
                else {
                    //event.preventDefault();
                    return false;
                }
            });
        }
    }
})
;


