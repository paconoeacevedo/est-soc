(function () {
    'use strict';

    angular
        .module('app.authentication')
        .factory('AuthenticationService', AuthenticationService);

    function AuthenticationService($http, $cookies, RestService, $rootScope, $localStorage, Base64Service, Constants) {
        var service = {};

        service.SetCredentials = setCredential;
        service.ClearCredentials = clearCredential;
        service.isAuth = isAuth;
        service.Auth = false;
        return service;


        function setCredential(id, username, password, rols, type) {
            var authdata = Base64Service.encode(username + ':' + password);
            $localStorage.$default({
                globals: {
                    id: id,
                    username: username,
                    authdata: authdata,
                    role: rols,
                    type: type
                }
            });
            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
        }

        function clearCredential() {
            $localStorage.$reset();
            $http.defaults.headers.common.Authorization = 'Basic ';
        }

        function isAuth() {
            if ($cookies.get('auth') !== undefined) {
                if ($localStorage.globals !== undefined && $localStorage.data !== undefined) {
                    console.log('session true');
                    return true;
                } else {
                    console.log('session false');
                    return false;
                }
            }else{
                console.log('expires session');
                $localStorage.$reset();
                $http.defaults.headers.common.Authorization = 'Basic ';
                return false; 
            }
            
        }
    }

})();
