(function () {
    'use strict';

    angular
        .module('app.autocaptura')
        .factory('AutoCapturaService', AutoCapturaService);

    function AutoCapturaService($http, RestService, $localStorage, $rootScope, Base64Service, Constants, $q) {
        var service = {};
        service.user = {};
        service.institucion = {};
        service.getEstudioFromToken = getEstudioFromToken;
        service.getInstitucion = getInstitucion;
        service.updateUser = updateUser;
        service.loadUser = loadUser;
        service.checkUser = checkUser;
        service.generarCaratula = generarCaratula;
        return service;

        function loadUser() {
            if ($rootScope.isAuth) {
                console.log('cargando datos usuario');
                var type = $localStorage.globals.type;
                service.getUser().then(
                    function (response) {
                        service.user = $rootScope.user = response.data.usuario;
                        if (type === '2') {
                            service.institucion = $rootScope.institucion = response.data.institucion;
                        }
                        hide();
                    }, function (error) {
                        console.log('Error al guardar completar la solicitud: ' + error);
                    }
                );

            }
        }

        function getEstudioFromToken(token) {
            var url = Constants.BaseURLBack + '/estudio/getEstudioFromToken/' + token;
            return RestService.get(url, '');
        }

        function getInstitucion(id) {
            var url = Constants.BaseURLBack + '/user/getInstitucion/' + id;
            return RestService.get(url, '');
        }

        function updateUser(data) {
            var url = Constants.BaseURLBack + '/user/updateUser';
            return RestService.post(url, '', data);
        }

        function checkUser(data) {
            var url = Constants.BaseURLBack + '/user/checkUser';
            return RestService.post(url, '', data);
        }

        function generarCaratula(id) {
            var url = Constants.BaseURLBack + '/reporte/caratula/' + id;
            return RestService.get(url, '');
        }
    }

})();
