(function () { 
	'use strict';
	
        angular.module('app', [
            'ui.router',
            'ngMaterial', 
            'ngAnimate', 
            'ngStorage',
            'ngCookies',
            'angularMoment',
            'moment-picker',
            'datatables',
            'ngResource',
            'app.authentication', 
            'app.home', 
            'app.dashboard',
            'app.estudios',
            'app.admin',
            'app.user',
            'app.utils', 
            'dndLists',
            'app.autocaptura',
            'app.autocapturainsigna',
            'app.autocapturaiter'
        ]);
		
        angular.module('app.authentication', []);
        angular.module('app.autocaptura', []);
        angular.module('app.autocapturainsigna', []);
        angular.module('app.autocapturaiter', []);
        angular.module('app.dashboard', []);
        angular.module('app.estudios', []);
        angular.module('app.admin', []);
        angular.module('app.user', []);
        angular.module('app.home', ['ngMaterial']);
        angular.module('app.utils', []);
		
                
})();

/*Prueba cambio*/

var success=function(msg){
    $.notify(msg, 'success');
};

var info=function(msg){
    $.notify(msg, 'info');
};

var warning=function(msg){
    $.notify(msg, 'warn');
};

var error=function(msg){
    $.notify(msg, 'error');
};